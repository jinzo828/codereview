﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrCourse
    {
        public EnrCourse()
        {
            EnrCourseStaff = new HashSet<EnrCourseStaff>();
            EnrEnrollCount = new HashSet<EnrEnrollCount>();
            EnrEnrollment = new HashSet<EnrEnrollment>();
        }

        public int CourseId { get; set; }
        public string CourseSectionCode { get; set; }
        public string CourseName { get; set; }
        public DateTime? CrsBeginDate { get; set; }
        public DateTime? CrsEndDate { get; set; }
        public string CourseHour { get; set; }
        public byte? Semester { get; set; }
        public byte? CourseType { get; set; }
        public byte? NumberOfWeeks { get; set; }
        public short? MinutesPerWeek { get; set; }
        public string RoomNumber { get; set; }
        public string Cepdno { get; set; }
        public string FaNo { get; set; }
        public string OaNo { get; set; }
        public string ObNo { get; set; }
        public int? Psn { get; set; }
        public string Cipcode { get; set; }
        public byte? CourseNumber { get; set; }
        public bool? CourseActive { get; set; }
        public int? EligType { get; set; }
        public string EligTypeCert { get; set; }
        public string VirtualDelivery { get; set; }

        public virtual ICollection<EnrCourseStaff> EnrCourseStaff { get; set; }
        public virtual ICollection<EnrEnrollCount> EnrEnrollCount { get; set; }
        public virtual ICollection<EnrEnrollment> EnrEnrollment { get; set; }
        public virtual EnrProgram PsnNavigation { get; set; }
    }
}
