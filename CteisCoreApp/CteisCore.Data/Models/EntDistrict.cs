﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntDistrict
    {
        public EntDistrict()
        {
            EntEntityFanoNavigation = new HashSet<EntEntity>();
            EntEntityOanoNavigation = new HashSet<EntEntity>();
            UsrUserFalist = new HashSet<UsrUserFalist>();
        }

        public string DistNo { get; set; }
        public string DistrictName { get; set; }
        public string Eeno { get; set; }
        public bool? FiscalDist { get; set; }
        public bool? OperDist { get; set; }
        public bool? SendDist { get; set; }
        public DateTime? ClosedDistDate { get; set; }

        public virtual ICollection<EntEntity> EntEntityFanoNavigation { get; set; }
        public virtual ICollection<EntEntity> EntEntityOanoNavigation { get; set; }
        public virtual ICollection<UsrUserFalist> UsrUserFalist { get; set; }
    }
}
