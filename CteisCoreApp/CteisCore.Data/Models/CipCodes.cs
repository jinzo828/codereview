﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class CipCodes
    {
        public string Cipcode { get; set; }
        public string CipName { get; set; }
        public decimal? CipFactor { get; set; }
        public decimal? CipRank { get; set; }
        public string CipWfc { get; set; }
        public string CipCareerPath { get; set; }
        public bool CipStateApproved { get; set; }
        public string FedclusterName { get; set; }
        public string CipMinimumMinutes { get; set; }
        public bool? CipActive { get; set; }
        public bool CipAssessment { get; set; }
        public bool? NewProgramCip { get; set; }
        public string FedclusterCode { get; set; }
        public string ExamTest { get; set; }
        public string TestName { get; set; }
        public string CommentInfo { get; set; }
        public string ScedSubjectAreaCode { get; set; }
        public string ScedCourseIdCode { get; set; }
    }
}
