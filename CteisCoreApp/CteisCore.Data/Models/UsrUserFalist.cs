﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class UsrUserFalist
    {
        public int FalistId { get; set; }
        public string UserId { get; set; }
        public string Fano { get; set; }

        public virtual EntDistrict FanoNavigation { get; set; }
        //public virtual AspNetUsers User { get; set; }
    }
}
