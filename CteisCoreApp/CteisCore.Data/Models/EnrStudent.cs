﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrStudent
    {
        public EnrStudent()
        {
            EnrEnrollment = new HashSet<EnrEnrollment>();
            EnrProgramEnrollment = new HashSet<EnrProgramEnrollment>();
        }

        public string Uic { get; set; }
        public string LastName { get; set; }
        public byte[] EncLastName { get; set; }
        public string FirstName { get; set; }
        public byte[] EncFirstName { get; set; }
        public string MiddleInit { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }
        public string RaceEthnic { get; set; }
        public bool? MultiRacial { get; set; }
        public string Address1 { get; set; }
        public byte[] EncAddress1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public byte[] EncCity { get; set; }
        public string AddrState { get; set; }
        public string County { get; set; }
        public string ZipCode { get; set; }
        public string Phone1 { get; set; }
        public byte[] EncPhone1 { get; set; }
        public string Phone2 { get; set; }
        public byte[] EncPhone2 { get; set; }
        public string Email { get; set; }
        public byte[] EncEmail { get; set; }
        public string SendingDist { get; set; }
        public string SendingBldg { get; set; }
        public string Grade { get; set; }
        public string ExitStatus { get; set; }
        public DateTime? SrsdExitdate { get; set; }
        public bool? SpecialPop { get; set; }
        public bool? Handicapped { get; set; }
        public string HandicapType { get; set; }
        public string Disadvantaged { get; set; }
        public bool? Lep { get; set; }
        public bool? SingleParent { get; set; }
        public bool? DisplacedHomemaker { get; set; }
        public bool? Migrant { get; set; }
        public string Updatedby { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? ValidUic { get; set; }
        public string ErrorReason { get; set; }
        public string UiccheckDate { get; set; }
        public string CheckUic { get; set; }
        public string Uicmatch { get; set; }
        public string Uicobtain { get; set; }
        public string SrsdUpdated { get; set; }
        public string Mmeyear { get; set; }
        public string MathValid { get; set; }
        public string MathScore { get; set; }
        public string ReadingValid { get; set; }
        public string ReadingScore { get; set; }

        public virtual ICollection<EnrEnrollment> EnrEnrollment { get; set; }
        public virtual ICollection<EnrProgramEnrollment> EnrProgramEnrollment { get; set; }
        public virtual LkpExitStatus ExitStatusNavigation { get; set; }
    }
}
