﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using CteisCore.Data.Models;

namespace CteisCoreApp.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public bool Active { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        [MaxLengthAttribute(1)]
        public string Level { get; set; }

        [Display(Name = "College Name")]
        public string SchoolName { get; set; }

        //public virtual ICollection<EnrEnrollment> EnrEnrollment { get; set; }
        //public virtual ICollection<EnrEnrollment> EnrEnrollCount { get; set; }
        //public virtual ICollection<EnrEnrollment> UsrCepdlist { get; set; }
        //public virtual ICollection<EnrEnrollment> UsrUserCollegePrograms { get; set; }
        //public virtual ICollection<EnrEnrollment> UsrUserFalist { get; set; }
        //public virtual ICollection<EnrEnrollment> UsrUserBldgList { get; set; }

    }
}
