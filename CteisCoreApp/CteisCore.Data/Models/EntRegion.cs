﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntRegion
    {
        public EntRegion()
        {
            EntEntity = new HashSet<EntEntity>();
        }

        public string RegNo { get; set; }
        public string RegName { get; set; }
        public string TeamLeader { get; set; }
        public string Tlphone { get; set; }

        public virtual ICollection<EntEntity> EntEntity { get; set; }
    }
}
