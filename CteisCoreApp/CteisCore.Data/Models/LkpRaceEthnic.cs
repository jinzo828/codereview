﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class LkpRaceEthnic
    {
        public int RaceEthnicId { get; set; }
        public string RaceEthnic { get; set; }
        public string RaceEthDesc { get; set; }
        public string EdenRaceEthnic { get; set; }
        public int SortOrder { get; set; }
    }
}
