﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntProsperityRegion
    {
        public EntProsperityRegion()
        {
            EntEntity = new HashSet<EntEntity>();
        }

        public string ProsperityCode { get; set; }
        public string ProsperityName { get; set; }

        public virtual ICollection<EntEntity> EntEntity { get; set; }
    }
}
