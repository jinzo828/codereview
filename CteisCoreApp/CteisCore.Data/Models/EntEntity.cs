﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntEntity
    {
        public EntEntity()
        {
            EnrEnrollCount = new HashSet<EnrEnrollCount>();
            EnrProgram = new HashSet<EnrProgram>();
            UsrUserBldgList = new HashSet<UsrUserBldgList>();
            UsrUserCollegePrograms = new HashSet<UsrUserCollegePrograms>();
        }

        public int EntId { get; set; }
        public string RegNo { get; set; }
        public string Cepdno { get; set; }
        public string Fano { get; set; }
        public string Oano { get; set; }
        public string Obno { get; set; }
        public string BuildingType { get; set; }
        public string ConsortiumTitle { get; set; }
        public string ProsperityRegion { get; set; }
        public string CountyCode { get; set; }
        public string Isdcode { get; set; }
        public string UscongressDist { get; set; }
        public string MihouseDist { get; set; }
        public string MisenateDist { get; set; }

        public virtual ICollection<EnrEnrollCount> EnrEnrollCount { get; set; }
        public virtual ICollection<EnrProgram> EnrProgram { get; set; }
        public virtual ICollection<UsrUserBldgList> UsrUserBldgList { get; set; }
        public virtual ICollection<UsrUserCollegePrograms> UsrUserCollegePrograms { get; set; }
        public virtual EntDistrict FanoNavigation { get; set; }
        public virtual EntDistrict OanoNavigation { get; set; }
        public virtual EntBuilding ObnoNavigation { get; set; }
        public virtual EntProsperityRegion ProsperityRegionNavigation { get; set; }
        public virtual EntRegion RegNoNavigation { get; set; }
    }
}
