﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CteisCore.Data.Models
{
    public partial class OldTheusersinroles
    {
        public Guid UserId { get; set; }
        [Key]
        public Guid RoleId { get; set; }
    }
}
