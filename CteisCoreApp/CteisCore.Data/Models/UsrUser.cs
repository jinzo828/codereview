﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class UsrUser
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Lvl { get; set; }
        public int UserNum { get; set; }
        public string Mark1 { get; set; }
        public string Title { get; set; }
        public string Active { get; set; }
        public DateTime? Updatedate { get; set; }
        public string UpdateBy { get; set; }
    }
}
