﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CteisCore.Data.Models
{
    public partial class OldTheroles
    {
        public Guid ApplicationId { get; set; }
        [Key]
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
    }
}
