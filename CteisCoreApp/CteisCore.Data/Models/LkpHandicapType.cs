﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class LkpHandicapType
    {
        public int HandicaptTypeId { get; set; }
        public string HandicapType { get; set; }
        public string HandicapDesc { get; set; }
    }
}
