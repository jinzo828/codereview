﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrStaff
    {
        public EnrStaff()
        {
            EnrCourseStaff = new HashSet<EnrCourseStaff>();
        }

        public int StaffId { get; set; }
        public string StaffDesc { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInit { get; set; }
        public DateTime? Dob { get; set; }
        public string Gender { get; set; }
        public string Certification { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public bool? Active { get; set; }
        public string Cepdno { get; set; }
        public string FaNo { get; set; }
        public int? Pic { get; set; }
        public byte Picvalid { get; set; }
        public DateTime? PiccheckDate { get; set; }
        public string Picerror { get; set; }

        public virtual ICollection<EnrCourseStaff> EnrCourseStaff { get; set; }
    }
}
