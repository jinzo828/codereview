﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrEnrollCount
    {
        public int EnrollCountId { get; set; }
        public string Report { get; set; }
        public string ReportYear { get; set; }
        public int CourseId { get; set; }
        public string ReportCheck { get; set; }
        public string Cepd { get; set; }
        public string Fano { get; set; }
        public string Faname { get; set; }
        public string Oano { get; set; }
        public string Oaname { get; set; }
        public string Obno { get; set; }
        public string Obname { get; set; }
        public int? Psn { get; set; }
        public string Cipcode { get; set; }
        public string ProgramName { get; set; }
        public string Csc { get; set; }
        public string CourseName { get; set; }
        public int? Cn { get; set; }
        public byte? Bm { get; set; }
        public byte? Sn { get; set; }
        public string RoomNo { get; set; }
        public int? Mpw { get; set; }
        public byte? Sem { get; set; }
        public int? Now { get; set; }
        public int? BegEnr { get; set; }
        public int? EndEnr { get; set; }
        public int? EnrDis { get; set; }
        public int? EnrDsv { get; set; }
        public int? EnrLep { get; set; }
        public int? Enr9th { get; set; }
        public byte? Crstype { get; set; }
        public byte? AddStaff { get; set; }
        public int? SecondaryStaff { get; set; }
        public int? EnrFem { get; set; }
        public string TeacherName { get; set; }
        public string Status { get; set; }
        public DateTime? Updatedate { get; set; }
        public string Updateby { get; set; }
        public int? Ninthgrdr { get; set; }
        public DateTime? BegDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte? Deleted { get; set; }
        public byte? Addstaffpt { get; set; }
        public byte? Secstaffpt { get; set; }
        public string Stafffirstname { get; set; }
        public string Stafflastname { get; set; }
        public int? StaffId { get; set; }
        public string Asegments { get; set; }
        public string Bsegments { get; set; }
        public string Csegments { get; set; }
        public string Dsegments { get; set; }
        public string ValidRec { get; set; }
        public string CourseHour { get; set; }
        public int? DualEnr { get; set; }
        public string VirtualDelivery { get; set; }
        public int? EntId { get; set; }

        public virtual EnrCourse Course { get; set; }
        public virtual EntEntity Ent { get; set; }
        public virtual EnrProgram PsnNavigation { get; set; }
       // public virtual AspNetUsers UpdatebyNavigation { get; set; }
    }
}
