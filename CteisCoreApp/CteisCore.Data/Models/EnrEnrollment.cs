﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrEnrollment
    {
        public int EnrollId { get; set; }
        public string Uic { get; set; }
        public DateTime? EnrBeginDate { get; set; }
        public DateTime? EnrEndDate { get; set; }
        public string SemesterGrade1 { get; set; }
        public string Reportcheck { get; set; }
        public int CourseId { get; set; }
        public string SubSection { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public virtual EnrCourse Course { get; set; }
        public virtual EnrStudent UicNavigation { get; set; }
        //public virtual AspNetUsers UpdatedByNavigation { get; set; }
    }
}
