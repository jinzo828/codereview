﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class Theroles
    {
        public Guid ApplicationId { get; set; }
        public Guid RoleId { get; set; }
        public string RoleName { get; set; }
        public string LoweredRoleName { get; set; }
        public string Description { get; set; }
    }
}
