﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class Theusersinroles
    {
        public Guid UserId { get; set; }
        public Guid RoleId { get; set; }
    }
}
