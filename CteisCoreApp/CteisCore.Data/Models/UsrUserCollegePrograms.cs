﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class UsrUserCollegePrograms
    {
        public int CollegeUsrProgramId { get; set; }
        public string Fano { get; set; }
        public string Oano { get; set; }
        public string Obno { get; set; }
        public int Psn { get; set; }
        public int? EntId { get; set; }
        public string UserId { get; set; }

        public virtual EntEntity Ent { get; set; }
        public virtual EnrProgram PsnNavigation { get; set; }
        //public virtual AspNetUsers User { get; set; }
    }
}
