﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntSendingBuilding
    {
        public int EntSendingBuildingId { get; set; }
        public int OperEntId { get; set; }
        public string SendObNo { get; set; }

        public virtual EntBuilding SendObNoNavigation { get; set; }
    }
}
