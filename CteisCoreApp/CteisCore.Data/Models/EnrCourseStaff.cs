﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrCourseStaff
    {
        public int CourseStaffId { get; set; }
        public int CourseId { get; set; }
        public string CourseSectionCode { get; set; }
        public string StaffType { get; set; }
        public int StaffId { get; set; }
        public bool? Mentor { get; set; }

        public virtual EnrCourse Course { get; set; }
        public virtual EnrStaff Staff { get; set; }
    }
}
