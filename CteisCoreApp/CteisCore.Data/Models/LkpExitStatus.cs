﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class LkpExitStatus
    {
        public LkpExitStatus()
        {
            EnrStudent = new HashSet<EnrStudent>();
        }

        public int LkpExitStatusId { get; set; }
        public string ExitStatusId { get; set; }
        public string ExitStatusDesc { get; set; }

        public virtual ICollection<EnrStudent> EnrStudent { get; set; }
    }
}
