﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntBuilding
    {
        public EntBuilding()
        {
            EntEntity = new HashSet<EntEntity>();
            EntSendingBuilding = new HashSet<EntSendingBuilding>();
        }

        public string BuildNo { get; set; }
        public string BuildName { get; set; }
        public string Eeno { get; set; }
        public bool? SendBldg { get; set; }
        public bool? OperBldg { get; set; }
        public DateTime? ClosedBldgDate { get; set; }
        public string BuildingType { get; set; }
        public string ConsortiumType { get; set; }
        public string CountyCode { get; set; }
        public string ProsperityCode { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public virtual ICollection<EntEntity> EntEntity { get; set; }
        public virtual ICollection<EntSendingBuilding> EntSendingBuilding { get; set; }
    }
}
