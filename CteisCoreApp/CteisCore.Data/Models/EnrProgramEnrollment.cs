﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EnrProgramEnrollment
    {
        public int Peid { get; set; }
        public int Psn { get; set; }
        public string StudentUic { get; set; }
        public string Comments { get; set; }
        public string CompStat { get; set; }
        public DateTime? StatChangeDate { get; set; }
        public bool? TechPrep { get; set; }
        public bool? NonTrad { get; set; }
        public string Assessment { get; set; }
        public decimal? AssessScore { get; set; }
        public int? AssessMaxScore { get; set; }
        public int? AssessPf { get; set; }
        public bool? Concentrator { get; set; }
        public decimal? Segment1 { get; set; }
        public decimal? Segment2 { get; set; }
        public decimal? Segment3 { get; set; }
        public decimal? Segment4 { get; set; }
        public decimal? Segment5 { get; set; }
        public decimal? Segment6 { get; set; }
        public decimal? Segment7 { get; set; }
        public decimal? Segment8 { get; set; }
        public decimal? Segment9 { get; set; }
        public decimal? Segment10 { get; set; }
        public decimal? Segment11 { get; set; }
        public decimal? Segment12 { get; set; }
        public string LastCourseDate { get; set; }
        public string ReportCheck { get; set; }
        public string AccessYear { get; set; }
        public int? TotMin { get; set; }
        public decimal? Acr { get; set; }
        public int? TotSegGr2 { get; set; }
        public decimal? Segment13 { get; set; }

        public virtual EnrProgram PsnNavigation { get; set; }
        public virtual EnrStudent StudentUicNavigation { get; set; }
    }
}
