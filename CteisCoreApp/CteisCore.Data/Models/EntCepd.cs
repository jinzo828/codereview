﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class EntCepd
    {
        public string Cepdno { get; set; }
        public string Cepdcounties { get; set; }
        public string Cepdname { get; set; }
    }
}
