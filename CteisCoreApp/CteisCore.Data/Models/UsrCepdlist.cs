﻿using System;
using System.Collections.Generic;

namespace CteisCore.Data.Models
{
    public partial class UsrCepdlist
    {
        public int CepdlistId { get; set; }
        public string UserId { get; set; }
        public string Cepdno { get; set; }

        //public virtual AspNetUsers User { get; set; }
    }
}
