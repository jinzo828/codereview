﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CteisCoreApp.Data;

namespace CteisCoreApp.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170417145430_level")]
    partial class level
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrCourse", b =>
                {
                    b.Property<int>("CourseId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cepdno");

                    b.Property<string>("Cipcode");

                    b.Property<bool?>("CourseActive");

                    b.Property<string>("CourseHour");

                    b.Property<string>("CourseName");

                    b.Property<byte?>("CourseNumber");

                    b.Property<string>("CourseSectionCode");

                    b.Property<byte?>("CourseType");

                    b.Property<DateTime?>("CrsBeginDate");

                    b.Property<DateTime?>("CrsEndDate");

                    b.Property<int?>("EligType");

                    b.Property<string>("EligTypeCert");

                    b.Property<string>("FaNo");

                    b.Property<short?>("MinutesPerWeek");

                    b.Property<byte?>("NumberOfWeeks");

                    b.Property<string>("OaNo");

                    b.Property<string>("ObNo");

                    b.Property<int?>("Psn");

                    b.Property<string>("RoomNumber");

                    b.Property<byte?>("Semester");

                    b.HasKey("CourseId");

                    b.ToTable("EnrCourse");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrCourseStaff", b =>
                {
                    b.Property<int>("CourseStaffId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CourseId");

                    b.Property<string>("CourseSectionCode");

                    b.Property<int>("StaffId");

                    b.Property<string>("StaffType");

                    b.HasKey("CourseStaffId");

                    b.ToTable("EnrCourseStaff");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrEnrollment", b =>
                {
                    b.Property<int>("EnrollId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CourseId");

                    b.Property<DateTime?>("EnrBeginDate");

                    b.Property<DateTime?>("EnrEndDate");

                    b.Property<string>("Reportcheck");

                    b.Property<string>("SemesterGrade1");

                    b.Property<string>("SubSection");

                    b.Property<string>("Uic");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime?>("UpdatedDate");

                    b.HasKey("EnrollId");

                    b.ToTable("EnrEnrollment");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrProgram", b =>
                {
                    b.Property<int>("Psn")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cepdno");

                    b.Property<string>("Cipcode");

                    b.Property<DateTime?>("DeactivateDate");

                    b.Property<string>("FaNo");

                    b.Property<string>("Faname");

                    b.Property<string>("OaNo");

                    b.Property<string>("Oaname");

                    b.Property<string>("ObNo");

                    b.Property<string>("Obname");

                    b.Property<bool>("ProgramActive");

                    b.Property<string>("ProgramComments");

                    b.Property<string>("ProgramName");

                    b.Property<DateTime?>("ProgramStartDate");

                    b.Property<string>("ProgramType");

                    b.Property<string>("Region");

                    b.HasKey("Psn");

                    b.ToTable("EnrProgram");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrStaff", b =>
                {
                    b.Property<int>("StaffId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Active");

                    b.Property<string>("Cepdno");

                    b.Property<string>("Certification");

                    b.Property<DateTime?>("Dob");

                    b.Property<DateTime?>("ExpirationDate");

                    b.Property<string>("FaNo");

                    b.Property<string>("FirstName");

                    b.Property<string>("Gender");

                    b.Property<string>("LastName");

                    b.Property<string>("MiddleInit");

                    b.Property<int?>("Pic");

                    b.Property<DateTime?>("PiccheckDate");

                    b.Property<string>("Picerror");

                    b.Property<byte>("Picvalid");

                    b.Property<string>("StaffDesc");

                    b.HasKey("StaffId");

                    b.ToTable("EnrStaff");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EnrStudent", b =>
                {
                    b.Property<string>("Uic")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AddrState");

                    b.Property<string>("Address2");

                    b.Property<string>("CheckUic");

                    b.Property<string>("County");

                    b.Property<string>("Disadvantaged");

                    b.Property<bool?>("DisplacedHomemaker");

                    b.Property<DateTime?>("Dob");

                    b.Property<byte[]>("EncAddress1");

                    b.Property<byte[]>("EncCity");

                    b.Property<byte[]>("EncEmail");

                    b.Property<byte[]>("EncFirstName");

                    b.Property<byte[]>("EncLastName");

                    b.Property<byte[]>("EncPhone1");

                    b.Property<byte[]>("EncPhone2");

                    b.Property<string>("ErrorReason");

                    b.Property<string>("ExitStatus");

                    b.Property<string>("Gender");

                    b.Property<string>("Grade");

                    b.Property<string>("HandicapType");

                    b.Property<bool?>("Handicapped");

                    b.Property<bool?>("Lep");

                    b.Property<string>("MathScore");

                    b.Property<string>("MathValid");

                    b.Property<string>("MiddleInit");

                    b.Property<bool?>("Migrant");

                    b.Property<string>("Mmeyear");

                    b.Property<bool?>("MultiRacial");

                    b.Property<string>("RaceEthnic");

                    b.Property<string>("ReadingScore");

                    b.Property<string>("ReadingValid");

                    b.Property<string>("SendingBldg");

                    b.Property<string>("SendingDist");

                    b.Property<bool?>("SingleParent");

                    b.Property<bool?>("SpecialPop");

                    b.Property<DateTime?>("SrsdExitdate");

                    b.Property<DateTime?>("SrsdUpdated");

                    b.Property<DateTime?>("UiccheckDate");

                    b.Property<string>("Uicmatch");

                    b.Property<string>("Uicobtain");

                    b.Property<DateTime?>("UpdatedDate");

                    b.Property<string>("Updatedby");

                    b.Property<int?>("ValidUic");

                    b.Property<string>("ZipCode");

                    b.HasKey("Uic");

                    b.ToTable("EnrStudent");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EntBuilding", b =>
                {
                    b.Property<string>("BuildNo")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuildName");

                    b.Property<string>("BuildingType");

                    b.Property<DateTime?>("ClosedBldgDate");

                    b.Property<string>("ConsortiumType");

                    b.Property<string>("CountyCode");

                    b.Property<string>("Eeno");

                    b.Property<bool?>("OperBldg");

                    b.Property<string>("ProsperityCode");

                    b.Property<bool?>("SendBldg");

                    b.HasKey("BuildNo");

                    b.ToTable("EntBuilding");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EntDistrict", b =>
                {
                    b.Property<string>("DistNo")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("ClosedDistDate");

                    b.Property<string>("DistrictName");

                    b.Property<string>("Eeno");

                    b.Property<bool?>("FiscalDist");

                    b.Property<bool?>("OperDist");

                    b.Property<bool?>("SendDist");

                    b.HasKey("DistNo");

                    b.ToTable("EntDistrict");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EntEntity", b =>
                {
                    b.Property<int>("EntId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuildingType");

                    b.Property<string>("Cepdno");

                    b.Property<string>("ConsortiumTitle");

                    b.Property<string>("CountyCode");

                    b.Property<string>("Fano");

                    b.Property<string>("Isdcode");

                    b.Property<string>("MihouseDist");

                    b.Property<string>("MisenateDist");

                    b.Property<string>("Oano");

                    b.Property<string>("Obno");

                    b.Property<string>("ProsperityRegion");

                    b.Property<string>("RegNo");

                    b.Property<string>("UscongressDist");

                    b.HasKey("EntId");

                    b.ToTable("EntEntity");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.EntRegion", b =>
                {
                    b.Property<string>("RegNo")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("RegName");

                    b.Property<string>("TeamLeader");

                    b.Property<string>("Tlphone");

                    b.HasKey("RegNo");

                    b.ToTable("EntRegion");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.Rusers", b =>
                {
                    b.Property<Guid>("ApplicationId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("IsAnonymous");

                    b.Property<DateTime>("LastActivityDate");

                    b.Property<string>("LoweredUserName");

                    b.Property<string>("MobileAlias");

                    b.Property<Guid>("UserId");

                    b.Property<string>("UserName");

                    b.HasKey("ApplicationId");

                    b.ToTable("Rusers");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.TblEnrollCount", b =>
                {
                    b.Property<string>("Report")
                        .ValueGeneratedOnAdd();

                    b.Property<byte?>("AddStaff");

                    b.Property<byte?>("Addstaffpt");

                    b.Property<string>("Asegments");

                    b.Property<DateTime?>("BegDate");

                    b.Property<int?>("BegEnr");

                    b.Property<byte?>("Bm");

                    b.Property<string>("Bsegments");

                    b.Property<string>("Cepd");

                    b.Property<string>("Cipcode");

                    b.Property<int>("ClassPk");

                    b.Property<int?>("Cn");

                    b.Property<string>("CourseHour");

                    b.Property<string>("CourseName");

                    b.Property<byte?>("Crstype");

                    b.Property<string>("Csc");

                    b.Property<string>("Csegments");

                    b.Property<byte?>("Deleted");

                    b.Property<string>("Dsegments");

                    b.Property<int?>("DualEnr");

                    b.Property<DateTime?>("EndDate");

                    b.Property<int?>("EndEnr");

                    b.Property<int?>("Enr9th");

                    b.Property<int?>("EnrDis");

                    b.Property<int?>("EnrDsv");

                    b.Property<int?>("EnrFem");

                    b.Property<int?>("EnrLep");

                    b.Property<string>("Faname");

                    b.Property<string>("Fano");

                    b.Property<int?>("Mpw");

                    b.Property<int?>("Ninthgrdr");

                    b.Property<int?>("Now");

                    b.Property<string>("Oaname");

                    b.Property<string>("Oano");

                    b.Property<string>("Obname");

                    b.Property<string>("Obno");

                    b.Property<int?>("PicNumber");

                    b.Property<string>("ProgramName");

                    b.Property<int?>("Psn");

                    b.Property<string>("ReportCheck");

                    b.Property<string>("RoomNo");

                    b.Property<int?>("SecondaryStaff");

                    b.Property<byte?>("Secstaffpt");

                    b.Property<byte?>("Sem");

                    b.Property<byte?>("Sn");

                    b.Property<string>("Stafffirstname");

                    b.Property<string>("Stafflastname");

                    b.Property<string>("Status");

                    b.Property<string>("TeacherName");

                    b.Property<string>("Updateby");

                    b.Property<DateTime?>("Updatedate");

                    b.Property<string>("ValidRec");

                    b.Property<string>("Year");

                    b.HasKey("Report");

                    b.ToTable("TblEnrollCount");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.TblProgramEnrollment", b =>
                {
                    b.Property<int>("Peid")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AccessYear");

                    b.Property<decimal?>("Acr");

                    b.Property<int?>("AssessMaxScore");

                    b.Property<int?>("AssessPf");

                    b.Property<decimal?>("AssessScore");

                    b.Property<string>("Assessment");

                    b.Property<string>("Comments");

                    b.Property<string>("CompStat");

                    b.Property<bool?>("Concentrator");

                    b.Property<string>("LastCourseDate");

                    b.Property<bool?>("NonTrad");

                    b.Property<int>("Psn");

                    b.Property<string>("ReportCheck");

                    b.Property<decimal?>("Segment1");

                    b.Property<decimal?>("Segment10");

                    b.Property<decimal?>("Segment11");

                    b.Property<decimal?>("Segment12");

                    b.Property<decimal?>("Segment13");

                    b.Property<decimal?>("Segment2");

                    b.Property<decimal?>("Segment3");

                    b.Property<decimal?>("Segment4");

                    b.Property<decimal?>("Segment5");

                    b.Property<decimal?>("Segment6");

                    b.Property<decimal?>("Segment7");

                    b.Property<decimal?>("Segment8");

                    b.Property<decimal?>("Segment9");

                    b.Property<DateTime?>("StatChangeDate");

                    b.Property<string>("StudentUic");

                    b.Property<bool?>("TechPrep");

                    b.Property<int?>("TotMin");

                    b.Property<int?>("TotSegGr2");

                    b.HasKey("Peid");

                    b.ToTable("TblProgramEnrollment");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.Theroles", b =>
                {
                    b.Property<Guid>("ApplicationId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("LoweredRoleName");

                    b.Property<Guid>("RoleId");

                    b.Property<string>("RoleName");

                    b.HasKey("ApplicationId");

                    b.ToTable("Theroles");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.Tstudents", b =>
                {
                    b.Property<string>("StudentUic")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AddrState");

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<string>("CheckUic");

                    b.Property<string>("City");

                    b.Property<string>("County");

                    b.Property<string>("Disadvantaged");

                    b.Property<bool?>("DisplacedHomemaker");

                    b.Property<DateTime?>("Dob");

                    b.Property<string>("ErrorReason");

                    b.Property<string>("ExitStatus");

                    b.Property<string>("FirstName");

                    b.Property<string>("Gender");

                    b.Property<string>("Grade");

                    b.Property<string>("HandicapType");

                    b.Property<bool?>("Handicapped");

                    b.Property<string>("LastName");

                    b.Property<bool?>("Lep");

                    b.Property<string>("MathScore");

                    b.Property<string>("MathValid");

                    b.Property<string>("MiddleInit");

                    b.Property<bool?>("Migrant");

                    b.Property<string>("Mmeyear");

                    b.Property<bool?>("MultiRacial");

                    b.Property<string>("RaceEthnic");

                    b.Property<string>("ReadingScore");

                    b.Property<string>("ReadingValid");

                    b.Property<string>("SendingBldg");

                    b.Property<string>("SendingDist");

                    b.Property<bool?>("SingleParent");

                    b.Property<bool?>("SpecialPop");

                    b.Property<DateTime?>("SrsdExitdate");

                    b.Property<DateTime?>("SrsdUpdated");

                    b.Property<DateTime?>("UiccheckDate");

                    b.Property<string>("Uicmatch");

                    b.Property<string>("Uicobtain");

                    b.Property<DateTime?>("UpdatedDate");

                    b.Property<string>("Updatedby");

                    b.Property<int?>("ValidUic");

                    b.Property<string>("ZipCode");

                    b.HasKey("StudentUic");

                    b.ToTable("Tstudents");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.UsrCepdlist", b =>
                {
                    b.Property<int>("CepdlistId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Cepdno");

                    b.Property<string>("UserId");

                    b.HasKey("CepdlistId");

                    b.ToTable("UsrCepdlist");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.UsrUser", b =>
                {
                    b.Property<string>("UserName")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Active");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Lvl");

                    b.Property<string>("Mark1");

                    b.Property<string>("Phone");

                    b.Property<string>("Title");

                    b.Property<string>("UpdateBy");

                    b.Property<DateTime?>("Updatedate");

                    b.Property<int>("UserNum");

                    b.HasKey("UserName");

                    b.ToTable("UsrUser");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.UsrUserBldgList", b =>
                {
                    b.Property<int>("BldglistId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EntId");

                    b.Property<string>("Fano");

                    b.Property<string>("Oano");

                    b.Property<string>("Obno");

                    b.Property<string>("UserId");

                    b.HasKey("BldglistId");

                    b.ToTable("UsrUserBldgList");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.UsrUserCollegePrograms", b =>
                {
                    b.Property<int>("CollegeUsrCipCodeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Fano");

                    b.Property<string>("Oano");

                    b.Property<string>("Obno");

                    b.Property<int>("Psn");

                    b.Property<string>("UserId");

                    b.HasKey("CollegeUsrCipCodeId");

                    b.ToTable("UsrUserCollegePrograms");
                });

            modelBuilder.Entity("CteisCoreApp.Data.Models.UsrUserFalist", b =>
                {
                    b.Property<int>("FalistId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Fano");

                    b.Property<string>("UserId");

                    b.HasKey("FalistId");

                    b.ToTable("UsrUserFalist");
                });

            modelBuilder.Entity("CteisCoreApp.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<bool>("Active");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Level");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SchoolName");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("Title");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UpdatedBy");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("CteisCoreApp.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("CteisCoreApp.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("CteisCoreApp.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
