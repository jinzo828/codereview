﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using CteisCoreApp.Models;
using Microsoft.EntityFrameworkCore.Metadata;
using CteisCore.Data.Models;
//using CteisCore.Data.Models;

namespace CteisCoreApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //builder.Entity<Theusersinroles>()
            //    .HasKey(c => new {c.UserId, c.RoleId });

            //modelBuilder.Entity<AspNetRoleClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.RoleId)
            //        .HasName("IX_AspNetRoleClaims_RoleId");

            //    entity.Property(e => e.RoleId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetRoleClaims)
            //        .HasForeignKey(d => d.RoleId);
            //});

            //modelBuilder.Entity<AspNetRoles>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedName)
            //        .HasName("RoleNameIndex");

            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedName).HasMaxLength(256);
            //});

            //modelBuilder.Entity<AspNetUserClaims>(entity =>
            //{
            //    entity.HasIndex(e => e.UserId)
            //        .HasName("IX_AspNetUserClaims_UserId");

            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserClaims)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserLogins>(entity =>
            //{
            //    entity.HasKey(e => new { e.LoginProvider, e.ProviderKey })
            //        .HasName("PK_AspNetUserLogins");

            //    entity.HasIndex(e => e.UserId)
            //        .HasName("IX_AspNetUserLogins_UserId");

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.ProviderKey).HasMaxLength(450);

            //    entity.Property(e => e.UserId)
            //        .IsRequired()
            //        .HasMaxLength(450);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserLogins)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserRoles>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.RoleId })
            //        .HasName("PK_AspNetUserRoles");

            //    entity.HasIndex(e => e.RoleId)
            //        .HasName("IX_AspNetUserRoles_RoleId");

            //    entity.HasIndex(e => e.UserId)
            //        .HasName("IX_AspNetUserRoles_UserId");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.RoleId).HasMaxLength(450);

            //    entity.HasOne(d => d.Role)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.RoleId);

            //    entity.HasOne(d => d.User)
            //        .WithMany(p => p.AspNetUserRoles)
            //        .HasForeignKey(d => d.UserId);
            //});

            //modelBuilder.Entity<AspNetUserTokens>(entity =>
            //{
            //    entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name })
            //        .HasName("PK_AspNetUserTokens");

            //    entity.Property(e => e.UserId).HasMaxLength(450);

            //    entity.Property(e => e.LoginProvider).HasMaxLength(450);

            //    entity.Property(e => e.Name).HasMaxLength(450);
            //});

            //modelBuilder.Entity<AspNetUsers>(entity =>
            //{
            //    entity.HasIndex(e => e.NormalizedEmail)
            //        .HasName("EmailIndex");

            //    entity.HasIndex(e => e.NormalizedUserName)
            //        .HasName("UserNameIndex")
            //        .IsUnique();

            //    entity.Property(e => e.Id).HasMaxLength(450);

            //    entity.Property(e => e.Active).HasDefaultValueSql("0");

            //    entity.Property(e => e.Email).HasMaxLength(256);

            //    entity.Property(e => e.Level).HasMaxLength(1);

            //    entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

            //    entity.Property(e => e.NormalizedUserName)
            //        .IsRequired()
            //        .HasMaxLength(256);

            //    entity.Property(e => e.UpdatedDate).HasDefaultValueSql("'0001-01-01T00:00:00.000'");

            //    entity.Property(e => e.UserName).HasMaxLength(256);
            //});

            modelBuilder.Entity<CipCodes>(entity =>
            {
                entity.HasKey(e => e.Cipcode)
                    .HasName("PK_CIPCodes");

                entity.Property(e => e.Cipcode)
                    .HasColumnName("CIPCode")
                    .HasColumnType("varchar(8)");

                entity.Property(e => e.CipAssessment).HasDefaultValueSql("0");

                entity.Property(e => e.CipCareerPath)
                    .HasColumnName("CIP_CareerPath")
                    .HasMaxLength(50);

                entity.Property(e => e.CipFactor)
                    .HasColumnName("CIP_Factor")
                    .HasColumnType("decimal");

                entity.Property(e => e.CipMinimumMinutes)
                    .HasColumnName("CIP_MinimumMinutes")
                    .HasColumnType("nchar(10)");

                entity.Property(e => e.CipName)
                    .IsRequired()
                    .HasColumnName("CIP_Name")
                    .HasMaxLength(100);

                entity.Property(e => e.CipRank)
                    .HasColumnName("CIP_Rank")
                    .HasColumnType("decimal");

                entity.Property(e => e.CipStateApproved).HasColumnName("CIP_StateApproved");

                entity.Property(e => e.CipWfc)
                    .HasColumnName("CIP_WFC")
                    .HasMaxLength(10);

                entity.Property(e => e.CommentInfo).HasColumnType("nchar(100)");

                entity.Property(e => e.ExamTest).HasColumnType("nchar(3)");

                entity.Property(e => e.FedclusterCode)
                    .HasColumnName("FEDClusterCode")
                    .HasColumnType("nchar(3)");

                entity.Property(e => e.FedclusterName)
                    .HasColumnName("FEDClusterName")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ScedCourseIdCode)
                    .HasColumnName("SCED_CourseIdCode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.ScedSubjectAreaCode)
                    .HasColumnName("SCED_SubjectAreaCode")
                    .HasColumnType("varchar(100)");

                entity.Property(e => e.TestName).HasColumnType("nchar(100)");
            });

            modelBuilder.Entity<EnrCourse>(entity =>
            {

                entity.HasKey(e => e.CourseId)
                    .HasName("PK_enrCourse_Id");

                entity.ToTable("enrCourse");

                entity.Property(e => e.Cepdno)
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.Cipcode)
                    .HasColumnName("CIPCode")
                    .HasColumnType("varchar(8)");

                entity.Property(e => e.CourseHour).HasColumnType("varchar(5)");

                entity.Property(e => e.CourseName).HasColumnType("varchar(40)");

                entity.Property(e => e.CourseSectionCode)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.CrsBeginDate).HasColumnType("datetime");

                entity.Property(e => e.CrsEndDate).HasColumnType("datetime");

                entity.Property(e => e.EligTypeCert).HasColumnType("varchar(50)");

                entity.Property(e => e.FaNo).HasColumnType("char(5)");

                entity.Property(e => e.OaNo).HasColumnType("char(5)");

                entity.Property(e => e.ObNo).HasColumnType("char(5)");

                entity.Property(e => e.Psn).HasColumnName("PSN");

                entity.Property(e => e.RoomNumber).HasColumnType("varchar(5)");

                entity.Property(e => e.VirtualDelivery).HasColumnType("char(1)");

                entity.HasOne(d => d.PsnNavigation)
                    .WithMany(p => p.EnrCourse)
                    .HasForeignKey(d => d.Psn)
                    .HasConstraintName("FK_enrProgram");
            });

            modelBuilder.Entity<EnrCourseStaff>(entity =>
            {
                entity.HasKey(e => e.CourseStaffId)
                    .HasName("PK_enrCourseStaff_Id");

                entity.ToTable("enrCourseStaff");

                entity.Property(e => e.CourseSectionCode)
                    .IsRequired()
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.StaffType)
                    .IsRequired()
                    .HasColumnType("varchar(4)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.EnrCourseStaff)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_enrCourseStaff");

                entity.HasOne(d => d.Staff)
                    .WithMany(p => p.EnrCourseStaff)
                    .HasForeignKey(d => d.StaffId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_enrCourseStaff_Id");
            });

            modelBuilder.Entity<EnrEnrollCount>(entity =>
            {
                entity.HasKey(e => e.EnrollCountId)
                    .HasName("PK_tblEnrollCount_1");

                entity.ToTable("enrEnrollCount");

                entity.Property(e => e.Addstaffpt).HasColumnName("addstaffpt");

                entity.Property(e => e.Asegments)
                    .HasColumnName("ASegments")
                    .HasColumnType("nchar(13)");

                entity.Property(e => e.BegDate).HasColumnType("datetime");

                entity.Property(e => e.Bm).HasColumnName("BM");

                entity.Property(e => e.Bsegments)
                    .HasColumnName("BSegments")
                    .HasColumnType("nchar(13)");

                entity.Property(e => e.Cepd)
                    .HasColumnName("CEPD")
                    .HasColumnType("nchar(2)");

                entity.Property(e => e.Cipcode)
                    .HasColumnName("CIPCode")
                    .HasMaxLength(50);

                entity.Property(e => e.Cn).HasColumnName("CN");

                entity.Property(e => e.CourseHour).HasColumnType("varchar(5)");

                entity.Property(e => e.CourseName).HasMaxLength(50);

                entity.Property(e => e.Crstype).HasColumnName("CRSType");

                entity.Property(e => e.Csc)
                    .HasColumnName("CSC")
                    .HasMaxLength(20);

                entity.Property(e => e.Csegments)
                    .HasColumnName("CSegments")
                    .HasColumnType("nchar(13)");

                entity.Property(e => e.Deleted).HasColumnName("deleted");

                entity.Property(e => e.Dsegments)
                    .HasColumnName("DSegments")
                    .HasColumnType("nchar(13)");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.EnrDsv).HasColumnName("EnrDSv");

                entity.Property(e => e.EnrLep).HasColumnName("EnrLEP");

                entity.Property(e => e.Faname)
                    .IsRequired()
                    .HasColumnName("FAName")
                    .HasMaxLength(50);

                entity.Property(e => e.Fano)
                    .IsRequired()
                    .HasColumnName("FANO")
                    .HasColumnType("nchar(5)");

                entity.Property(e => e.Mpw).HasColumnName("MPW");

                entity.Property(e => e.Ninthgrdr).HasColumnName("ninthgrdr");

                entity.Property(e => e.Now).HasColumnName("NOW");

                entity.Property(e => e.Oaname)
                    .IsRequired()
                    .HasColumnName("OAName")
                    .HasMaxLength(50);

                entity.Property(e => e.Oano)
                    .IsRequired()
                    .HasColumnName("OANO")
                    .HasColumnType("nchar(5)");

                entity.Property(e => e.Obname)
                    .HasColumnName("OBName")
                    .HasMaxLength(50);

                entity.Property(e => e.Obno)
                    .HasColumnName("OBNO")
                    .HasColumnType("nchar(5)");

                entity.Property(e => e.ProgramName).HasMaxLength(50);

                entity.Property(e => e.Psn).HasColumnName("PSN");

                entity.Property(e => e.Report)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ReportCheck).HasColumnType("nchar(9)");

                entity.Property(e => e.ReportYear)
                    .IsRequired()
                    .HasColumnType("nchar(9)");

                entity.Property(e => e.RoomNo).HasMaxLength(5);

                entity.Property(e => e.Secstaffpt).HasColumnName("secstaffpt");

                entity.Property(e => e.Sn).HasColumnName("SN");

                entity.Property(e => e.Stafffirstname).HasMaxLength(50);

                entity.Property(e => e.Stafflastname)
                    .HasColumnName("stafflastname")
                    .HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(500);

                entity.Property(e => e.TeacherName).HasMaxLength(50);

                entity.Property(e => e.Updateby)
                    .IsRequired()
                    .HasColumnName("updateby")
                    .HasMaxLength(450);

                entity.Property(e => e.Updatedate)
                    .HasColumnName("updatedate")
                    .HasColumnType("datetime");

                entity.Property(e => e.ValidRec)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.VirtualDelivery).HasColumnType("char(1)");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.EnrEnrollCount)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_enrEnrollCount_enrCourse");

                entity.HasOne(d => d.Ent)
                    .WithMany(p => p.EnrEnrollCount)
                    .HasForeignKey(d => d.EntId)
                    .HasConstraintName("FK_enrEnrollCount_entEntity");

                entity.HasOne(d => d.PsnNavigation)
                    .WithMany(p => p.EnrEnrollCount)
                    .HasForeignKey(d => d.Psn)
                    .HasConstraintName("FK_enrEnrollCount_enrProgram");

                //entity.HasOne(d => d.UpdatebyNavigation)
                //    .WithMany(p => p.EnrEnrollCount)
                //    .HasForeignKey(d => d.Updateby)
                //    .OnDelete(DeleteBehavior.Restrict)
                //    .HasConstraintName("FK_applicationuser_enrEnrollCount");
            });

            modelBuilder.Entity<EnrEnrollment>(entity =>
            {
                entity.HasKey(e => e.EnrollId)
                    .HasName("PK_enrEnrEnrollment_Id");

                entity.ToTable("enrEnrollment");

                entity.Property(e => e.EnrBeginDate).HasColumnType("datetime");

                entity.Property(e => e.EnrEndDate).HasColumnType("datetime");

                entity.Property(e => e.Reportcheck)
                    .HasColumnName("reportcheck")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.SemesterGrade1).HasColumnType("varchar(2)");

                entity.Property(e => e.SubSection).HasColumnType("char(1)");

                entity.Property(e => e.Uic)
                    .IsRequired()
                    .HasColumnName("UIC")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.UpdatedBy).HasMaxLength(450);

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.HasOne(d => d.Course)
                    .WithMany(p => p.EnrEnrollment)
                    .HasForeignKey(d => d.CourseId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_enrCourse_UIC");

                entity.HasOne(d => d.UicNavigation)
                    .WithMany(p => p.EnrEnrollment)
                    .HasForeignKey(d => d.Uic)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_enrEnrollment_UICS");

                //entity.HasOne(d => d.UpdatedByNavigation)
                //    .WithMany(p => p.EnrEnrollment)
                //    .HasForeignKey(d => d.UpdatedBy)
                //    .HasConstraintName("FK_applicationuser_enrEnrollment");
            });

            modelBuilder.Entity<EnrProgram>(entity =>
            {
                entity.HasKey(e => e.Psn)
                    .HasName("PK_enrProgram_Id");

                entity.ToTable("enrProgram");

                entity.Property(e => e.Psn)
                    .HasColumnName("PSN")
                    .ValueGeneratedNever();

                entity.Property(e => e.Cepdno)
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.Cipcode)
                    .HasColumnName("CIPCode")
                    .HasColumnType("varchar(8)");

                entity.Property(e => e.DeactivateDate).HasColumnType("datetime");

                entity.Property(e => e.FaNo).HasColumnType("char(5)");

                entity.Property(e => e.Faname)
                    .HasColumnName("FAName")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.OaNo).HasColumnType("char(5)");

                entity.Property(e => e.Oaname)
                    .HasColumnName("OAName")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ObNo).HasColumnType("char(5)");

                entity.Property(e => e.Obname)
                    .HasColumnName("OBName")
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.ProgramComments).HasColumnType("varchar(500)");

                entity.Property(e => e.ProgramName).HasColumnType("varchar(50)");

                entity.Property(e => e.ProgramStartDate).HasColumnType("smalldatetime");

                entity.Property(e => e.ProgramType).HasColumnType("varchar(5)");

                entity.Property(e => e.Region).HasColumnType("char(2)");

                entity.HasOne(d => d.Ent)
                    .WithMany(p => p.EnrProgram)
                    .HasForeignKey(d => d.EntId)
                    .HasConstraintName("FK_entEntity_PSN");
            });

            modelBuilder.Entity<EnrProgramEnrollment>(entity =>
            {
                entity.HasKey(e => e.Peid)
                    .HasName("PK_enrProgramEnrollment_Id");

                entity.ToTable("enrProgramEnrollment");

                entity.Property(e => e.Peid).HasColumnName("PEId");

                entity.Property(e => e.AccessYear).HasColumnType("nchar(4)");

                entity.Property(e => e.Acr)
                    .HasColumnName("ACR")
                    .HasColumnType("money");

                entity.Property(e => e.AssessPf).HasColumnName("AssessPF");

                entity.Property(e => e.AssessScore).HasColumnType("decimal");

                entity.Property(e => e.Assessment).HasColumnType("char(2)");

                entity.Property(e => e.Comments).HasColumnType("varchar(500)");

                entity.Property(e => e.CompStat)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.LastCourseDate).HasColumnType("nchar(6)");

                entity.Property(e => e.Psn).HasColumnName("PSN");

                entity.Property(e => e.ReportCheck).HasColumnType("nchar(9)");

                entity.Property(e => e.Segment1).HasColumnType("decimal");

                entity.Property(e => e.Segment10).HasColumnType("decimal");

                entity.Property(e => e.Segment11).HasColumnType("decimal");

                entity.Property(e => e.Segment12).HasColumnType("decimal");

                entity.Property(e => e.Segment13).HasColumnType("decimal");

                entity.Property(e => e.Segment2).HasColumnType("decimal");

                entity.Property(e => e.Segment3).HasColumnType("decimal");

                entity.Property(e => e.Segment4).HasColumnType("decimal");

                entity.Property(e => e.Segment5).HasColumnType("decimal");

                entity.Property(e => e.Segment6).HasColumnType("decimal");

                entity.Property(e => e.Segment7).HasColumnType("decimal");

                entity.Property(e => e.Segment8).HasColumnType("decimal");

                entity.Property(e => e.Segment9).HasColumnType("decimal");

                entity.Property(e => e.StatChangeDate).HasColumnType("datetime");

                entity.Property(e => e.StudentUic)
                    .IsRequired()
                    .HasColumnName("StudentUIC")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.TotSegGr2).HasColumnName("TotSegGR2");

                entity.HasOne(d => d.PsnNavigation)
                    .WithMany(p => p.EnrProgramEnrollment)
                    .HasForeignKey(d => d.Psn)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_program_enrProgramEnrollment");

                entity.HasOne(d => d.StudentUicNavigation)
                    .WithMany(p => p.EnrProgramEnrollment)
                    .HasForeignKey(d => d.StudentUic)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_programEnrollment_StudentUIC");
            });

            modelBuilder.Entity<EnrStaff>(entity =>
            {
                entity.HasKey(e => e.StaffId)
                    .HasName("PK_enrStaff_Id");

                entity.ToTable("enrStaff");

                entity.Property(e => e.Cepdno)
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.Certification).HasColumnType("varchar(100)");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.ExpirationDate).HasColumnType("datetime");

                entity.Property(e => e.FaNo).HasColumnType("char(5)");

                entity.Property(e => e.FirstName).HasColumnType("varchar(50)");

                entity.Property(e => e.Gender).HasColumnType("char(1)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.MiddleInit).HasColumnType("varchar(1)");

                entity.Property(e => e.Pic).HasColumnName("PIC");

                entity.Property(e => e.PiccheckDate)
                    .HasColumnName("PICCheckDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Picerror)
                    .HasColumnName("PICError")
                    .HasColumnType("varchar(200)");

                entity.Property(e => e.Picvalid).HasColumnName("PICValid");

                entity.Property(e => e.StaffDesc).HasColumnType("varchar(15)");
            });

            modelBuilder.Entity<EnrStudent>(entity =>
            {
                entity.HasKey(e => e.Uic)
                    .HasName("PK_enrStudent_UIC");

                entity.ToTable("enrStudent");

                entity.Property(e => e.Uic)
                    .HasColumnName("UIC")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.AddrState).HasColumnType("char(2)");

                entity.Property(e => e.Address2).HasColumnType("varchar(50)");

                entity.Property(e => e.CheckUic)
                    .IsRequired()
                    .HasColumnName("CheckUIC")
                    .HasColumnType("char(1)");

                entity.Property(e => e.County).HasColumnType("varchar(30)");

                entity.Property(e => e.Disadvantaged).HasColumnType("char(1)");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.EncEmail)
                    .HasColumnName("encEmail")
                    .HasMaxLength(200);

                entity.Property(e => e.EncPhone1)
                    .HasColumnName("encPhone1")
                    .HasMaxLength(100);

                entity.Property(e => e.EncPhone2)
                    .HasColumnName("encPhone2")
                    .HasMaxLength(100);

                entity.Property(e => e.ErrorReason).HasColumnType("varchar(500)");

                entity.Property(e => e.ExitStatus).HasColumnType("varchar(2)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.Gender).HasColumnType("char(1)");

                entity.Property(e => e.Grade).HasColumnType("varchar(2)");

                entity.Property(e => e.HandicapType).HasColumnType("varchar(2)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(5)");

                entity.Property(e => e.Lep).HasColumnName("LEP");

                entity.Property(e => e.MathScore).HasColumnType("char(1)");

                entity.Property(e => e.MathValid).HasColumnType("char(1)");

                entity.Property(e => e.MiddleInit).HasColumnType("varchar(1)");

                entity.Property(e => e.Mmeyear)
                    .HasColumnName("MMEYear")
                    .HasColumnType("char(4)");

                entity.Property(e => e.RaceEthnic).HasColumnType("char(1)");

                entity.Property(e => e.ReadingScore).HasColumnType("char(1)");

                entity.Property(e => e.ReadingValid).HasColumnType("char(1)");

                entity.Property(e => e.SendingBldg)
                    .IsRequired()
                    .HasColumnType("char(5)");

                entity.Property(e => e.SendingDist)
                    .IsRequired()
                    .HasColumnType("char(5)");

                entity.Property(e => e.SrsdExitdate)
                    .HasColumnName("srsd_exitdate")
                    .HasColumnType("datetime");

                entity.Property(e => e.SrsdUpdated)
                    .HasColumnName("SRSD_Updated")
                    .HasColumnType("datetime");

                entity.Property(e => e.UiccheckDate)
                    .HasColumnName("UICCheckDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Uicmatch)
                    .HasColumnName("UICMatch")
                    .HasColumnType("char(1)");

                entity.Property(e => e.Uicobtain)
                    .HasColumnName("UICObtain")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.Updatedby).HasColumnType("varchar(15)");

                entity.Property(e => e.ValidUic).HasColumnName("ValidUIC");

                entity.Property(e => e.ZipCode).HasColumnType("varchar(10)");

                entity.HasOne(d => d.ExitStatusNavigation)
                    .WithMany(p => p.EnrStudent)
                    .HasForeignKey(d => d.ExitStatus)
                    .HasConstraintName("FK_ExitStatus_enrStudent");
            });

            modelBuilder.Entity<EntBuilding>(entity =>
            {
                entity.HasKey(e => e.BuildNo)
                    .HasName("PK_entBuilding_Id");

                entity.ToTable("entBuilding");

                entity.Property(e => e.BuildNo).HasColumnType("char(5)");

                entity.Property(e => e.BuildName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.BuildingType).HasColumnType("char(1)");

                entity.Property(e => e.ClosedBldgDate).HasColumnType("datetime");

                entity.Property(e => e.ConsortiumType).HasColumnType("varchar(100)");

                entity.Property(e => e.CountyCode).HasColumnType("varchar(2)");

                entity.Property(e => e.Eeno)
                    .HasColumnName("EENo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Latitude).HasMaxLength(50);

                entity.Property(e => e.Longitude).HasMaxLength(50);

                entity.Property(e => e.ProsperityCode).HasColumnType("char(2)");
            });

            modelBuilder.Entity<EntCepd>(entity =>
            {
                entity.HasKey(e => e.Cepdno)
                    .HasName("PK_entCEPD");

                entity.ToTable("entCEPD");

                entity.Property(e => e.Cepdno)
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.Cepdcounties)
                    .HasColumnName("CEPDCounties")
                    .HasColumnType("varchar(75)");

                entity.Property(e => e.Cepdname)
                    .HasColumnName("CEPDName")
                    .HasColumnType("varchar(75)");
            });

            modelBuilder.Entity<EntDistrict>(entity =>
            {
                entity.HasKey(e => e.DistNo)
                    .HasName("PK_entDistrict_Id");

                entity.ToTable("entDistrict");

                entity.Property(e => e.DistNo).HasColumnType("char(5)");

                entity.Property(e => e.ClosedDistDate).HasColumnType("datetime");

                entity.Property(e => e.DistrictName)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.Eeno)
                    .HasColumnName("EENo")
                    .HasColumnType("char(5)");
            });

            modelBuilder.Entity<EntEntity>(entity =>
            {
                entity.HasKey(e => e.EntId)
                    .HasName("PK_entEntity_Id");

                entity.ToTable("entEntity");

                entity.Property(e => e.BuildingType).HasColumnType("char(1)");

                entity.Property(e => e.Cepdno)
                    .IsRequired()
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.ConsortiumTitle).HasColumnType("varchar(100)");

                entity.Property(e => e.CountyCode).HasMaxLength(3);

                entity.Property(e => e.Fano)
                    .IsRequired()
                    .HasColumnName("FANo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Isdcode)
                    .HasColumnName("ISDCode")
                    .HasMaxLength(3);

                entity.Property(e => e.MihouseDist)
                    .HasColumnName("MIHouseDist")
                    .HasMaxLength(3);

                entity.Property(e => e.MisenateDist)
                    .HasColumnName("MISenateDist")
                    .HasMaxLength(3);

                entity.Property(e => e.Oano)
                    .IsRequired()
                    .HasColumnName("OANo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Obno)
                    .IsRequired()
                    .HasColumnName("OBNo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.ProsperityRegion).HasColumnType("char(2)");

                entity.Property(e => e.RegNo)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.UscongressDist)
                    .HasColumnName("USCongressDist")
                    .HasMaxLength(3);

                entity.HasOne(d => d.FanoNavigation)
                    .WithMany(p => p.EntEntityFanoNavigation)
                    .HasForeignKey(d => d.Fano)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_entEntity_District");

                entity.HasOne(d => d.OanoNavigation)
                    .WithMany(p => p.EntEntityOanoNavigation)
                    .HasForeignKey(d => d.Oano)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_entEntity_OANO");

                entity.HasOne(d => d.ObnoNavigation)
                    .WithMany(p => p.EntEntity)
                    .HasForeignKey(d => d.Obno)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_entBuilding_OBNO");

                entity.HasOne(d => d.ProsperityRegionNavigation)
                    .WithMany(p => p.EntEntity)
                    .HasForeignKey(d => d.ProsperityRegion)
                    .HasConstraintName("FK_enrProsProsperityRegoin_entEntity");

                entity.HasOne(d => d.RegNoNavigation)
                    .WithMany(p => p.EntEntity)
                    .HasForeignKey(d => d.RegNo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_entRegion_RegNo");
            });

            modelBuilder.Entity<EntProsperityRegion>(entity =>
            {
                entity.HasKey(e => e.ProsperityCode)
                    .HasName("PK_enrProsperityRegion_Id");

                entity.ToTable("entProsperityRegion");

                entity.Property(e => e.ProsperityCode).HasColumnType("char(2)");

                entity.Property(e => e.ProsperityName)
                    .IsRequired()
                    .HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<EntRegion>(entity =>
            {
                entity.HasKey(e => e.RegNo)
                    .HasName("PK_entRegion_Id");

                entity.ToTable("entRegion");

                entity.Property(e => e.RegNo).HasColumnType("char(2)");

                entity.Property(e => e.RegName)
                    .IsRequired()
                    .HasColumnType("varchar(60)");

                entity.Property(e => e.TeamLeader).HasColumnType("varchar(50)");

                entity.Property(e => e.Tlphone)
                    .HasColumnName("TLPhone")
                    .HasColumnType("varchar(13)");
            });

            modelBuilder.Entity<LkpExitStatus>(entity =>
            {
                entity.HasKey(e => e.ExitStatusId)
                    .HasName("PK_lkpExitStatus_Id");

                entity.ToTable("lkpExitStatus");

                entity.Property(e => e.ExitStatusId).HasColumnType("varchar(2)");

                entity.Property(e => e.ExitStatusDesc).HasColumnType("varchar(75)");
            });

            modelBuilder.Entity<Rusers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_RUsers");

                entity.ToTable("RUsers");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.LastActivityDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredUserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.MobileAlias).HasMaxLength(16);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<Theeeuserssss>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_THEEEUSERSSSS");

                entity.ToTable("THEEEUSERSSSS");

                entity.Property(e => e.UserId).ValueGeneratedNever();

                entity.Property(e => e.LastActivityDate).HasColumnType("datetime");

                entity.Property(e => e.LoweredUserName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.MobileAlias).HasMaxLength(16);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<Theroles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK_THEROLES");

                entity.ToTable("THEROLES");

                entity.Property(e => e.RoleId).ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(256);

                entity.Property(e => e.LoweredRoleName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<Theusersinroles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PK_THEUSERSINROLES");

                entity.ToTable("THEUSERSINROLES");
            });

            modelBuilder.Entity<UsrCepdlist>(entity =>
            {
                entity.HasKey(e => e.CepdlistId)
                    .HasName("PK_tblUserCEPDList");

                entity.ToTable("usrCEPDList");

                entity.Property(e => e.CepdlistId).HasColumnName("CEPDListId");

                entity.Property(e => e.Cepdno)
                    .HasColumnName("CEPDNo")
                    .HasColumnType("char(2)");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.UsrCepdlist)
                //    .HasForeignKey(d => d.UserId)
                //    .OnDelete(DeleteBehavior.Restrict)
                //    .HasConstraintName("FK_applicationuser_usrCEPDList");
            });

            modelBuilder.Entity<UsrUser>(entity =>
            {
                entity.HasKey(e => e.UserName)
                    .HasName("PK_usrUser");

                entity.ToTable("usrUser");

                entity.Property(e => e.UserName).HasColumnType("varchar(10)");

                entity.Property(e => e.Active).HasColumnType("varchar(10)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnType("varchar(50)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnType("varchar(15)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnType("varchar(25)");

                entity.Property(e => e.Lvl)
                    .IsRequired()
                    .HasColumnName("LVL")
                    .HasColumnType("char(1)");

                entity.Property(e => e.Mark1).HasColumnType("varchar(10)");

                entity.Property(e => e.Phone).HasColumnType("varchar(20)");

                entity.Property(e => e.Title).HasColumnType("varchar(25)");

                entity.Property(e => e.UpdateBy).HasColumnType("varchar(50)");

                entity.Property(e => e.Updatedate).HasColumnType("smalldatetime");
            });

            modelBuilder.Entity<UsrUserBldgList>(entity =>
            {
                entity.HasKey(e => e.BldglistId)
                    .HasName("PK_tblUserBLDGList");

                entity.ToTable("usrUserBldgList");

                entity.Property(e => e.BldglistId).HasColumnName("BLDGListId");

                entity.Property(e => e.Fano)
                    .HasColumnName("FANo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Oano)
                    .HasColumnName("OANo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.Obno)
                    .HasColumnName("OBNo")
                    .HasColumnType("char(5)");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.HasOne(d => d.Ent)
                    .WithMany(p => p.UsrUserBldgList)
                    .HasForeignKey(d => d.EntId)
                    .HasConstraintName("FK_usrUserBldgList_EntId");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.UsrUserBldgList)
                //    .HasForeignKey(d => d.UserId)
                //    .OnDelete(DeleteBehavior.Restrict)
                //    .HasConstraintName("FK_applicationuser_usrUserBldgList");
            });

            modelBuilder.Entity<UsrUserCollegePrograms>(entity =>
            {
                entity.HasKey(e => e.CollegeUsrProgramId)
                    .HasName("PK_UsrUserCollegePrograms");

                entity.Property(e => e.Psn).HasColumnName("PSN");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(450);

                entity.HasOne(d => d.Ent)
                    .WithMany(p => p.UsrUserCollegePrograms)
                    .HasForeignKey(d => d.EntId)
                    .HasConstraintName("FK_useruserCollegePrograms");

                entity.HasOne(d => d.PsnNavigation)
                    .WithMany(p => p.UsrUserCollegePrograms)
                    .HasForeignKey(d => d.Psn)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_useruserCollegePrograms_enrProgram");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.UsrUserCollegePrograms)
                //    .HasForeignKey(d => d.UserId)
                //    .OnDelete(DeleteBehavior.Restrict)
                //    .HasConstraintName("FK_applicationuser_UsrUserCollegePrograms");
            });

            modelBuilder.Entity<UsrUserFalist>(entity =>
            {
                entity.HasKey(e => e.FalistId)
                    .HasName("PK_usrUserFAList");

                entity.ToTable("usrUserFAList");

                entity.Property(e => e.FalistId).HasColumnName("FAListId");

                entity.Property(e => e.Fano)
                    .HasColumnName("FANO")
                    .HasColumnType("char(5)");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(450);

                entity.HasOne(d => d.FanoNavigation)
                    .WithMany(p => p.UsrUserFalist)
                    .HasForeignKey(d => d.Fano)
                    .HasConstraintName("FK_FANO_entDistrict");

                //entity.HasOne(d => d.User)
                //    .WithMany(p => p.UsrUserFalist)
                //    .HasForeignKey(d => d.UserId)
                //    .HasConstraintName("FK_applicationuser_usrUserFAList");
            });

            modelBuilder.Entity<EntSendingBuilding>(entity =>
            {
                entity.ToTable("entSendingBuilding");

                entity.Property(e => e.EntSendingBuildingId).HasColumnName("entSendingBuildingId");

                entity.Property(e => e.SendObNo)
                    .IsRequired()
                    .HasColumnType("char(5)");

                entity.HasOne(d => d.SendObNoNavigation)
                    .WithMany(p => p.EntSendingBuilding)
                    .HasForeignKey(d => d.SendObNo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_buildingNo");
            });

            modelBuilder.Entity<LkpHandicapType>(entity =>
            {
                entity.HasKey(e => e.HandicaptTypeId)
                    .HasName("PK_lkpHandicapType");

                entity.ToTable("lkpHandicapType");

                entity.Property(e => e.HandicapDesc).HasColumnType("varchar(40)");

                entity.Property(e => e.HandicapType)
                    .IsRequired()
                    .HasColumnType("varchar(2)");
            });

            modelBuilder.Entity<LkpRaceEthnic>(entity =>
            {
                entity.HasKey(e => e.RaceEthnicId)
                    .HasName("PK_lkpRaceEthnic");

                entity.ToTable("lkpRaceEthnic");

                entity.Property(e => e.EdenRaceEthnic).HasColumnType("varchar(3)");

                entity.Property(e => e.RaceEthDesc).HasColumnType("varchar(50)");

                entity.Property(e => e.RaceEthnic)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);



        }

        //public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        //public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        //public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        //public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        //public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        //public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        //public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public DbSet<EnrCourse> EnrCourse { get; set; }
        public DbSet<EnrCourseStaff> EnrCourseStaff { get; set; }
        public DbSet<EnrEnrollCount> EnrEnrollCount { get; set; }
        public DbSet<EnrEnrollment> EnrEnrollment { get; set; }
        public DbSet<EnrProgram> EnrProgram { get; set; }
        public DbSet<EnrProgramEnrollment> EnrProgramEnrollment { get; set; }
        public DbSet<EnrStaff> EnrStaff { get; set; }
        public DbSet<EnrStudent> EnrStudent { get; set; }
        public DbSet<EntBuilding> EntBuilding { get; set; }
        public DbSet<EntCepd> EntCepd { get; set; }
        public DbSet<EntDistrict> EntDistrict { get; set; }
        public DbSet<EntEntity> EntEntity { get; set; }
        public DbSet<EntProsperityRegion> EntProsperityRegion { get; set; }
        public DbSet<EntRegion> EntRegion { get; set; }
        public DbSet<LkpExitStatus> LkpExitStatus { get; set; }
        public DbSet<Rusers> Rusers { get; set; }
        public DbSet<Theeeuserssss> Theeeuserssss { get; set; }
        public DbSet<Theroles> Theroles { get; set; }
        public DbSet<Theusersinroles> Theusersinroles { get; set; }
        public DbSet<UsrCepdlist> UsrCepdlist { get; set; }
        public DbSet<UsrUser> UsrUser { get; set; }
        public DbSet<UsrUserBldgList> UsrUserBldgList { get; set; }
        public DbSet<UsrUserCollegePrograms> UsrUserCollegePrograms { get; set; }
        public DbSet<UsrUserFalist> UsrUserFalist { get; set; }
        public DbSet<EntSendingBuilding> EntSendingBuilding { get; set; }
        public DbSet<LkpHandicapType> LkpHandicapType { get; set; }
        public DbSet<LkpRaceEthnic> LkpRaceEthnic { get; set; }
        public DbSet<OldTheeeuserssss> OldTheeeuserssss { get; set; }
        public DbSet<OldusrUser> OldusrUser { get; set; }
        public DbSet<OldTheusersinroles> OldTheusersinroles { get; set; }
        public DbSet<OldTheroles> OldTheroles { get; set; }

    }
}
