﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CteisCoreApp.Services;
using CteisCoreApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CteisCoreApp.Models;
using CteisCore.Bl.Services;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCore.Bl.ViewModels.CteisViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using CteisCore.Bl.Services.DomainServices;
using System.Security.Claims;

namespace CteisCoreApp.Controllers.OCTE
{
    [Authorize(Roles = "OCTPAdmin, OCTPUser")]
    public class SystemMaintenanceController : Controller
    {

        private readonly UserManagementService _userManagemnetService;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserService _userService;
        private readonly CEPDService _cepdService;
        private readonly CEPDListService _cepdListService;
        private readonly EntityService _entityService;
        private readonly FiscalAgentListService _fiscalAgentListService;
        private readonly BuildingListService _buildingListService;
        private readonly CollegeProgramsService _collegeProgramsService;
        private readonly BuildingService _buildingService;
        private readonly ProgramService _programService;

        public SystemMaintenanceController(UserManagementService userManagementService, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserService userService, CEPDService cepdService, CEPDListService cepdListService, EntityService entityService, FiscalAgentListService fiscalAgentListService, BuildingListService buildingListService, CollegeProgramsService collegeProgramsService, BuildingService buildingService, ProgramService programService)
        {
            _userManagemnetService = userManagementService;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
            _cepdService = cepdService;
            _cepdListService = cepdListService;
            _entityService = entityService;
            _fiscalAgentListService = fiscalAgentListService;
            _buildingListService = buildingListService;
            _collegeProgramsService = collegeProgramsService;
            _buildingService = buildingService;
            _programService = programService;
        }

        //Manage Students Page
        public ActionResult ManageUsers()
        {

            return View();
        }

        //UserGridListRead
        public ActionResult OCTE_UserRead([DataSourceRequest]DataSourceRequest request)
        {
            var UsersList = _userService.GetUsersList();

            DataSourceResult result = UsersList.ToDataSourceResult(request);

            return Json(result);
        }

        //Returns a list of System Users
        public JsonResult GetAllSystemUsers()
        {
            var users = _userService.GetAllSystemUsers();

            return Json(users);
        }

        //Get all Cepd Admins
        public JsonResult GetAllCepdAdmins()
        {
            var cepds = _userService.GetAllCepdAdmins();

            return Json(cepds);
        }

        //GEt all Fiscal Agents
        public JsonResult GetAllFiscalAgents()
        {
            var fiscalAgents = _userService.GetAllFiscalAgents();

            return Json(fiscalAgents);
        }

        //Get all Ad Hoc Users
        public JsonResult GetAllAdHocUsers()
        {
            var users = _userService.GetAllAdHocUsers();

            return Json(users);
        }

        //Get All OCTE ADMINS
        public JsonResult GetAllOCTEAdmins()
        {
            var octeAdmins = _userService.GetAllOCTEAdmins();

            return Json(octeAdmins);
        }

        //Get All OCTE TRAC
        public JsonResult GetAllOCTETRACS()
        {
            var octetracs = _userService.GetAllOCTETracs();

            return Json(octetracs);
        }

        //Get All OCTE CRCR
        public JsonResult GetAllOCTECRCR()
        {
            var octecrcrs = _userService.GetAllOCTECRCR();

            return Json(octecrcrs);
        }

        //GetAll OCTE
        public JsonResult GetAllOCTE()
        {
            var octes = _userService.GetAllOCTE();

            return Json(octes);
        }

        //Get All College Users
        public JsonResult GetAllCollegeUsers()
        {
            var collegeusers = _userService.GetAllCollegeUsers();

            return Json(collegeusers);
        }

        //Get All Users
        public JsonResult GetAllUsers()
        {
            var UsersList = _userService.GetUsersList();

            return Json(UsersList);
        }

        //Returns a list of Cepds to view
        public JsonResult GetCepdList()
        {
            var cepds = _cepdService.GetCEPDList();

            return Json(cepds);
        }

        //Returns a list of Fiscal
        public JsonResult GetFiscalList()
        {
            var fiscals = _entityService.FiscalList();

            return Json(fiscals);
        }

        //Returns a list of usr Cepds 
        public JsonResult GetUserCepdListByUserId(string UserId)
        {
            var cepds = _cepdListService.GetCEPDListByUserId(UserId);

            var ceps = new List<string>();
            if (cepds.Count != 0)
            {
                foreach (var item in cepds)
                {
                    ceps.Add(item.Cepdno);
                }
            }


            return Json(ceps);
        }

        //Returns list of users by Cepd
        public JsonResult GetUsersByCEPD(string CEPD)
        {
            var users = _userService.GetUsersByCEPD(CEPD);

            return Json(users);
        }

        //Returns list of Users by FIscal
        public JsonResult GetUsersByFiscal(string Fiscal)
        {
            var users = _userService.GetUsersByFiscal(Fiscal);

            return Json(users);
        }

        //Return List of Fiscal by CEPD
        public JsonResult GetFiscalListByCepd(string CEPD)
        {
            var fiscals = _entityService.GetFiscalsByCEPD(CEPD);

            return Json(fiscals);
        }

        //Return a list of fiscals by User Id using UsrFaList
        public JsonResult GetFiscalListByUserId(string UserId)
        {
            var list = _fiscalAgentListService.GetFiscalsByCEPDByUserId(UserId);

            return Json(list);
        }

        //Return List of FiscalswithFano by CEPD
        public JsonResult GetFiscalsandFanoByCepd(string Cepd)
        {
            var list = _entityService.GetFiscalsandFANOByCEPD(Cepd);

            return Json(list);
        }

        //Returns a list of vmBuilding by Fiscal
        public JsonResult GetvmBuildings(string Fiscal)
        {
            var buildings = _buildingService.GetBuildingListByFiscal(Fiscal);

            return Json(buildings);
        }

        //Returns a list of User Buildings
        public JsonResult GetvmBuildingsByUserId(string UserId)
        {
            var buildings = _userService.GetUsersBuildingListByUserId(UserId);

            return Json(buildings);
        }

        //Returns a list of UserCollegePrograms
        public JsonResult GetUserCollegeProgramsByUserId(string UserId)
        {
            var programs = _collegeProgramsService.GetUsrCollegeListByUserId(UserId);

            return Json(programs);
        }

        //Returns a parital view for creating a user
        public ActionResult CreateUserView(string UserId)
        {
            if (UserId != null)
            {
                var userView = _userService.GetVmUserView(UserId);
                return PartialView("/Views/SystemMaintenance/PartialViews/_CreateUserView.cshtml", userView);
            }
            else
            {
                vmUserView model = new vmUserView();
                vmUser user = new vmUser();
                model.user = user;
                return PartialView("/Views/SystemMaintenance/PartialViews/_CreateUserView.cshtml", model);
            }
        }

        public async Task<ActionResult> CreateUser(vmUserView model)
        {

            vmValidationMessage message = new vmValidationMessage();

            if (ModelState.IsValid)
            {

                if (model.user.Email != null && model.user.FirstName != null && model.user.LastName != null && model.user.MeisNumber != null)
                {
                    var checkmeiss = _userManagemnetService.CheckForDuplicateMeiss(model.user.MeisNumber);

                    if (checkmeiss != true)
                    {
                        var status = await _userManagemnetService.CreateUser(model.user.MeisNumber, model.user.FirstName, model.user.LastName, model.user.Title, model.user.Email, model.user.PhoneNumber, model.user.SchoolName);

                        if (status == "Success")
                        {
                            var user = _userService.GetUserByMEISS(model.user.MeisNumber);

                            if (user != null)
                            {
                                await _userManagemnetService.AddUserClaims(user.Id, model.user.FirstName, model.user.LastName);

                                List<string> Roles = new List<string>();
                                if (model.CEPD == true)
                                {
                                    Roles.Add("CEPDAdmin");

                                    if (model.CEPDSelect.Count != 0)
                                    {
                                        foreach (var item in model.CEPDSelect)
                                        {
                                            _cepdListService.CreateUserCEPD(user.Id, item);
                                        }
                                    }
                                }

                                if (model.FiscalAgent == true)
                                {
                                    Roles.Add("CTEISFiscAgent");

                                    if (model.StoredFiscals.Count != 0)
                                    {
                                        foreach (var item in model.StoredFiscals)
                                        {
                                            _fiscalAgentListService.CreateUserFa(user.Id, item);
                                        }
                                    }
                                }

                                if (model.level == "NA")
                                {
                                    _userManagemnetService.UpdateUserLevel("0", user.Id);
                                }
                                if (model.level == ("Level1"))
                                {
                                    _userManagemnetService.UpdateUserLevel("1", user.Id);

                                    Roles.Add("User");
                                }
                                if (model.level == ("Level2"))
                                {
                                    _userManagemnetService.UpdateUserLevel("2", user.Id);

                                    Roles.Add("CTEISReadOnly");

                                    if (model.ModuleEnrollment == true)
                                    {
                                        Roles.Add("CTEISFunding");
                                    }
                                    if (model.ModuleExpenditures == true)
                                    {
                                        Roles.Add("CTEISExpenditures");
                                    }
                                    if (model.ModuleFollowUp == true)
                                    {
                                        Roles.Add("CTEISFollowUp");
                                    }
                                    if (model.ModuleProgram == true)
                                    {
                                        Roles.Add("CTEISPrograms");
                                    }

                                    if (model.SelectedBuildings.Count != 0)
                                    {
                                        foreach (var item in model.SelectedBuildings)
                                        {
                                            var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                            if (entity != null)
                                            {
                                                _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                            }
                                        }
                                    }

                                }
                                if (model.level == ("Level3"))
                                {
                                    _userManagemnetService.UpdateUserLevel("3", user.Id);

                                    Roles.Add("CTEISUser");

                                    if (model.ModuleEnrollment == true)
                                    {
                                        Roles.Add("CTEISFunding");
                                    }
                                    if (model.ModuleExpenditures == true)
                                    {
                                        Roles.Add("CTEISExpenditures");
                                    }
                                    if (model.ModuleFollowUp == true)
                                    {
                                        Roles.Add("CTEISFollowUp");
                                    }
                                    if (model.ModuleProgram == true)
                                    {
                                        Roles.Add("CTEISPrograms");
                                    }

                                    if (model.SelectedBuildings.Count != 0)
                                    {
                                        foreach (var item in model.SelectedBuildings)
                                        {
                                            var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                            if (entity != null)
                                            {
                                                _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                            }
                                        }
                                    }

                                }
                                if (model.level == ("Level4"))
                                {
                                    _userManagemnetService.UpdateUserLevel("4", user.Id);

                                    Roles.Add("CTEISEconDis");

                                    if (model.ModuleEnrollment == true)
                                    {
                                        Roles.Add("CTEISFunding");
                                    }
                                    if (model.ModuleExpenditures == true)
                                    {
                                        Roles.Add("CTEISExpenditures");
                                    }
                                    if (model.ModuleFollowUp == true)
                                    {
                                        Roles.Add("CTEISFollowUp");
                                    }
                                    if (model.ModuleProgram == true)
                                    {
                                        Roles.Add("CTEISPrograms");
                                    }

                                    if (model.SelectedBuildings.Count != 0)
                                    {
                                        foreach (var item in model.SelectedBuildings)
                                        {
                                            var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                            if (entity != null)
                                            {
                                                _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                            }
                                        }
                                    }

                                }

                                if (model.manage != null)
                                {
                                    Roles.Add("OCTPUser");

                                    if (model.ReportingADHoc == true)
                                    {
                                        Roles.Add("OCTEAdHoc");
                                    }
                                    if (model.ReportingTRAC == true)
                                    {
                                        Roles.Add("OCTETRAC");
                                    }
                                    if (model.ReportingCRCR == true)
                                    {
                                        Roles.Add("OCTECRCR");
                                    }
                                    if (model.SystemCTEISAdmin == true)
                                    {
                                        Roles.Add("OCTPAdmin");
                                    }
                                    if (model.SystemMDAdmin == true)
                                    {
                                        Roles.Add("OCTPMastDir");
                                    }

                                    if (model.manage == "ManagementReadOnly")
                                    {
                                        Roles.Add("OCTPReadOnly");

                                        if (model.ManagementFunding == true)
                                        {
                                            Roles.Add("OCTPFunding");
                                        }
                                        if (model.ManagementExpenditures == true)
                                        {
                                            Roles.Add("OCTPExpenditures");
                                        }
                                        if (model.ManagementFollowUp == true)
                                        {
                                            Roles.Add("OCTPFollowUp");
                                        }
                                        if (model.ManagementNPReview == true)
                                        {
                                            Roles.Add("ProgramReview");
                                        }
                                        if (model.ManagementNPAdmin == true)
                                        {
                                            Roles.Add("ProgramAdmin");
                                        }
                                    }
                                    if (model.manage == "ManagementReportsOnly")
                                    {
                                        Roles.Add("OCTPReportsOnly");
                                    }
                                    if (model.manage == "ManagementEdit")
                                    {
                                        Roles.Add("OCTPEdit");

                                        if (model.ManagementFunding == true)
                                        {
                                            Roles.Add("OCTPFunding");
                                        }
                                        if (model.ManagementExpenditures == true)
                                        {
                                            Roles.Add("OCTPExpenditures");
                                        }
                                        if (model.ManagementFollowUp == true)
                                        {
                                            Roles.Add("OCTPFollowUp");
                                        }
                                        if (model.ManagementNPReview == true)
                                        {
                                            Roles.Add("ProgramReview");
                                        }
                                        if (model.ManagementNPAdmin == true)
                                        {
                                            Roles.Add("ProgramAdmin");
                                        }
                                    }
                                }

                                if (model.college != null)
                                {
                                    if (model.college == "CollegeAdmin")
                                    {
                                        Roles.Add("CollegeAdmin");

                                        if (model.SelectedPrograms.Count != 0)
                                        {
                                            foreach (var item in model.SelectedPrograms)
                                            {
                                                var program = _programService.GetProgramByPsn(Int32.Parse(item));

                                                if (program != null)
                                                {
                                                    _collegeProgramsService.CreateUserCollegeProgram(program.FaNo, program.OaNo, program.ObNo, user.Id, program.Psn);
                                                }
                                            }
                                        }
                                    }
                                    if (model.college == "CollegeUser")
                                    {
                                        Roles.Add("CollegeUser");

                                        if (model.SelectedPrograms.Count != 0)
                                        {
                                            foreach (var item in model.SelectedPrograms)
                                            {
                                                var program = _programService.GetProgramByPsn(Int32.Parse(item));

                                                if (program != null)
                                                {
                                                    _collegeProgramsService.CreateUserCollegeProgram(program.FaNo, program.OaNo, program.ObNo, user.Id, program.Psn);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (model.PTDUser == true)
                                {
                                    Roles.Add("PTDUser");
                                }
                                if (model.PTDSurv == true)
                                {
                                    Roles.Add("PTDSurv");
                                }

                                if (Roles.Count != 0)
                                {
                                    foreach (var item in Roles)
                                    {
                                        _userManagemnetService.AddRoleToUser(user.Id, item);
                                    }
                                }

                            }

                            message.Message = model.user.FirstName + " Created";
                            message.Type = "Success";
                            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

                        }
                    }
                    else
                    {
                        message.Message = "Meiss Account Already Exists.";
                        message.Type = "Error";
                        ModelState.AddModelError(string.Empty, "Meiss Account Already Exists.");
                        return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                    }
                }
                else
                {

                    message.Message = "Missing Fields.";

                    if (model.user.Email == null)
                    {
                        message.Message += "<br /> Email";
                    }
                    if (model.user.FirstName == null)
                    {
                        message.Message += "<br /> FirstName";
                    }
                    if (model.user.LastName == null)
                    {
                        message.Message += "<br /> LastName";
                    }
                    if (model.user.MeisNumber == null)
                    {
                        message.Message += "<br /> Meiss Number";
                    }

                    message.Type = "Error";
                    ModelState.AddModelError(string.Empty, "Missing Key Fields.");
                    return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                }
            }

            message.Message = "Error";
            message.Type = "Error";
            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
            //return Json("success");
        }

        public async Task<ActionResult> UpdateUser(vmUserView model)
        {
            vmValidationMessage message = new vmValidationMessage();

            if (ModelState.IsValid)
            {
                if (model.user.Email != null && model.user.FirstName != null && model.user.LastName != null && model.user.MeisNumber != null)
                {
                    var user = _userService.GetUserById(model.user.UserId);

                    if (user != null)
                    {
                        _userManagemnetService.RemoveAllRolesFromUser(user.UserId);
                        _cepdListService.RemoveAllUserCepdAdminByUserId(user.UserId);
                        _fiscalAgentListService.RemoveAllUserFaListByUserId(user.UserId);
                        _buildingListService.RemoveAllUserBuildingListByUserId(user.UserId);
                        _collegeProgramsService.RemoveAllCollegeProgramsListByUserId(user.UserId);

                        _userManagemnetService.UpdateUser(model.user.UserId, model.user.SchoolName, model.user.FirstName, model.user.LastName, model.user.MeisNumber, model.user.PhoneNumber, model.user.Title, model.user.Email);

                        _userManagemnetService.RemoveUserClaims(user.UserId);
                        await _userManagemnetService.AddUserClaims(model.user.UserId, model.user.FirstName, model.user.LastName);

                        if (model.Activate == true)
                        {
                            _userManagemnetService.ActivateUser(user.UserId);
                        }
                        if (model.Deactivate == true)
                        {
                            _userManagemnetService.DeactivateUser(user.UserId);
                        }

                        List<string> Roles = new List<string>();
                        if (model.CEPD == true)
                        {
                            Roles.Add("CEPDAdmin");

                            if (model.CEPDSelect != null && model.CEPDSelect.Count != 0)
                            {
                                foreach (var item in model.CEPDSelect)
                                {
                                    _cepdListService.CreateUserCEPD(user.UserId, item);
                                }
                            }
                        }

                        if (model.FiscalAgent == true)
                        {
                            Roles.Add("CTEISFiscAgent");

                            if (model.StoredFiscals != null && model.StoredFiscals.Count != 0)
                            {
                                foreach (var item in model.StoredFiscals)
                                {
                                    _fiscalAgentListService.CreateUserFa(user.UserId, item);
                                }
                            }
                        }

                        if (model.level == "NA")
                        {
                            _userManagemnetService.UpdateUserLevel("0", user.UserId);
                        }
                        if (model.level == ("Level1"))
                        {
                            _userManagemnetService.UpdateUserLevel("1", user.UserId);

                            Roles.Add("User");

                        }
                        if (model.level == ("Level2"))
                        {
                            _userManagemnetService.UpdateUserLevel("2", user.UserId);

                            Roles.Add("CTEISReadOnly");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings != null && model.SelectedBuildings.Count != 0)
                            {

                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }

                        }
                        if (model.level == ("Level3"))
                        {
                            _userManagemnetService.UpdateUserLevel("3", user.UserId);

                            Roles.Add("CTEISUser");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings != null && model.SelectedBuildings.Count != 0)
                            {
                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }

                        }
                        if (model.level == ("Level4"))
                        {
                            _userManagemnetService.UpdateUserLevel("4", user.UserId);

                            Roles.Add("CTEISEconDis");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings != null && model.SelectedBuildings.Count != 0)
                            {
                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }

                            if (model.manage != null)
                            {
                                Roles.Add("OCTPUser");

                                if (model.ReportingADHoc == true)
                                {
                                    Roles.Add("OCTEAdHoc");
                                }
                                if (model.ReportingTRAC == true)
                                {
                                    Roles.Add("OCTETRAC");
                                }
                                if (model.ReportingCRCR == true)
                                {
                                    Roles.Add("OCTECRCR");
                                }
                                if (model.SystemCTEISAdmin == true)
                                {
                                    Roles.Add("OCTPAdmin");
                                }
                                if (model.SystemMDAdmin == true)
                                {
                                    Roles.Add("OCTPMastDir");
                                }

                                if (model.manage == "ManagementReadOnly")
                                {
                                    Roles.Add("OCTPReadOnly");

                                    if (model.ManagementFunding == true)
                                    {
                                        Roles.Add("OCTPFunding");
                                    }
                                    if (model.ManagementExpenditures == true)
                                    {
                                        Roles.Add("OCTPExpenditures");
                                    }
                                    if (model.ManagementFollowUp == true)
                                    {
                                        Roles.Add("OCTPFollowUp");
                                    }
                                    if (model.ManagementNPReview == true)
                                    {
                                        Roles.Add("ProgramReview");
                                    }
                                    if (model.ManagementNPAdmin == true)
                                    {
                                        Roles.Add("ProgramAdmin");
                                    }
                                }
                                if (model.manage == "ManagementReportsOnly")
                                {
                                    Roles.Add("OCTPReportsOnly");
                                }
                                if (model.manage == "ManagementEdit")
                                {
                                    Roles.Add("OCTPEdit");

                                    if (model.ManagementFunding == true)
                                    {
                                        Roles.Add("OCTPFunding");
                                    }
                                    if (model.ManagementExpenditures == true)
                                    {
                                        Roles.Add("OCTPExpenditures");
                                    }
                                    if (model.ManagementFollowUp == true)
                                    {
                                        Roles.Add("OCTPFollowUp");
                                    }
                                    if (model.ManagementNPReview == true)
                                    {
                                        Roles.Add("ProgramReview");
                                    }
                                    if (model.ManagementNPAdmin == true)
                                    {
                                        Roles.Add("ProgramAdmin");
                                    }
                                }
                            }
                        }

                        if (model.college != null)
                        {
                            if (model.college == "CollegeAdmin")
                            {
                                Roles.Add("CollegeAdmin");

                                if (model.SelectedPrograms.Count != 0)
                                {
                                    foreach (var item in model.SelectedPrograms)
                                    {
                                        var program = _programService.GetProgramByPsn(Int32.Parse(item));

                                        if (program != null)
                                        {
                                            _collegeProgramsService.CreateUserCollegeProgram(program.FaNo, program.OaNo, program.ObNo, user.UserId, program.Psn);
                                        }
                                    }
                                }
                            }
                            if (model.college == "CollegeUser")
                            {
                                Roles.Add("CollegeUser");

                                if (model.SelectedPrograms.Count != 0)
                                {
                                    foreach (var item in model.SelectedPrograms)
                                    {
                                        var program = _programService.GetProgramByPsn(Int32.Parse(item));

                                        if (program != null)
                                        {
                                            _collegeProgramsService.CreateUserCollegeProgram(program.FaNo, program.OaNo, program.ObNo, user.UserId, program.Psn);
                                        }
                                    }
                                }
                            }
                        }

                        if (model.PTDUser == true)
                        {
                            Roles.Add("PTDUser");
                        }
                        if (model.PTDSurv == true)
                        {
                            Roles.Add("PTDSurv");
                        }

                        if (Roles.Count != 0)
                        {
                            foreach (var item in Roles)
                            {
                                _userManagemnetService.AddRoleToUser(user.UserId, item);
                            }
                        }

                        message.Message = model.user.FirstName + " Updated";
                        message.Type = "Success";
                        return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

                    }

                    message.Message = "Error Can't Find User";
                    message.Type = "Error";
                    return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

                }

                message.Message = "Missing Fields.";

                if (model.user.Email == null)
                {
                    message.Message += "<br /> Email";
                }
                if (model.user.FirstName == null)
                {
                    message.Message += "<br /> FirstName";
                }
                if (model.user.LastName == null)
                {
                    message.Message += "<br /> LastName";
                }
                if (model.user.MeisNumber == null)
                {
                    message.Message += "<br /> Meiss Number";
                }
                message.Type = "Error";
                return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
            }
            else
            {
                message.Message = "Missing Fields.";
                message.Type = "Error";
                return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
            }
        }

        //College AUtocomeplete 
        public List<vmAutoComplete> SearchAutoComplete(string Search)
        {
            var buildings = _buildingService.GetAllBuildings();
            var cips = _programService.GetAllCipCodes();

            List<vmAutoComplete> auto = new List<vmAutoComplete>();

            foreach (var item in buildings)
            {
                vmAutoComplete comp = new vmAutoComplete();
                comp.Name = item.BuildName;
                comp.Data = item.BuildNo;
                auto.Add(comp);
            }

            foreach (var item in cips)
            {
                vmAutoComplete comp = new vmAutoComplete();
                comp.Name = item.Cipcode;
                comp.Data = item.Cipcode;
                auto.Add(comp);
            }

            return auto;

        }

        //College Programs
        public JsonResult GetSearchPrograms(string searchString)
        {
            var programs = _programService.GetProgramsByBuildNumOrCipCode(searchString);

            return Json(programs);
        }
    }
}
