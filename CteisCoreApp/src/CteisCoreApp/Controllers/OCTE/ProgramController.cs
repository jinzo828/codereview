using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CteisCoreApp.Services;
using CteisCoreApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CteisCoreApp.Models;
using CteisCore.Bl.Services;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCore.Bl.ViewModels.CteisViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using CteisCore.Bl.Services.DomainServices;
using System.Security.Claims;

namespace CteisCoreApp.Controllers.OCTE
{
    //[Authorize(Roles = "OCTPAdmin, OCTPUser")]
    public class ProgramController : Controller
    {
        private readonly ProgramService _programService;

        public ProgramController(ProgramService programService)
            {
            _programService = programService;
            }

        //Get the data for the Program maintenance screen
        public ActionResult UpdateGrid([DataSourceRequest] DataSourceRequest request, string Action)
            {
            if (Action == "Active")
                {
                var programs = _programService.GetAllPrograms(true);
                    DataSourceResult result = programs.ToDataSourceResult(request);
                return Json(result);
                }
            else if (Action == "InActive")
                {
                var programs = _programService.GetAllPrograms(false);
                DataSourceResult result = programs.ToDataSourceResult(request);
                return Json(result);
                }
            else
                {
                var programs = _programService.GetAllPrograms();
                DataSourceResult result = programs.ToDataSourceResult(request);
                return Json(result);
                }

            }

        // Return the view
        public IActionResult ProgramMaintenance()
        {
            return View();
        }

        //Get Program By PSN
        public ActionResult CreateProgramView(int PSN)
            {
            if (PSN != 0)
                {
                var program = _programService.GetProgramByPsn(PSN);
                return PartialView("/Views/Program/PartialViews/_CreateProgram.cshtml", program);
                }
            else
                {
                vmProgram programView = new vmProgram();
                return PartialView("/Views/Program/PartialViews/_CreateProgram.cshtml", programView);
                }
            }

        }
}