﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CteisCoreApp.Services;
using CteisCoreApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CteisCoreApp.Models;
using CteisCore.Bl.Services;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCore.Bl.ViewModels.CteisViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using CteisCore.Bl.Services.DomainServices;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CteisCoreApp.Controllers.DataEntry
{
    [Authorize(Roles = "CTEISFunding, OCTPAdmin, OCTPUser, OCTPUser, OCTPFunding")]
    public class StudentsController : Controller
    {

        private readonly UserManagementService _userManagemnetService;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserService _userService;
        private readonly CEPDService _cepdService;
        private readonly CEPDListService _cepdListService;
        private readonly EntityService _entityService;
        private readonly FiscalAgentListService _fiscalAgentListService;
        private readonly BuildingListService _buildingListService;
        private readonly CollegeProgramsService _collegeProgramsService;
        private readonly BuildingService _buildingService;
        private readonly ProgramService _programService;
        private readonly StudentService _studentService;
        private readonly SendingBuildingService _sendingBuildingService;
        private readonly ProgramEnrollmentService _programEnrollmentService;

        public StudentsController(UserManagementService userManagementService, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserService userService, CEPDService cepdService, CEPDListService cepdListService, EntityService entityService, 
            FiscalAgentListService fiscalAgentListService, BuildingListService buildingListService, CollegeProgramsService collegeProgramsService, BuildingService buildingService, ProgramService programService, StudentService studentService, SendingBuildingService sendingBuildingService,
            ProgramEnrollmentService programEnrollmentService)
        {
            _userManagemnetService = userManagementService;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
            _cepdService = cepdService;
            _cepdListService = cepdListService;
            _entityService = entityService;
            _fiscalAgentListService = fiscalAgentListService;
            _buildingListService = buildingListService;
            _collegeProgramsService = collegeProgramsService;
            _buildingService = buildingService;
            _programService = programService;
            _studentService = studentService;
            _sendingBuildingService = sendingBuildingService;
            _programEnrollmentService = programEnrollmentService;
        }

        //Update Grid
        public ActionResult UpdateGrid([DataSourceRequest]DataSourceRequest request, string OCTEBuildingNo, string FABuildingNo, string OCTESearch, string FASearch, bool LeftSchool)
        {

            if (FASearch != null)
            {
                var UserId = _userManager.GetUserId(HttpContext.User);

                if(FASearch.All(c => char.IsDigit(c)) == true)
                {
                    var students = _studentService.GetStudentByUIC(FASearch, UserId);

                    DataSourceResult result = students.ToDataSourceResult(request);

                    return Json(result);

                }
                else
                {
                    var student = _studentService.GetStudentByLastName(FASearch, UserId);

                    DataSourceResult result = student.ToDataSourceResult(request);

                    return Json(result);

                }

            }
            else if (OCTESearch != null)
            {
                var list = _studentService.GetAllStudentsByUIC(OCTESearch);
                DataSourceResult result = list.ToDataSourceResult(request);
                return Json(result);

            }
            else if (FABuildingNo != null)
            {

                if (LeftSchool == false)
                {
                    var list = _studentService.GetStudentsBySendingFacility(FABuildingNo);
                    //return Json(list);

                    DataSourceResult result = list.ToDataSourceResult(request);
                    return Json(result);
                }
                else if (LeftSchool == true)
                {
                    var list = _studentService.GetStudentsBySendingFacilityLeftSchool(FABuildingNo);
                    //return Json(list);
                    DataSourceResult result = list.ToDataSourceResult(request);

                    return Json(result);
                }

            }
            else if (OCTEBuildingNo != null)
            {
                var list = _studentService.GetStudentsBySendingFacilityLeftSchool(OCTEBuildingNo);
                DataSourceResult result = list.ToDataSourceResult(request);

                return Json(result);
            }


                List<vmStudent> rlist = new List<vmStudent>();
                return Json(rlist);

        }

        //Create Sudent Record
        public ActionResult CreateStudent(vmStudent student, DateTime datetimepicker, string RoleFilter, string SendingDist, string SendingFac, bool SingleParent, bool DisplacedHomemaker)
        {
            vmValidationMessage message = new vmValidationMessage();
            var userId = _userManager.GetUserId(HttpContext.User);


            if (student.StudentUIC == null || student.FirstName == null || student.LastName == null || datetimepicker == null
                   || SendingDist == null || SendingFac == null || RoleFilter == null)
            {
                message.Message = "Missing Required Fields: ";

                if (student.StudentUIC == null)
                {
                    message.Message += "<br /> UIC";
                }
                if (student.FirstName == null)
                {
                    message.Message += "<br /> First Name";
                }
                if(student.LastName == null)
                {
                    message.Message += "<br /> Last Name";
                }
                if(SendingDist == null)
                {
                    message.Message += "<br /> Sending District";
                }
                if(SendingFac == null)
                {
                    message.Message += "<br /> Sending Facility";
                }
                if(RoleFilter == null)
                {
                    message.Message += "<br /> Gender";
                }

                message.Type = "Error";

            }
            else
            {
                var check = _studentService.UICAlreadyExsists(student.StudentUIC);

                if(check == false)
                {
                    var status = _studentService.CreateStudent(student.StudentUIC, student.LastName, student.FirstName, student.MiddleInital, datetimepicker, RoleFilter, student.Address1, student.Address2
                    , student.City, student.ZipCode, student.Phone1, student.Phone2, student.Email, SendingFac, SendingDist, SingleParent, DisplacedHomemaker, userId, DateTime.Now);

                    if (status == "Success")
                    {
                        message.Message = student.FirstName + " Created";
                        message.Type = "Success";
                    }
                    else
                    {
                        message.Message = status;
                        message.Type = "Error";
                    }
                }
                else
                {
                    message.Message = "UIC already used";
                    message.Type = "Error";
                }
            }

            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
     
        }

        //Update Student Record
        public ActionResult UpdateStudent(vmStudent student, DateTime datetimepicker, string RoleFilter, string SendingDist, string SendingFac, bool SingleParent, bool DisplacedHomemaker)
        {
            vmValidationMessage message = new vmValidationMessage();
            var userId = _userManager.GetUserId(HttpContext.User);

            if (student.StudentUIC == null || student.FirstName == null || student.LastName == null || datetimepicker == null
                  || SendingDist == null || SendingFac == null || RoleFilter == null)
            {
                message.Message = "Missing Required Fields: ";

                if (student.StudentUIC == null)
                {
                    message.Message += "<br /> UIC";
                }
                if (student.FirstName == null)
                {
                    message.Message += "<br /> First Name";
                }
                if (student.LastName == null)
                {
                    message.Message += "<br /> Last Name";
                }
                if (SendingDist == null)
                {
                    message.Message += "<br /> Sending District";
                }
                if (SendingFac == null)
                {
                    message.Message += "<br /> Sending Facility";
                }
                if(RoleFilter == null)
                {
                    message.Message += "<br /> Gender";
                }

                message.Type = "Error";

            }
            else
            {
                var status = _studentService.UpdateStudent(student.StudentUIC, student.LastName, student.FirstName, student.MiddleInital, datetimepicker, RoleFilter, student.Address1, student.Address2
                   , student.City, student.ZipCode, student.Phone1, student.Phone2, student.Email, SendingFac, SendingDist, SingleParent, DisplacedHomemaker, userId, DateTime.Now);

                if (status == "Success")
                {
                    message.Message = student.FirstName + " Updated";
                    message.Type = "Success";
                }
                else
                {
                    message.Message = status;
                    message.Type = "Error";
                }

            }



            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
        }

        //Manage Students View
        public ActionResult ManageStudents()
        {
            //var students = _studentService.StoredProcedureTest();
            return View();
        }

        //Returns a list of cepds
        public JsonResult GetCepdList()
        {
            var list = _cepdService.GetCEPDList();

            return Json(list);
        }

        //Returns a list of Fiscals by Cepd
        public JsonResult GetFiscalsAndFanoByCEPD(string CEPD)
        {
            var list = _entityService.GetFiscalsandFANOByCEPD(CEPD);

            return Json(list);
        }

        //Returns a list of buildings by Fiscal
        public JsonResult GetBuildingsByFiscal(string FANO)
        {
            var list = _buildingService.GetBuildingListByFiscal(FANO);

            return Json(list);
        }

        //Get Student By UIC
        public JsonResult GetStudentByUIC(string UIC)
        {
            var userId = _userManager.GetUserId(HttpContext.User);

            var student = _studentService.GetVmStudent(UIC);

            return Json(student);
        }

        //Octe UIC Search
        //public ActionResult GetOCTEStudentByUIC([DataSourceRequest]DataSourceRequest request, string UIC)
        //{
        //    var student = _studentService.GetAllStudentsByUIC(UIC);

        //    DataSourceResult result = student.ToDataSourceResult(request);

        //    return Json(student);
        //}

        //GetStudentsByUIC
        public JsonResult GetStudentsByUic(string UIC)
        {
            var UserId = _userManager.GetUserId(HttpContext.User);
            var students = _studentService.GetStudentByUIC(UIC, UserId);

            return Json(students);
        }

        //Get students by last name or UIC
        //public JsonResult GetStudentbyLastNameOrUIC(string Search)
        //{
        //    List<vmStudent> list = new List<vmStudent>();
        //    var UserId = _userManager.GetUserId(HttpContext.User);
        //    var students = _studentService.GetStudentByUIC(Search, UserId);

        //    var student = _studentService.GetStudentByLastName(Search, UserId);

        //    if(students.Count != 0)
        //    {
        //        foreach(var item in students)
        //        {
        //            list.Add(item);
        //        }
        //    }

        //    if(student.Count != 0)
        //    {
        //        foreach(var item in student)
        //        {
        //            list.Add(item);
        //        }
        //    }

        //    return Json(list);

        //}


        //Get Student By LastName
        public JsonResult GetStudentsByLastName(string LastName)
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            var student = _studentService.GetStudentByLastName(LastName, userId);

            return Json(student);
        }

        //Returns a users building list/SendingBuilding List
        public JsonResult GetFiscalBuildingList()
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            List<vmBuilding> list = new List<vmBuilding>();
            var buildingList = _buildingListService.GetUserBuildingListByUserId(userId);
            var sendingBuildingList = _sendingBuildingService.GetSendingBuildingListByUserId(userId);

            if (sendingBuildingList.Count != 0)
            {
                foreach (var item in sendingBuildingList)
                {
                    bool alradyExists = list.Any(x => x.BuildingName == item.BuildingName);

                    if (alradyExists == false)
                    {
                        list.Add(item);
                    }
                }
            }

            if (buildingList.Count != 0)
            {
                foreach (var item in buildingList)
                {
                    bool alradyExists = list.Any(x => x.BuildingName == item.BuildingName);

                    if (alradyExists == false)
                    {
                        list.Add(item);
                    }
                }
            }

            return Json(list.OrderBy(x=>x.BuildingName));
        }

        //Return a list of sendingbuiling and building fiscals
        public JsonResult GetSendingFiscalList()
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            var list = _entityService.SendingFiscalList(userId);

            return Json(list);
        }

        //Get Student By UIC
        public ActionResult CreateStudentView(string StudentUIC)
        {
            if (StudentUIC != null)
            {
                var student = _studentService.GetVmStudent(StudentUIC);
                return PartialView("/Views/Students/PartialViews/_CreateStudent.cshtml", student);
            }
            else
            {
                vmStudent studentView = new vmStudent();
                return PartialView("/Views/Students/PartialViews/_CreateStudent.cshtml", studentView);
            }
        }

        //Get all Fiscals
        public JsonResult GetAllDistricts()
        {
            var list = _entityService.FiscalList();

            return Json(list);
        }

        //Get sending Fac
        public JsonResult GetSendingFac(string Oano)
        {
            //var list = _buildingService.GetBuildingListByFiscal(FANO);
            var list = _buildingService.GetBuildingsByOano(Oano);

            return Json(list);
        }

        public ActionResult CreateProgramEnrollmentView(string StudentUIC)
        {
            if (StudentUIC != null)
            {
                var student = _studentService.GetVmStudent(StudentUIC);
                var list = _programEnrollmentService.GetStudentProgramEnrollments(StudentUIC);
                return PartialView("/Views/Students/PartialViews/_ProgramEnrollment.cshtml", list);
            }
            else
            {
                vmStudent studentView = new vmStudent();
                var list = _programEnrollmentService.GetStudentProgramEnrollments(StudentUIC);
                return PartialView("/Views/Students/PartialViews/_ProgramEnrollment.cshtml", list);
            }
        }
    }
}
