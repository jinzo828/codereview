﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CteisCoreApp.Services;
using CteisCoreApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CteisCoreApp.Models;
using CteisCore.Bl.Services;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCore.Bl.ViewModels.CteisViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using CteisCore.Bl.Services.DomainServices;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CteisCoreApp.Controllers.DataEntry
{
    public class CoursesController : Controller
    {
        private readonly UserManagementService _userManagemnetService;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserService _userService;
        private readonly CEPDService _cepdService;
        private readonly CEPDListService _cepdListService;
        private readonly EntityService _entityService;
        private readonly FiscalAgentListService _fiscalAgentListService;
        private readonly BuildingListService _buildingListService;
        private readonly CollegeProgramsService _collegeProgramsService;
        private readonly BuildingService _buildingService;
        private readonly ProgramService _programService;
        private readonly StudentService _studentService;
        private readonly SendingBuildingService _sendingBuildingService;
        private readonly ProgramEnrollmentService _programEnrollmentService;
        private readonly CourseService _courseService;

        public CoursesController(UserManagementService userManagementService, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserService userService, CEPDService cepdService, CEPDListService cepdListService, EntityService entityService,
            FiscalAgentListService fiscalAgentListService, BuildingListService buildingListService, CollegeProgramsService collegeProgramsService, BuildingService buildingService, ProgramService programService, StudentService studentService, SendingBuildingService sendingBuildingService,
            ProgramEnrollmentService programEnrollmentService, CourseService courseService)
        {
            _userManagemnetService = userManagementService;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
            _cepdService = cepdService;
            _cepdListService = cepdListService;
            _entityService = entityService;
            _fiscalAgentListService = fiscalAgentListService;
            _buildingListService = buildingListService;
            _collegeProgramsService = collegeProgramsService;
            _buildingService = buildingService;
            _programService = programService;
            _studentService = studentService;
            _sendingBuildingService = sendingBuildingService;
            _programEnrollmentService = programEnrollmentService;
            _courseService = courseService;
        }

        //Manage Courses Page
        public ActionResult ManageCourses()
        {
            return View();
        }

        //Return a list of Cepds
        public JsonResult GetCepdList()
        {
            var list = _cepdService.GetCEPDList();

            return Json(list);
        }

        //Returns a list of Fiscals by Cepd
        public JsonResult GetFiscalsAndFanoByCEPD(string CEPD)
        {
            var list = _entityService.GetFiscalsandFANOByCEPD(CEPD);

            return Json(list);
        }

        //Returns a list of buildings by Fiscal
        public JsonResult GetBuildingsByFiscal(string FANO)
        {
            var list = _buildingService.GetBuildingListByFiscal(FANO);

            return Json(list);
        }

        //Returns a users building list/SendingBuilding List
        public JsonResult GetFiscalBuildingList()
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            List<vmBuilding> list = new List<vmBuilding>();
            var buildingList = _buildingListService.GetUserBuildingListByUserId(userId);
            var sendingBuildingList = _sendingBuildingService.GetSendingBuildingListByUserId(userId);

            if (sendingBuildingList.Count != 0)
            {
                foreach (var item in sendingBuildingList)
                {
                    bool alradyExists = list.Any(x => x.BuildingName == item.BuildingName);

                    if (alradyExists == false)
                    {
                        list.Add(item);
                    }
                }
            }

            if (buildingList.Count != 0)
            {
                foreach (var item in buildingList)
                {
                    bool alradyExists = list.Any(x => x.BuildingName == item.BuildingName);

                    if (alradyExists == false)
                    {
                        list.Add(item);
                    }
                }
            }

            return Json(list.OrderBy(x => x.BuildingName));
        }

        public ActionResult UpdateGrid([DataSourceRequest]DataSourceRequest request, string FANO, string Active, bool OCTE)
        {
            var UserId = _userManager.GetUserId(HttpContext.User);

            if (OCTE == true)
            {
                var list = _courseService.GetAllCoursesByFiscal(FANO);

                DataSourceResult result = list.ToDataSourceResult(request);

                return Json(result);
            }
            else if (Active == "OnlyActive")
            {
                var list = _courseService.GetCoursesByFiscalByUserId(FANO, UserId, true);

                DataSourceResult result = list.ToDataSourceResult(request);

                return Json(result);
            }
            else if (Active == "All")
            {
                var list = _courseService.GetAllCoursesByFiscalByUserId(FANO, UserId);

                DataSourceResult result = list.ToDataSourceResult(request);

                return Json(result);
            }
            else
            {

                return Json(" ");
            }
        }

        public JsonResult UserBuildingFiscalList()
        {
            var UserId = _userManager.GetUserId(HttpContext.User);

            var list = _buildingListService.UserBuildingFiscalListByUserId(UserId);

            return Json(list);
        }

        public ActionResult GetManageCoursePartial(int? CourseId)
        {
            if(CourseId != null)
            {
                return PartialView("/Views/Courses/PartialViews/_manageCourseParital.cshtml");
            }
            else
            {
                return PartialView("/Views/Courses/PartialViews/_manageCourseParital.cshtml");
            }
        }

        public ActionResult GetUserBuildingList([DataSourceRequest]DataSourceRequest request)
        {
            var UserId = _userManager.GetUserId(HttpContext.User);

            var buildinglist = _buildingListService.GetUserBuildingListByUserId(UserId);

            DataSourceResult result = buildinglist.ToDataSourceResult(request);

            return Json(result);

        }

        public ActionResult GetProgramList([DataSourceRequest]DataSourceRequest request,string BuildingNum)
        {
            var programs = _programService.GetProgramsByBuilding(BuildingNum);

            DataSourceResult result = programs.ToDataSourceResult(request);

            return Json(result);

        }

    }
}
