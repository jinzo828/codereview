﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CteisCoreApp.Services;
using CteisCoreApp.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using CteisCoreApp.Models;
using CteisCore.Bl.Services;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCore.Bl.ViewModels.CteisViewModels;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using CteisCore.Bl.Services.DomainServices;
using System.Security.Claims;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CteisCoreApp.Controllers.Admin
{
    [Authorize(Roles = "CTEISFiscAgent")]
    public class FiscalAgentController : Controller
    {

        private readonly UserManagementService _userManagemnetService;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserService _userService;
        private readonly CEPDService _cepdService;
        private readonly CEPDListService _cepdListService;
        private readonly EntityService _entityService;
        private readonly FiscalAgentListService _fiscalAgentListService;
        private readonly BuildingListService _buildingListService;
        private readonly CollegeProgramsService _collegeProgramsService;
        private readonly BuildingService _buildingService;
        private readonly ProgramService _programService;

        public FiscalAgentController(UserManagementService userManagementService, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserService userService, CEPDService cepdService, CEPDListService cepdListService, EntityService entityService, FiscalAgentListService fiscalAgentListService, BuildingListService buildingListService, CollegeProgramsService collegeProgramsService, BuildingService buildingService, ProgramService programService)
        {
            _userManagemnetService = userManagementService;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
            _cepdService = cepdService;
            _cepdListService = cepdListService;
            _entityService = entityService;
            _fiscalAgentListService = fiscalAgentListService;
            _buildingListService = buildingListService;
            _collegeProgramsService = collegeProgramsService;
            _buildingService = buildingService;
            _programService = programService;
        }

        // GET: /<controller>/
        public IActionResult ManageUsers()
        {
            var userId = _userManager.GetUserId(HttpContext.User);

            var fiscalAgencies = _fiscalAgentListService.GetFaUserListByUserId(userId);

            return View(fiscalAgencies);
        }

        //Returns the List of Fa
        public JsonResult GetFaList()
        {
            var userId = _userManager.GetUserId(HttpContext.User);

            var list = _fiscalAgentListService.GetFaUserListByUserId(userId);

            return Json(list);
        }

        public JsonResult GetAllUsersByFiscal(string FANO)
        {
            var list = _userService.GetAllUsersByFiscal(FANO);

            return Json(list);
        }

        public JsonResult GetAllUsersByFiscalForCTEISFunding(string FANO)
        {
            var list = _userService.GetAllUsersByFanoForCteisFunding(FANO);

            return Json(list);
        }

        public JsonResult GetAllUsersByFanoForCTEISExpenditures(string FANO)
        {
            var list = _userService.GetAllUsersByFANOForCTEISExpenditures(FANO);

            return Json(list);
        }

        //Returns a list of users by last name
        public JsonResult SearchUsersByLastName(string LastName)
        {
            var list = _userService.GetAllUsersByLastName(LastName);

            return Json(list);
        }

        public JsonResult GetAllUsersByFanoForCTEISPrograms(string FANO)
        {
            var list = _userService.GetAllUsersByFanoForCTEISPrograms(FANO);

            return Json(list);
        }

        public JsonResult GetAllUsersByFanoForCTEISFollowup(string FANO)
        {
            var list = _userService.GetAllUsersByFanoForCTEISFollowup(FANO);

            return Json(list);
        }

        public JsonResult GetAllUsersByFanoForCTEISReadOnly(string FANO)
        {
            var list = _userService.GetAllUsersByFanoForCTEISReadOnly(FANO);

            return Json(list);
        }

        public JsonResult GetAllUsersByFanoForCTEISEconDist(string FANO)
        {
            var list = _userService.GetAllUsersByFanoForCTEISEconDist(FANO);

            return Json(list);
        }

        //Returns a list of Users Building List
        public JsonResult GetUserBuildingsByUserIdAndFANO(string UserId, string FANO)
        {
                var list = _buildingListService.GetUserBuildingListByUserIdAndFANO(UserId, FANO);

                return Json(list);
        }

        //Get Buildings by FANO
        public JsonResult GetBuildingListByFANO(string FANO)
        {
            var list = _buildingService.GetBuildingListByFiscal(FANO);

            return Json(list);
        }

        //View for creating and updating user
        public ActionResult CreateUserView(string UserId, string Fiscal)
        {
            if (UserId != null)
            {
                var userView = _userService.GetVmUserView(UserId);
                userView.FANO = Fiscal;
                return PartialView("/Views/FiscalAgent/PartialViews/_CreateUserView.cshtml", userView);
            }
            else
            {
                vmUserView model = new vmUserView();
                vmUser user = new vmUser();
                model.user = user;
                model.FANO = Fiscal;
                return PartialView("/Views/FiscalAgent/PartialViews/_CreateUserView.cshtml", model);
            }
        }

        public async Task<ActionResult> CreateUser(vmUserView model)
        {
            vmValidationMessage message = new vmValidationMessage();

            if (model.user.Email != null && model.user.FirstName != null && model.user.LastName != null && model.user.MeisNumber != null)
            {
                var check = _userManagemnetService.CheckForDuplicateMeiss(model.user.MeisNumber);

                if (check != true)
                {

                    var status = await _userManagemnetService.CreateUser(model.user.MeisNumber, model.user.FirstName, model.user.LastName, model.user.Title, model.user.Email, model.user.PhoneNumber, model.user.SchoolName);

                    if (status == "Success")
                    {

                        var user = _userService.GetUserByMEISS(model.user.MeisNumber);

                        if (user != null)
                        {
                            await _userManagemnetService.AddUserClaims(user.Id, model.user.FirstName, model.user.LastName);

                            List<string> Roles = new List<string>();
                            Roles.Add("CTEISFiscAgent");

                            _fiscalAgentListService.CreateUserFa(user.Id, model.FANO);

                            if (model.level == "Level1")
                            {
                                _userManagemnetService.UpdateUserLevel("1", user.Id);
                                Roles.Add("User");
                            }
                            if (model.level == ("Level2"))
                            {
                                _userManagemnetService.UpdateUserLevel("2", user.Id);

                                Roles.Add("CTEISReadOnly");

                                if (model.ModuleEnrollment == true)
                                {
                                    Roles.Add("CTEISFunding");
                                }
                                if (model.ModuleExpenditures == true)
                                {
                                    Roles.Add("CTEISExpenditures");
                                }
                                if (model.ModuleFollowUp == true)
                                {
                                    Roles.Add("CTEISFollowUp");
                                }
                                if (model.ModuleProgram == true)
                                {
                                    Roles.Add("CTEISPrograms");
                                }

                                if (model.SelectedBuildings.Count != 0)
                                {
                                    foreach (var item in model.SelectedBuildings)
                                    {
                                        var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                        if (entity != null)
                                        {
                                            _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                        }
                                    }
                                }
                            }
                            if (model.level == ("Level3"))
                            {
                                _userManagemnetService.UpdateUserLevel("3", user.Id);

                                Roles.Add("CTEISUser");

                                if (model.ModuleEnrollment == true)
                                {
                                    Roles.Add("CTEISFunding");
                                }
                                if (model.ModuleExpenditures == true)
                                {
                                    Roles.Add("CTEISExpenditures");
                                }
                                if (model.ModuleFollowUp == true)
                                {
                                    Roles.Add("CTEISFollowUp");
                                }
                                if (model.ModuleProgram == true)
                                {
                                    Roles.Add("CTEISPrograms");
                                }

                                if (model.SelectedBuildings.Count != 0)
                                {
                                    foreach (var item in model.SelectedBuildings)
                                    {
                                        var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                        if (entity != null)
                                        {
                                            _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                        }
                                    }
                                }
                            }
                            if (model.level == ("Level4"))
                            {
                                _userManagemnetService.UpdateUserLevel("4", user.Id);

                                Roles.Add("CTEISEconDis");

                                if (model.ModuleEnrollment == true)
                                {
                                    Roles.Add("CTEISFunding");
                                }
                                if (model.ModuleExpenditures == true)
                                {
                                    Roles.Add("CTEISExpenditures");
                                }
                                if (model.ModuleFollowUp == true)
                                {
                                    Roles.Add("CTEISFollowUp");
                                }
                                if (model.ModuleProgram == true)
                                {
                                    Roles.Add("CTEISPrograms");
                                }

                                if (model.SelectedBuildings.Count != 0)
                                {
                                    foreach (var item in model.SelectedBuildings)
                                    {
                                        var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                        if (entity != null)
                                        {
                                            _buildingListService.CreateUserBuilding(user.Id, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                        }
                                    }
                                }
                            }


                            if (Roles.Count != 0)
                            {
                                foreach (var item in Roles)
                                {
                                    _userManagemnetService.AddRoleToUser(user.Id, item);
                                }
                            }

                            message.Message = model.user.FirstName + " Created";
                            message.Type = "Success";
                            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

                        }
                        else
                        {
                            message.Message = "Error";
                            message.Type = "Error";
                            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                        }

                    }
                    else
                    {
                        message.Message = "Error";
                        message.Type = "Error";
                        return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                    }

                }
                else
                {
                    message.Message = "Meiss Account Already Exists.";
                    message.Type = "Error";
                    ModelState.AddModelError(string.Empty, "Meiss Account Already Exists.");
                    return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                }
            }
            else
            {
                message.Message = "Missing Fields.";

                if (model.user.Email == null)
                {
                    message.Message += "<br /> Email";
                }
                if (model.user.FirstName == null)
                {
                    message.Message += "<br /> FirstName";
                }
                if (model.user.LastName == null)
                {
                    message.Message += "<br /> LastName";
                }
                if (model.user.MeisNumber == null)
                {
                    message.Message += "<br /> Meiss Number";
                }

                message.Type = "Error";
                ModelState.AddModelError(string.Empty, "Missing Key Fields.");
                return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
            }

            message.Message = "Error";
            message.Type = "Error";
            return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
        }


        public async Task<ActionResult> UpdateUser(vmUserView model)
        {

            vmValidationMessage message = new vmValidationMessage();

            if (ModelState.IsValid)
            {
                if (model.user.Email != null && model.user.FirstName != null && model.user.LastName != null && model.user.MeisNumber != null && model.SelectedBuildings != null)
                {
                    var user = _userService.GetUserById(model.user.UserId);

                    if (user != null)
                    {
                        _userManagemnetService.RemoveAllFiscalRolesFromUser(user.UserId);
                        _buildingListService.RemoveAllUserBuildingListByUserId(user.UserId, model.FANO);

                        _userManagemnetService.UpdateUser(model.user.UserId, model.user.SchoolName, model.user.FirstName, model.user.LastName, model.user.MeisNumber, model.user.PhoneNumber, model.user.Title, model.user.Email);

                        _userManagemnetService.RemoveUserClaims(user.UserId);
                        await _userManagemnetService.AddUserClaims(user.UserId, model.user.FirstName, model.user.LastName);

                        if (model.Activate == true)
                        {
                            _userManagemnetService.ActivateUser(user.UserId);
                        }
                        if (model.Deactivate == true)
                        {
                            _userManagemnetService.DeactivateUser(user.UserId);
                        }

                        List<string> Roles = new List<string>();
                        Roles.Add("CTEISFiscAgent");

                        //_fiscalAgentListService.CreateUserFa(user.Id, model.FANO);

                        if (model.level == "Level1")
                        {
                            _userManagemnetService.UpdateUserLevel("1", user.UserId);
                            Roles.Add("User");
                        }
                        if (model.level == ("Level2"))
                        {
                            _userManagemnetService.UpdateUserLevel("2", user.UserId);

                            Roles.Add("CTEISReadOnly");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings.Count != 0)
                            {
                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }
                        }
                        if (model.level == ("Level3"))
                        {
                            _userManagemnetService.UpdateUserLevel("3", user.UserId);

                            Roles.Add("CTEISUser");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings.Count != 0)
                            {
                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }
                        }
                        if (model.level == ("Level4"))
                        {
                            _userManagemnetService.UpdateUserLevel("4", user.UserId);

                            Roles.Add("CTEISEconDis");

                            if (model.ModuleEnrollment == true)
                            {
                                Roles.Add("CTEISFunding");
                            }
                            if (model.ModuleExpenditures == true)
                            {
                                Roles.Add("CTEISExpenditures");
                            }
                            if (model.ModuleFollowUp == true)
                            {
                                Roles.Add("CTEISFollowUp");
                            }
                            if (model.ModuleProgram == true)
                            {
                                Roles.Add("CTEISPrograms");
                            }

                            if (model.SelectedBuildings.Count != 0)
                            {
                                foreach (var item in model.SelectedBuildings)
                                {
                                    var entity = _entityService.GetEntityByEntId(Int32.Parse(item));

                                    if (entity != null)
                                    {
                                        _buildingListService.CreateUserBuilding(user.UserId, entity.EntId, entity.Obno, entity.Fano, entity.Oano);
                                    }
                                }
                            }
                        }


                        if (Roles.Count != 0)
                        {
                            foreach (var item in Roles)
                            {
                                _userManagemnetService.AddRoleToUser(user.UserId, item);
                            }
                        }

                        message.Message = model.user.FirstName + " Updated";
                        message.Type = "Success";
                        return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

                    }

                    message.Message = "Error";
                    message.Type = "Error";
                    return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
                }

                message.Message = "Missing Fields.";

                if (model.user.Email == null)
                {
                    message.Message += "<br /> Email";
                }
                if (model.user.FirstName == null)
                {
                    message.Message += "<br /> FirstName";
                }
                if (model.user.LastName == null)
                {
                    message.Message += "<br /> LastName";
                }
                if (model.user.MeisNumber == null)
                {
                    message.Message += "<br /> Meiss Number";
                }
                if (model.SelectedBuildings == null)
                {
                    message.Message += "<br /> No Buildings Selected for User";
                }

                message.Type = "Error";
                ModelState.AddModelError(string.Empty, "Missing Key Fields.");
                return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);

            }
            else
            {
                message.Message = "Error";
                message.Type = "Error";
                return PartialView("/Views/Shared/_ValidationMessage.cshtml", message);
            }
        }

    }
}
