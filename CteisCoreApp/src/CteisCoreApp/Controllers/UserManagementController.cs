﻿using CteisCoreApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using System.Security.Claims;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using CteisCore.Bl.Services.DomainServices;

namespace CteisCoreApp.Controllers
{
    public class UserManagementController : Controller
    {

        private readonly UserManagementService _userManagemnetService;
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserService _userService;
        private readonly CEPDListService _cepdListService;
        private readonly HandicaptTypeService _handicaptTypeService;
        private readonly RaceEthnicService _raceEthnicService;
        private readonly ExitStatusService _exitStatusService;
        private readonly SendingBuildingService _sendingBuildingService;
        private readonly StudentService _studentService;
        private readonly ProgramService _programService;
        private readonly CourseService _courseService;
        private readonly ProgramEnrollmentService _programEnrollmentService;

        public UserManagementController(UserManagementService userManagementService, ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserService userService, 
            CEPDListService cepdListService, HandicaptTypeService handicapTypeService, RaceEthnicService raceEthnicService, ExitStatusService exitStatusService, SendingBuildingService sendingBuildingService,
            StudentService studentService, ProgramService programService, CourseService courseService, ProgramEnrollmentService programEnrollmentService)
        {
            _userManagemnetService = userManagementService;
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _userService = userService;
            _cepdListService = cepdListService;
            _handicaptTypeService = handicapTypeService;
            _raceEthnicService = raceEthnicService;
            _exitStatusService = exitStatusService;
            _sendingBuildingService = sendingBuildingService;
            _studentService = studentService;
            _programService = programService;
            _courseService = courseService;
            _programEnrollmentService = programEnrollmentService;
        }

        //User List Page
        public ActionResult UsersPage()
        {
            //var users = _userManagemnetService.GetAllUsers();
            var users = _userService.GetUsersList();

            return View(users);
        }

        //CepdAdminList
        public ActionResult UserCEPDAdminList()
        {
            var list = _userService.GetUserCEPDList();

            return View(list);
        }

        //FauserList
        public ActionResult UserFaList()
        {
            var list = _userService.GetUserFalist();

            return View(list);
        }

        //UsrusreBuildingList
        public ActionResult UserBuildingList()
        {
            var list = _userService.GetUserBuildingList();

            return View(list);
        }

        //Returns a list of all Roles
        public List<vmRole> RoleList()
        {
            var roles = _userManagemnetService.GetAllvmRoles();

            return roles;
        }

        //User information Page
        public ActionResult UserInformation(string Id)
        {
            var user = _userService.GetUserById(Id);

            return View(user);
        }

        public async Task<ActionResult> UpdateUserInformation(vmUser user, string Role, string userRoles)
        {
            if (user != null && ModelState.IsValid)
            {
                _userManagemnetService.UpdateUser(user.UserId, user.SchoolName, user.FirstName, user.LastName, user.MeisNumber, user.PhoneNumber, user.Title, user.Email);

                var appUser = _userService.GetApplicationUser(user.UserId);

                if(appUser != null)
                {
                    _userManagemnetService.RemoveUserClaims(user.UserId);

                    await _userManager.AddClaimAsync(appUser, new Claim("FirstName", user.FirstName));
                    await _userManager.AddClaimAsync(appUser, new Claim("LastName", user.LastName));
                }

                if (Role != null)
                {
                    _userManagemnetService.AddRoleToUser(user.UserId, Role);
                }
                if (userRoles != null)
                {
                    _userManagemnetService.AddRoleToUser(user.UserId, userRoles);
                }

                return RedirectToAction("UserInformation", new { Id = user.UserId });
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Error");
                return RedirectToAction("UserInformation", new { Id = user.UserId });
            }
        }

        public ActionResult RemoveRoleFromUser(string UserId, string RoleName)
        {
            _userManagemnetService.RemoveRoleFromUser(UserId, RoleName);

            return RedirectToAction("UserInformation", new { Id = UserId });
        }

        public ActionResult DeleteAllUsers()
        {
            _userManagemnetService.DeleteUsersWithNoRole();

            return RedirectToAction("UsersPage");


        }

        public async Task<ActionResult> CreateAllUsers()
        {

            //var users = (from a in _context.OldTheeeuserssss
            //             select a).ToList();

            //var test = (from a in _context.OldTheeeuserssss
            //            join b in _context.OldusrUser on a.UserName equals b.UserName
            //            select a).ToList();

            //if (users.Count != 0)
            //{
            //    foreach (var item in users)
            //    {
            //        var usr = (from a in _context.OldusrUser
            //                   where a.UserName == item.UserName
            //                   select a).FirstOrDefault();

            //        if (usr != null)
            //        {
            //            ApplicationUser user = new ApplicationUser();
            //            user.UserName = usr != null ? usr.UserName : "None";
            //            user.FirstName = usr != null ? usr.FirstName : "None";
            //            user.LastName = usr != null ? usr.LastName : "None";
            //            user.Level = usr != null ? usr.Lvl : "None";
            //            user.Title = usr != null ? usr.Title : "None";
            //            user.Email = usr != null ? usr.Email : "None";
            //            user.PhoneNumber = usr != null ? usr.Phone : "None";
            //            _userManager.CreateAsync(user, user.UserName).Wait();

            //            var appUser = _userService.GetUserByMEISS(usr.UserName);
            //            if (appUser != null)
            //            {
            //                await _userManagemnetService.AddUserClaims(appUser.Id, usr.FirstName, usr.LastName);
            //            }

            //        }


            //    }

            //    return RedirectToAction("UsersPage");
            //}

            var users = (from a in _context.OldTheeeuserssss
                         select a).ToList();

            foreach(var item in users)
            {
                var usr = (from a in _context.OldusrUser
                           where a.UserName == item.UserName
                           select a).FirstOrDefault();

                if(usr != null)
                {
                    ApplicationUser user = new ApplicationUser();
                    user.UserName = item.UserName != null ? item.UserName : "None";
                    user.FirstName = usr.FirstName != null ? usr.FirstName : "None";
                    user.LastName = usr.LastName != null ? usr.LastName : "None";
                    user.Level = usr.Lvl != null ? usr.Lvl : "None";
                    user.Title = usr.Title != null ? usr.Title : "None";
                    user.Email = usr.Email != null ? usr.Email : "None";
                    user.PhoneNumber = usr.Phone != null ? usr.Phone : "None";
                    user.Active = true;
                    _userManager.CreateAsync(user, user.UserName).Wait();

                    var appUser = _userService.GetUserByMEISS(usr.UserName);
                    if (appUser != null)
                    {
                        await _userManagemnetService.AddUserClaims(appUser.Id, usr.FirstName, usr.LastName);
                    }

                }

            }


            return RedirectToAction("UsersPage");
        }

        public ActionResult AddRoles()
        {

            var users = (from a in _context.Users
                         join b in _context.OldTheeeuserssss on a.UserName equals b.UserName
                         select a).ToList();

            if (users.Count != 0)
            {
                foreach (var item in users)
                {

                    var userRoles = (from a in _context.OldTheusersinroles
                                     join b in _context.OldTheroles on a.RoleId equals b.RoleId
                                     join c in _context.OldTheeeuserssss on a.UserId equals c.UserId
                                     where item.UserName == c.UserName
                                     select b).ToList();

                    if (userRoles.Count != 0)
                    {
                        foreach (var item2 in userRoles)
                        {
                            var baseRole = (from a in _context.Roles
                                            where a.NormalizedName == item2.RoleName.ToUpper()
                                            select a).FirstOrDefault();

                            if (baseRole == null)
                            {
                                IdentityRole role = new IdentityRole();
                                role.Name = item2.RoleName;
                                _roleManager.CreateAsync(role).Wait();

                                var newRole = (from a in _context.Roles
                                               where a.NormalizedName == item2.RoleName.ToUpper()
                                               select a).FirstOrDefault();

                                if (newRole != null)
                                {
                                    _userManager.AddToRoleAsync(item, role.Name).Wait();
                                }
                            }
                            else
                            {
                                var role = (from a in _context.Roles
                                            where a.NormalizedName == item2.RoleName.ToUpper()
                                            select a).FirstOrDefault();

                                if (role != null)
                                {
                                    _userManager.AddToRoleAsync(item, role.Name).Wait();
                                }
                            }
                        }
                    }
                }

                return RedirectToAction("UsersPage");
            }

            return RedirectToAction("UsersPage");
        }

        public ActionResult RolesPage()
        {
            var roles = _userManagemnetService.GetAllvmRoles();

            return View(roles);
        }

        public ActionResult CollegePrograms()
        {
            var collegeprograms = _userService.GetAllCollegeUsersPrograms();

            return View(collegeprograms);
        }

        public ActionResult CreateNewRole(string Role)
        {
            if(Role != null)
            {
                _userManagemnetService.CreateNewRole(Role);
            }

            return RedirectToAction("RolesPage");
        }

        public ActionResult DeleteRole(string RoleId)
        {
            if(RoleId != null)
            {
                _userManagemnetService.DeleteRole(RoleId);
            }

            return RedirectToAction("RolesPage");
        }

        public ActionResult DeleteUser(string Id)
        {
            _userManagemnetService.DeleteUser(Id);

            return RedirectToAction("UsersPage");
        }

        //List for User Information Page
        public ActionResult User_CEPDList([DataSourceRequest]DataSourceRequest request, string UserId)
        {
            var UsersList = _userService.GetUserCEPDListByUserId(UserId);

            DataSourceResult result = UsersList.ToDataSourceResult(request);

            return Json(result);
        }

        //List for User INformation Page
        public ActionResult User_FaList([DataSourceRequest]DataSourceRequest request, string UserId)
        {
            var UsersList = _userService.GetUserFalistByUserId(UserId);

            DataSourceResult result = UsersList.ToDataSourceResult(request);

            return Json(result);
        }

        //List for User INformation Page
        public ActionResult User_BuildingList([DataSourceRequest]DataSourceRequest request, string UserId)
        {
            var UsersList = _userService.GetUsersBuildingListByUserId(UserId);

            DataSourceResult result = UsersList.ToDataSourceResult(request);

            return Json(result);
        }

        //Return a list of College User programs by Id
        public ActionResult User_CollegePrograms([DataSourceRequest]DataSourceRequest request, string UserId)
        {
            var UsersList = _userService.GetCollegeProgramsyUserId(UserId);

            DataSourceResult result = UsersList.ToDataSourceResult(request);

            return Json(result);
        }

        //Returns the Look UP View
        public ActionResult LookUpItems()
        {
            return View();
        }

        //Grid for handicap types
        public ActionResult LkpHandicapType_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _handicaptTypeService.GetAllHandicapTypes();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Grid for Race Ethnic
        public ActionResult LkpRaceEthnic_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _raceEthnicService.GetAllRaceEthnics();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Grid for Exit Status
        public ActionResult LkpExitStatus_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _exitStatusService.GetAllExitStatus();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //View for sending Buildings
        public ActionResult SendingBuildings()
        {
            return View();
        }


        //Grid for SendingBuildings
        public ActionResult SendingBuildilngs_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _sendingBuildingService.GetAllSendingBuildings();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Returns student view
        public ActionResult Students()
        {
            return View();
        }

        //Get all Students
        public ActionResult Students_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _studentService.GetAllStudents();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Program View
        public ActionResult Programs()
        {
            return View();
        }


        //Get all Programs
        public ActionResult Programs_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _programService.GetAllPrograms();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Courses View
        public ActionResult Courses()
        {
            return View();
        }

        //Get all Courses
        public ActionResult Courses_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _courseService.GetAllCourses();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }

        //Program Enrollments View
        public ActionResult ProgramEnrollments()
        {
            return View();
        }

        //Get all Program Enrollments
        public ActionResult ProgramEnrollments_Read([DataSourceRequest]DataSourceRequest request)
        {
            var list = _programEnrollmentService.GetAllProgramEnrollments();

            DataSourceResult result = list.ToDataSourceResult(request);

            return Json(result);
        }
    }
}
