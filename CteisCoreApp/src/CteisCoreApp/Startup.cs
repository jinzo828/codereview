﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using CteisCoreApp.Services;
using CteisCore.Bl.Services;
using Newtonsoft.Json.Serialization;
using CteisCore.Bl.Services.DomainServices;
using Microsoft.AspNetCore.DataProtection;
using System.IO;

namespace CteisCoreApp
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 1;
            })

                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            services.AddKendo();

            //Proectction Test
            //services.AddDataProtection().PersistKeysToFileSystem(new DirectoryInfo(@"\\67.227.242.136\share\directory\"));

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
            services.AddTransient<MeisServiceReference.ServicePassExternalAppClient>();
            services.AddTransient<UserManagementService>();

            services.AddTransient<UserService>();
            services.AddTransient<CEPDListService>();
            services.AddTransient<FiscalAgentListService>();
            services.AddTransient<BuildingListService>();
            services.AddTransient<CollegeProgramsService>();
            services.AddTransient<CEPDService>();
            services.AddTransient<CEPDListService>();
            services.AddTransient<EntityService>();
            services.AddTransient<BuildingService>();
            services.AddTransient<ProgramService>();
            services.AddTransient<StudentService>();
            services.AddTransient<ExitStatusService>();
            services.AddTransient<HandicaptTypeService>();
            services.AddTransient<RaceEthnicService>();
            services.AddTransient<SendingBuildingService>();
            services.AddTransient<ProgramEnrollmentService>();
            services.AddTransient<CourseService>();
            services.AddTransient<EnrollmentService>();
            services.AddTransient<CourseStaffService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseDeveloperExceptionPage();

            app.UseStaticFiles();

            app.UseIdentity();

            // Add external authentication middleware below. To configure them please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Configure Kendo UI
            app.UseKendo(env);
        }
    }
}
