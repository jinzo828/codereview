﻿using System;
using System.Collections.Generic;
using System.Text;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Threading.Tasks;
using CteisCore.Data.Models;
using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.Services.DomainServices;

namespace CteisCoreApp.Services
{
   public class UserManagementService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly CEPDListService _cepdListService;
        private readonly FiscalAgentListService _fiscalAgentListService;
        private readonly BuildingListService _buildingListService;
        private readonly CollegeProgramsService _collegeProgramsService;

        public UserManagementService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, CEPDListService cepdListService, FiscalAgentListService fiscalAgentListService, BuildingListService buildingListService, CollegeProgramsService collegeProgramsService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _cepdListService = cepdListService;
            _fiscalAgentListService = fiscalAgentListService;
            _buildingListService = buildingListService;
            _collegeProgramsService = collegeProgramsService;
        }

        public List<vmRole> GetAllRolesForUserById(string UserId)
        {
            var roles = (from a in _context.UserRoles
                         join b in _context.Roles on a.RoleId equals b.Id
                         where a.UserId == UserId
                         select new vmRole
                         {
                             RoleName = b.Name,
                             RoleId = b.Id,
                             UserId = a.UserId
                         }).ToList();

            return roles;
        }


        public void AddRoleToUser(string UserId, string Role)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {
                var baseRole = (from a in _context.Roles
                                where a.Name == Role
                                select a).FirstOrDefault();

                if(baseRole == null)
                {
                    if (!_roleManager.RoleExistsAsync(Role).Result)
                    {
                        IdentityRole role = new IdentityRole();
                        role.Name = Role;
                        var re = _roleManager.CreateAsync(role);
                        re.Wait();
                        if (re.IsCompleted)
                        {

                        }
                    }
                }

                _userManager.AddToRoleAsync(user, Role).Wait();
            }
        }

        //Remove role from user
        public void RemoveRoleFromUser(string UserId, string Role)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {

                _userManager.RemoveFromRoleAsync(user, Role).Wait();
            }
        }

        //Remove All Roles From User
        public void RemoveAllRolesFromUser(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {
                var roles = (from a in _context.Roles
                             join b in _context.UserRoles on a.Id equals b.RoleId
                             where b.UserId == UserId
                             select a).ToList();

                if(roles.Count != 0)
                {
                    foreach(var item in roles)
                    {
                        _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                    }
                }
            }
        }

        //Remove All Fiscal Roles From User
        public void RemoveAllFiscalRolesFromUser(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {
                var roles = (from a in _context.Roles
                             join b in _context.UserRoles on a.Id equals b.RoleId
                             where b.UserId == UserId
                             select a).ToList();

                if (roles.Count != 0)
                {
                    foreach (var item in roles)
                    {
                        if(item.Name == "User")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISReadOnly")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISUser")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISEconDist")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISFunding")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISExpenditures")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISFollowup")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                        if(item.Name == "CTEISPrograms")
                        {
                            _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                        }
                    }
                }
            }
        }

        public List<IdentityRole> GetAllRoles()
        {
            var roles = (from a in _context.Roles
                         select a).ToList();

            return roles;
        }

        public List<vmUser> GetUsersListByRoleName(string RoleName)
        {
            var role = (from a in _context.Roles
                        where a.Name == RoleName
                        select a).FirstOrDefault();

            List<vmUser> Users = new List<vmUser>();

            if (role != null)
            {
                Users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where b.RoleId == role.Id
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             Role = c.Name,
                             SchoolName = a.SchoolName
                         }).ToList();


            }

            return Users;
        }

        public void UpdateUser(string UserId, string SchoolName, string FirstName, string LastName, string MeisNumber, string PhoneNumber, string Title, string Email)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if(user != null)
            {
                user.FirstName = FirstName;
                user.LastName = LastName;
                user.UserName = MeisNumber;
                user.PhoneNumber = PhoneNumber;
                user.Title = Title;
                user.SchoolName = SchoolName;
                user.Email = Email;

                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();

            }
        }

        //public async void DeleteUserAsync(string Id)
        //{
        //    var user = await _userManager.FindByIdAsync(Id);
        //    var rolesForUser = await _userManager.GetRolesAsync(user);

        //    //using (var transaction = _context.Database.BeginTransaction())
        //    //{

        //    if (rolesForUser.Count != 0)
        //    {
        //        foreach (var item in rolesForUser.ToList())
        //        {
        //            // item should be the name of the role
        //            var result = await _userManager.RemoveFromRoleAsync(user, item);
        //        }
        //    }

        //    await _userManager.DeleteAsync(user);
        //    //transaction.Commit();
        //    //}
        //}

        public List<vmRole> GetAllvmRoles()
        {
            var roles = (from a in _context.Roles
                         select new vmRole {
                             RoleId = a.Id,
                             RoleName = a.Name
                         }).ToList();

            return roles;
        }

        //Add role to Role Store
        public void CreateNewRole(string RoleName)
        {

            var baseRole = (from a in _context.Roles
                            where a.Name == RoleName
                            select a).FirstOrDefault();


            if(baseRole == null)
            {
                if (!_roleManager.RoleExistsAsync(RoleName).Result)
                {
                    IdentityRole role = new IdentityRole();
                    role.Name = RoleName;
                    var re = _roleManager.CreateAsync(role);
                    re.Wait();
                    if (re.IsCompleted)
                    {

                    }
                }
            }
        }

        //Removes Role from role store
        public void DeleteRole(string RoleId)
        {
            var role = (from a in _context.Roles
                        where a.Id == RoleId
                        select a).FirstOrDefault();

            if(role != null)
            {
                var usersInRole = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where c.Id == role.Id
                                   select a).ToList();

                if(usersInRole.Count != 0)
                {
                    foreach(var item in usersInRole)
                    {
                        _userManager.RemoveFromRoleAsync(item, role.Name).Wait();
                    }
                }

                _context.Entry(role).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Removes all users with no role
        public void DeleteUsersWithNoRole()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId into bc
                         from b in bc.DefaultIfEmpty()
                         join c in _context.Roles on b.RoleId equals c.Id into cb
                         from c in cb.DefaultIfEmpty()
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             Role = c != null ? c.Name : "None",
                             SchoolName = a.SchoolName
                         }).ToList();

            if(users.Count != 0)
            {
                foreach(var item in users)
                {
                    if(!item.MeisNumber.Equals("54321"))
                    {

                        var user = (from a in _context.Users
                                    where a.Id == item.UserId
                                    select a).FirstOrDefault();

                        if (user != null)
                        {
                            var roles = (from a in _context.UserRoles
                                         join b in _context.Roles on a.RoleId equals b.Id
                                         where user.Id == a.UserId
                                         select b).ToList();

                            if (roles.Count != 0)
                            {
                                foreach (var item2 in roles)
                                {
                                    _context.Entry(item2).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                                    _context.SaveChanges();
                                }
                            }


                            _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                            _context.SaveChanges();

                        }

                    }
                }
            }
        }

        //Deletes a user using Applicationuser Id
        public void DeleteUser(string Id)
        {
            var user = (from a in _context.Users
                        where a.Id == Id
                        select a).FirstOrDefault();

            if(user != null)
            {
                var roles = (from a in _context.UserRoles
                             join b in _context.Roles on a.RoleId equals b.Id
                             where user.Id == a.UserId
                             select b).ToList();

                if(roles.Count != 0)
                {
                    foreach(var item in roles)
                    {
                        _userManager.RemoveFromRoleAsync(user, item.Name).Wait();
                    }
                }

                _cepdListService.RemoveAllUserCepdAdminByUserId(user.Id);
                _fiscalAgentListService.RemoveAllUserFaListByUserId(user.Id);
                _buildingListService.RemoveAllUserBuildingListByUserId(user.Id);
                _collegeProgramsService.RemoveAllCollegeProgramsListByUserId(user.Id);

                _userManager.DeleteAsync(user).Wait();

            }
        }

        //Remove All User Claims
        public void RemoveUserClaims(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if(user != null)
            {
                var claims = (from a in _context.UserClaims
                              where a.UserId == user.Id
                              select a).ToList();

                if(claims.Count != 0)
                {
                    user.Claims.ToList().ForEach(claim => _context.Entry(claim).State = Microsoft.EntityFrameworkCore.EntityState.Deleted);

                    _context.SaveChanges();
                }
            }
        }

        public async Task AddUserClaims(string UserId, string FirstName, string LastName)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {
                await _userManager.AddClaimAsync(user, new Claim("FirstName", FirstName));
                await _userManager.AddClaimAsync(user, new Claim("LastName", LastName));
            }
        }

        //creates identity user
        public async Task<string> CreateUser(string MeisNumber, string FirstName, string LastName, string Title, string Email, string PhoneNumber, string SchoolName)
        {
            string ret = "fail";
            ApplicationUser user = new ApplicationUser();
            user.UserName = MeisNumber;
            user.FirstName = FirstName;
            user.LastName = LastName;
            user.Title = Title;
            user.Email = Email;
            user.PhoneNumber = PhoneNumber;
            user.SchoolName = SchoolName;
            user.Active = true;
            var result = await _userManager.CreateAsync(user, user.UserName);

            if (result.Succeeded)
            {
                if (!_roleManager.RoleExistsAsync("NormalUser").Result)
                {
                    IdentityRole role = new IdentityRole();
                    role.Name = "NormalUser";
                    var re = await _roleManager.CreateAsync(role);
                    if (!re.Succeeded)
                    {

                    }
                }

                _userManager.AddToRoleAsync(user, "NormalUser").Wait();
                ret = "Success";
            }

            return ret;
        }

        //checks if meiss number is already used
        public bool CheckForDuplicateMeiss(string Meiss)
        {
            var user = (from a in _context.Users
                        where a.UserName == Meiss
                        select a).FirstOrDefault();

            if(user != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //update user level
        public void UpdateUserLevel(string level, string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if(user!= null)
            {
                user.Level = level;

                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();

            }
        }

        //Activate user
        public void ActivateUser(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if(user != null)
            {
                user.Active = true;


                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Deactivate user
        public void DeactivateUser(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            if (user != null)
            {
                user.Active = false;


                _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }



    }
}
