﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class EntityService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly BuildingListService _buildingListService;
        private readonly SendingBuildingService _sendingBuildingService;

        public EntityService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, BuildingListService buildingListService,
            SendingBuildingService sendingBuildingService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _buildingListService = buildingListService;
            _sendingBuildingService = sendingBuildingService;
        }

        //Create Entity
        public void CreateEntity(string RegNo, string Cepdno, string Fano, string Oano, string Obno, string BuildingType, string ConsortiumTitle, string ProsperityRegion, string CountyCode,
                                    string Isdcode, string UscongressDist, string MihouseDist, string MisenateDist)
        {
            EntEntity entity = new EntEntity();
            entity.RegNo = RegNo;
            entity.Cepdno = Cepdno;
            entity.Fano = Fano;
            entity.Oano = Oano;
            entity.Obno = Obno;
            entity.BuildingType = BuildingType;
            entity.ConsortiumTitle = ConsortiumTitle;
            entity.ProsperityRegion = ProsperityRegion;
            entity.CountyCode = CountyCode;
            entity.Isdcode = Isdcode;
            entity.UscongressDist = UscongressDist;
            entity.MihouseDist = MihouseDist;
            entity.MisenateDist = MisenateDist;

            _context.EntEntity.Add(entity);
            _context.SaveChanges();
        }

        //Update Entity
        public void UpdateEntity(int EntId, string RegNo, string Cepdno, string Fano, string Oano, string Obno, string BuildingType, string ConsortiumTitle, string ProsperityRegion, string CountyCode,
                                    string Isdcode, string UscongressDist, string MihouseDist, string MisenateDist)
        {
            var entity = (from a in _context.EntEntity
                          where a.EntId == EntId
                          select a).FirstOrDefault();

            if(entity != null)
            {
                entity.RegNo = RegNo;
                entity.Cepdno = Cepdno;
                entity.Fano = Fano;
                entity.Oano = Oano;
                entity.Obno = Obno;
                entity.BuildingType = BuildingType;
                entity.ConsortiumTitle = ConsortiumTitle;
                entity.ProsperityRegion = ProsperityRegion;
                entity.CountyCode = CountyCode;
                entity.Isdcode = Isdcode;
                entity.UscongressDist = UscongressDist;
                entity.MihouseDist = MihouseDist;
                entity.MisenateDist = MisenateDist;

                _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        public void RemoveEntity(int EntId)
        {
            var entity = (from a in _context.EntEntity
                          where a.EntId == EntId
                          select a).FirstOrDefault();

            if(entity != null)
            {
                _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get Fiscal by CEPD
        public IEnumerable<vmDistrict> GetFiscalsByCEPD(string Cepd)
        {
            var list = (from a in _context.EntEntity
                        join b in _context.EntDistrict on a.Fano equals b.DistNo
                        where a.Cepdno == Cepd
                        select new vmDistrict
                        {
                            DistrictName = b.DistrictName,
                            Fano = a.Fano
                        }).GroupBy(i => i.Fano).Select(group => group.First()).ToList();

            return list.OrderBy(i=>i.DistrictName);
        }

        //Get Fiscals and FANO by CEPD
        public IEnumerable<vmDistrict> GetFiscalsandFANOByCEPD(string CEPD)
        {
            var list = (from a in _context.EntEntity
                        join b in _context.EntDistrict on a.Fano equals b.DistNo
                        where a.Cepdno == CEPD
                        orderby a.Fano
                        select new vmDistrict
                        {
                            DistrictName = b.DistrictName + "-" + a.Fano,
                            Fano = a.Fano
                        }).GroupBy(i => i.Fano).Select(group => group.First()).ToList();


            return list.OrderBy(i=>i.DistrictName);
        }

        //Get Fiscals List
        public IEnumerable<vmDistrict> FiscalList()
        {
            var list = (from a in _context.EntEntity
                        join b in _context.EntDistrict on a.Fano equals b.DistNo
                        orderby a.Fano
                        select new vmDistrict
                        {
                            DistrictName = a.Fano + "-" + b.DistrictName,
                            Fano = a.Fano
                        }).GroupBy(i => i.Fano).Select(group => group.First()).ToList();


            return list.OrderBy(i=>i.DistrictName);
        }

        //Get Entity by Ent Id
        public EntEntity GetEntityByEntId(int EntId)
        {
            var entity = (from a in _context.EntEntity
                          where a.EntId == EntId
                          select a).FirstOrDefault();

            return entity;
        }

        //Gets districts by FANO
        public vmDistrict GetDistrictByFano(string FANO)
        {
            var district = (from a in _context.EntDistrict
                            where a.DistNo == FANO
                            select new vmDistrict {
                                DistrictName = a.DistrictName + "-" + a.DistNo,
                                Fano = a.DistNo
                            }).FirstOrDefault();

            return district;
        }

        public vmDistrict GetDistrictByOano(string Oano)
        {
            var district = (from a in _context.EntDistrict
                            join b in _context.EntEntity on a.DistNo equals b.Fano
                            where a.DistNo == Oano
                            select new vmDistrict
                            {
                                DistrictName = a.DistrictName + "-" + a.DistNo,
                                Oano = a.DistNo,
                                EntId = b.EntId
                            }).FirstOrDefault();

            return district;
        }

        public IEnumerable<vmDistrict> SendingFiscalList(string userId)
        {
            var buildingList = _buildingListService.GetUserBuildingListDistrictsByUserId(userId);
            var sendingBuildingList = _sendingBuildingService.GetSendingBuildingDistrictsByUserId(userId);

            List<vmDistrict> districtList = new List<vmDistrict>();

            if(buildingList.Count != 0)
            {
                foreach(var item in buildingList)
                {
                    bool alradyExists = districtList.Any(x=> x.Oano == item.Oano);
                    if(alradyExists == false)
                    {
                        districtList.Add(item);
                    }
                }
            }

            if (sendingBuildingList.Count != 0)
            {
                foreach (var item in sendingBuildingList)
                {
                    bool alradyExists = districtList.Any(x => x.Oano == item.Oano);
                    if (alradyExists == false)
                    {
                        districtList.Add(item);
                    }
                }
            }


            return districtList.OrderBy(i=>i.DistrictName);
        }

    }
}
