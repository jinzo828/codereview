﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class BuildingListService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public BuildingListService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create a record of User Building List
        public void CreateUserBuilding(string UserId, int EntId, string OBNO, string FANO, string OANO)
        {

            var dbBuilding = (from a in _context.UsrUserBldgList
                            where a.EntId == EntId && a.UserId == UserId
                            select a).FirstOrDefault();


            if(dbBuilding == null)
            {
                UsrUserBldgList building = new UsrUserBldgList();

                building.UserId = UserId;
                building.EntId = EntId;
                building.Obno = OBNO;
                building.Fano = FANO;
                building.Oano = OANO;

                _context.UsrUserBldgList.Add(building);
                _context.SaveChanges();
            }

        }

        //Remoe UserBulidingByUser Id
        public void RemoveUserBuildingByUserId(int BldglistId)
        {
            var record = (from a in _context.UsrUserBldgList
                          where a.BldglistId == BldglistId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }


        //Remove all user building list by User Id
        public void RemoveAllUserBuildingListByUserId(string UserId, string FANO)
        {
            var list = (from a in _context.UsrUserBldgList
                        where a.UserId == UserId && a.Fano == FANO
                        select a).ToList();

            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    _context.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    _context.SaveChanges();
                }
            }
        }

        //Remvoes all User Buildings
        public void RemoveAllUserBuildingListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserBldgList
                        where a.UserId == UserId
                        select a).ToList();

            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    _context.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    _context.SaveChanges();
                }
            }
        }

        public List<vmBuilding> GetUserBuildingListByUserIdAndFANO(string UserId, string FANO)
        {
            var buildingList = (from a in _context.UsrUserBldgList
                                join b in _context.Users on a.UserId equals b.Id
                                join c in _context.EntEntity on a.EntId equals c.EntId
                                join d in _context.EntBuilding on c.Obno equals d.BuildNo
                                where b.Id == UserId && c.Fano == FANO
                                select new vmBuilding {
                                    BuildingName = d.BuildName,
                                    Fano = c.Fano,
                                    Obno = c.Obno,
                                    Oano = c.Oano,
                                    BuildingNumber = d.BuildNo,
                                    EntId = c.EntId
                                }).ToList();

            return buildingList;
        }

        public List<vmBuilding> GetUserBuildingListByFANO(string FANO)
        {
            var buildingList = (from a in _context.UsrUserBldgList
                                join b in _context.Users on a.UserId equals b.Id
                                join c in _context.EntEntity on a.EntId equals c.EntId
                                join d in _context.EntBuilding on c.Obno equals d.BuildNo
                                where c.Fano == FANO
                                select new vmBuilding
                                {
                                    BuildingName = d.BuildName,
                                    Fano = c.Fano,
                                    Obno = c.Obno,
                                    Oano = c.Oano,
                                    BuildingNumber = d.BuildNo,
                                    EntId = c.EntId
                                }).ToList();

            return buildingList;
        }

        //Returns buildings by UserId
        public List<vmBuilding> GetUserBuildingListByUserId(string UserId)
        {
            var buildingList = (from a in _context.UsrUserBldgList
                                join b in _context.Users on a.UserId equals b.Id
                                join c in _context.EntEntity on a.EntId equals c.EntId
                                join d in _context.EntBuilding on c.Obno equals d.BuildNo
                                join e in _context.EntDistrict on c.Fano equals e.DistNo
                                where b.Id == UserId
                                select new vmBuilding
                                {
                                    BuildingName = d.BuildName + "-" + d.BuildNo,
                                    Fano = c.Fano,
                                    Obno = c.Obno,
                                    Oano = c.Oano,
                                    BuildingNumber = d.BuildNo,
                                    Cepd = c.Cepdno,
                                    District = "District",
                                    Status = "??",
                                    FiscalAgency = "??"
                                }).GroupBy(i => i.BuildingNumber).Select(group => group.First()).ToList();

            return buildingList;
        }

        //Get Building List Districts
        public List<vmDistrict> GetUserBuildingListDistrictsByUserId(string UserId)
        {
            var list = (from b in _context.UsrUserBldgList
                        join c in _context.EntEntity on b.Obno equals c.Obno
                        join d in _context.EntDistrict on c.Oano equals d.DistNo
                        join e in _context.Users on b.UserId equals e.Id
                        select new vmDistrict
                        {
                            DistrictName = d.DistrictName + "-" + d.DistNo,
                            Oano = d.DistNo
                        }).GroupBy(i => i.Oano).Select(group => group.First()).ToList();

            return list;
        }

        //Get Users Fiscal List By UserId
        public IEnumerable<vmDistrict> UserBuildingFiscalListByUserId(string UserId)
        {
            var list = (from a in _context.EntDistrict
                        join b in _context.UsrUserBldgList on a.DistNo equals b.Fano
                        join c in _context.Users on b.UserId equals c.Id
                        where c.Id == UserId
                        select new vmDistrict
                        {
                            DistrictName = a.DistrictName + "-" + a.DistNo,
                            Fano = b.Fano
                        }).GroupBy(i => i.Fano).Select(group => group.First()).ToList();

            return list.OrderBy(i=>i.DistrictName);
        }

    }
}
