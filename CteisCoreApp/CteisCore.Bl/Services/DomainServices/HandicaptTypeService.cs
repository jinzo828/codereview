﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class HandicaptTypeService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public HandicaptTypeService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Creates a new record for handicaptype
        public void CreateHandicapType(string HandicapType, string HandicapDesc)
        {
            LkpHandicapType newHandicapType = new LkpHandicapType();
            newHandicapType.HandicapType = HandicapType;
            newHandicapType.HandicapDesc = HandicapDesc;

            _context.LkpHandicapType.Add(newHandicapType);
            _context.SaveChanges();

        }

        //Updates a handicap record
        public void UpdateHandicapType(int HandicapTypeId, string HandicapType, string HandicapDesc)
        {
            var record = (from a in _context.LkpHandicapType
                          where a.HandicaptTypeId == HandicapTypeId
                          select a).FirstOrDefault();

            if(record != null)
            {
                record.HandicapType = HandicapType;
                record.HandicapDesc = HandicapDesc;

                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Removes a record Handicap Type
        public void RemoveHandicapType(int HandicapTypeId)
        {
            var record = (from a in _context.LkpHandicapType
                          where a.HandicaptTypeId == HandicapTypeId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get all handicapTypes
        public List<LkpHandicapType> GetAllHandicapTypes()
        {
            var list = (from a in _context.LkpHandicapType
                        select a).ToList();

            return list;
        }

        //Returns handicapt type by Id
        public LkpHandicapType GetHandicapTypeByType(string HandicapType)
        {
            var handicapType = (from a in _context.LkpHandicapType
                                where a.HandicapType == HandicapType
                                select a).FirstOrDefault();

            return handicapType;
        }

        //Get Handicap Desc
        public string GetHandicapDescription(string HandicapType)
        {
            var handicapType = (from a in _context.LkpHandicapType
                                where a.HandicapType == HandicapType
                                select a.HandicapDesc).FirstOrDefault();

            return handicapType;
        }

    }
}
