﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class SendingBuildingService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public SendingBuildingService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create new Record of Sending Building
        public void CreateSendingBuilding(int OperEntId, string SendObNo)
        {
            EntSendingBuilding sendBuilding = new EntSendingBuilding();
            sendBuilding.OperEntId = OperEntId;
            sendBuilding.SendObNo = SendObNo;

            _context.EntSendingBuilding.Add(sendBuilding);
            _context.SaveChanges();
        }

        //Update Sending Building
        public void UpdateSendingBuilding(int EntSendingBuildingId, int OperEntId, string SendObNo)
        {
            var record = (from a in _context.EntSendingBuilding
                          where a.EntSendingBuildingId == EntSendingBuildingId
                          select a).FirstOrDefault();

            if(record != null)
            {
                record.OperEntId = OperEntId;
                record.SendObNo = SendObNo;

                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove SendingBuilding
        public void RemoveSendingBuilding(int EntSendingBuildingId)
        {
            var record = (from a in _context.EntSendingBuilding
                          where a.EntSendingBuildingId == EntSendingBuildingId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get all sendingBuilings
        public List<vmBuilding> GetAllSendingBuildings()
        {
            var list = (from a in _context.EntSendingBuilding
                        join b in _context.EntBuilding on a.SendObNo equals b.BuildNo
                        join c in _context.EntEntity on a.OperEntId equals c.EntId
                        select new vmBuilding {
                            BuildingName = b.BuildName,
                            Fano = c.Fano,
                            Obno = c.Obno,
                            Oano = c.Oano,
                            BuildingNumber = b.BuildNo,
                            EntId = c.EntId
                        }).ToList();

            return list;
        }

        //Get Sending Building LIst By UserId
        public List<vmBuilding> GetSendingBuildingListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserBldgList
                        join b in _context.EntSendingBuilding on a.EntId equals b.OperEntId
                        join c in _context.EntBuilding on b.SendObNo equals c.BuildNo
                        join e in _context.Users on a.UserId equals e.Id
                        join f in _context.EntEntity on a.EntId equals f.EntId
                        where e.Id == UserId
                        select new vmBuilding
                        {
                            BuildingName = c.BuildName + "-" + c.BuildNo,
                            Fano = f.Fano,
                            Obno = f.Obno,
                            Oano = f.Oano,
                            BuildingNumber = c.BuildNo,
                            EntId = f.EntId
                        }).GroupBy(i => i.BuildingNumber).Select(group => group.First()).ToList();

            return list;
        }

        //Get sending Builing Districts
        public List<vmDistrict> GetSendingBuildingDistrictsByUserId(string UserId)
        {
            var list = (from a in _context.EntSendingBuilding
                        join b in _context.UsrUserBldgList on a.OperEntId equals b.EntId
                        join c in _context.EntEntity on a.SendObNo equals c.Obno
                        join d in _context.EntDistrict on c.Oano equals d.DistNo
                        join e in _context.Users on b.UserId equals e.Id
                        select new vmDistrict
                        {
                            DistrictName = d.DistrictName + "-" + d.DistNo,
                            Oano = d.DistNo
                        }).Distinct().GroupBy(i => i.Oano).Select(group => group.First()).ToList();

            return list;
        }
    }
}
