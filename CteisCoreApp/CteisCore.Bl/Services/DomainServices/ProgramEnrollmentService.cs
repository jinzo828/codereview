﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CteisCore.Bl.Services.DomainServices
{
    public class ProgramEnrollmentService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ExitStatusService _exitStatusService;
        private readonly RaceEthnicService _ethnicService;
        private readonly HandicaptTypeService _handicapeTypeService;
        private readonly ProgramService _programService;
        private readonly CourseService _courseService;
        private readonly EnrollmentService _enrollmentService;

        public ProgramEnrollmentService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, ExitStatusService exitStatusService,
            RaceEthnicService ethnicService, HandicaptTypeService handicapTypeService, ProgramService programService, CourseService courseService, EnrollmentService enrollmentService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _exitStatusService = exitStatusService;
            _ethnicService = ethnicService;
            _handicapeTypeService = handicapTypeService;
            _programService = programService;
            _courseService = courseService;
            _enrollmentService = enrollmentService;
        }

        //Create a record of ProgramEnrollment
        public void CreateProgramEnrollment()
        {

        }

        //Update a reocrd of Program Enrollment
        public void UpdateProgramEnrollmentReocrd()
        {

        }

        //Remove Program Enrollment Record
        public void RemoveProgramEnrollmentRecord()
        {

        }

        //Get Program Enrollments by StudentUIC
        public List<vmProgramEnrollment> GetStudentProgramEnrollments(string UIC)
        {
            var studentEnrollments = (from a in _context.EnrProgramEnrollment
                                      join b in _context.EnrProgram on a.Psn equals b.Psn
                                      where a.StudentUic == UIC
                                      select new vmProgramEnrollment {
                                          Peid = a.Peid,
                                          Psn = a.Psn,
                                          StudentUic = a.StudentUic,
                                          Comments = a.Comments,
                                          CompStat = a.CompStat,
                                          StatChangeDate = a.StatChangeDate,
                                          TechPrep = a.TechPrep,
                                          NonTrad = a.NonTrad,
                                          Assessment = a.Assessment,
                                          AssessScore = a.AssessScore,
                                          AssessMaxScore = a.AssessMaxScore,
                                          AssessPf = a.AssessPf,
                                          Concentrator = a.Concentrator,
                                          Segment1 = a.Segment1,
                                          Segment2 = a.Segment2,
                                          Segment3 = a.Segment3,
                                          Segment4 = a.Segment4,
                                          Segment5 = a.Segment5,
                                          Segment6 = a.Segment6,
                                          Segment7 = a.Segment7,
                                          Segment8 = a.Segment8,
                                          Segment9 = a.Segment9,
                                          Segment10 = a.Segment10,
                                          Segment11 = a.Segment11,
                                          Segment12 = a.Segment12,
                                          Segment13 = a.Segment12,
                                          LastCourseDate = a.LastCourseDate,
                                          ReportCheck = a.ReportCheck,
                                          AccessYear = a.AccessYear,
                                          TotMin = a.TotMin,
                                          TotSegGr2 = a.TotSegGr2
                                      }).Distinct().GroupBy(i => i.Psn).Select(group => group.First()).ToList();

            if(studentEnrollments.Count != 0)
            {
                foreach(var item in studentEnrollments)
                {
                    item.Program = _programService.GetProgramByPsn(item.Psn);
                    item.Courses = _enrollmentService.GetCourseListByPSn(UIC, item.Psn);
                }
            }

            return studentEnrollments;
        }

        //Get all Program Enrollments
        public List<vmProgramEnrollment> GetAllProgramEnrollments()
        {
            var studentEnrollments = (from a in _context.EnrProgramEnrollment
                                      select new vmProgramEnrollment
                                      {
                                          Peid = a.Peid,
                                          Psn = a.Psn,
                                          StudentUic = a.StudentUic,
                                          Comments = a.Comments,
                                          CompStat = a.CompStat,
                                          StatChangeDate = a.StatChangeDate,
                                          TechPrep = a.TechPrep,
                                          NonTrad = a.NonTrad,
                                          Assessment = a.Assessment,
                                          AssessScore = a.AssessScore,
                                          AssessMaxScore = a.AssessMaxScore,
                                          AssessPf = a.AssessPf,
                                          Concentrator = a.Concentrator,
                                          Segment1 = a.Segment1,
                                          Segment2 = a.Segment2,
                                          Segment3 = a.Segment3,
                                          Segment4 = a.Segment4,
                                          Segment5 = a.Segment5,
                                          Segment6 = a.Segment6,
                                          Segment7 = a.Segment7,
                                          Segment8 = a.Segment8,
                                          Segment9 = a.Segment9,
                                          Segment10 = a.Segment10,
                                          Segment11 = a.Segment11,
                                          Segment12 = a.Segment12,
                                          Segment13 = a.Segment12,
                                          LastCourseDate = a.LastCourseDate,
                                          ReportCheck = a.ReportCheck,
                                          AccessYear = a.AccessYear,
                                          TotMin = a.TotMin,
                                          TotSegGr2 = a.TotSegGr2
                                      }).ToList();

            return studentEnrollments;
        }

    }
}
