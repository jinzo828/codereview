﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class ExitStatusService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ExitStatusService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create new Exit Status
        public void CreateExitStatus(string ExitStatusId, string ExitStatusDesc)
        {
            LkpExitStatus newStatus = new LkpExitStatus();
            newStatus.ExitStatusId = ExitStatusId;
            newStatus.ExitStatusId = ExitStatusDesc;

            _context.LkpExitStatus.Add(newStatus);
            _context.SaveChanges();
        }

        //Update Exit Status
        public void UpdateExitStatus(string ExitStatusId, string ExitStatusDesc)
        {
            var record = (from a in _context.LkpExitStatus
                          where a.ExitStatusId == ExitStatusId
                          select a).FirstOrDefault();

            if(record != null)
            {
                record.ExitStatusId = ExitStatusId;
                record.ExitStatusDesc = ExitStatusDesc;

                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove Exit Status
        public void RemoveExitStatus(int lkpExitStatusId)
        {
            var record = (from a in _context.LkpExitStatus
                          where a.LkpExitStatusId == lkpExitStatusId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }

        }

        //Returns all Exit Statuses
        public List<LkpExitStatus> GetAllExitStatus()
        {
            var list = (from a in _context.LkpExitStatus
                        select a).ToList();

            return list;
        }

        //Gets Exit Status by Id
        public LkpExitStatus GetExitStatusById(string ExitStatusId)
        {
            var exitStatus = (from a in _context.LkpExitStatus
                              where a.ExitStatusId == ExitStatusId
                              select a).FirstOrDefault();

            return exitStatus;
        }

        //Get ExitStatus String
        public string GetExitStatusDesc(string ExitStatusId)
        {
            var exitStatus = (from a in _context.LkpExitStatus
                              where a.ExitStatusId == ExitStatusId
                              select a.ExitStatusDesc).FirstOrDefault();

            return exitStatus;
        }

    }
}
