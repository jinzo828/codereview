﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CteisCore.Bl.Services.DomainServices
{
    public class CourseService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly CourseStaffService _courseStaffService;

        public CourseService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager,
            CourseStaffService courseStaffService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _courseStaffService = courseStaffService;
        }

        //Create Course Record
        public void CreateCourse()
        {

        }

        //Update Course Record
        public void UpdateCourse()
        {

        }

        //Remove Course Record
        public void RemoveCourse()
        {

        }

        //Returns a Course by CourseId
        public vmCourse GetCourseByCourseId(int CourseId)
        {
            var course = (from a in _context.EnrCourse
                          where a.CourseId == CourseId
                          select new vmCourse {
                              CourseId = a.CourseId,
                              CourseSectionCode = a.CourseSectionCode,
                              CourseName = a.CourseName,
                              CrsBeginDate = a.CrsBeginDate,
                              CrsEndDate = a.CrsEndDate,
                              CourseHour = a.CourseHour,
                              Semester = a.Semester,
                              CourseType = a.CourseType,
                              NumberOfWeeks = a.NumberOfWeeks,
                              MinutesPerWeek = a.MinutesPerWeek,
                              RoomNumber = a.RoomNumber,
                              Cepdno = a.Cepdno,
                              FaNo = a.FaNo,
                              OaNo = a.OaNo,
                              ObNo = a.ObNo,
                              Psn = a.Psn,
                              Cipcode = a.Cipcode,
                              CourseNumber = a.CourseNumber,
                              CourseActive = a.CourseActive,
                              EligType = a.EligType,
                              EligTypeCert = a.EligTypeCert,
                              VirtualDelivery = a.VirtualDelivery
                          }).FirstOrDefault();

            return course;
        }

        //Get All Courses
        public List<vmCourse> GetAllCourses()
        {
            var list = (from a in _context.EnrCourse
                        select new vmCourse
                        {
                            CourseId = a.CourseId,
                            CourseSectionCode = a.CourseSectionCode,
                            CourseName = a.CourseName,
                            CrsBeginDate = a.CrsBeginDate,
                            CrsEndDate = a.CrsEndDate,
                            CourseHour = a.CourseHour,
                            Semester = a.Semester,
                            CourseType = a.CourseType,
                            NumberOfWeeks = a.NumberOfWeeks,
                            MinutesPerWeek = a.MinutesPerWeek,
                            RoomNumber = a.RoomNumber,
                            Cepdno = a.Cepdno,
                            FaNo = a.FaNo,
                            OaNo = a.OaNo,
                            ObNo = a.ObNo,
                            Psn = a.Psn,
                            Cipcode = a.Cipcode,
                            CourseNumber = a.CourseNumber,
                            CourseActive = a.CourseActive,
                            EligType = a.EligType,
                            EligTypeCert = a.EligTypeCert,
                            VirtualDelivery = a.VirtualDelivery
                        }).ToList();

            return list;
        }

        //Get users course list 
        public IEnumerable<vmCourse> GetAllCoursesByFiscalByUserId(string Fano, string UserId)
        {
            var list = (from a in _context.EnrCourse
                        join b in _context.UsrUserBldgList on a.FaNo equals b.Fano
                        where a.FaNo == Fano && b.UserId == UserId
                        select new vmCourse
                        {
                            CourseId = a.CourseId,
                            CourseSectionCode = a.CourseSectionCode,
                            CourseName = a.CourseName,
                            CrsBeginDate = a.CrsBeginDate,
                            CrsEndDate = a.CrsEndDate,
                            CourseHour = a.CourseHour,
                            Semester = a.Semester,
                            CourseType = a.CourseType,
                            NumberOfWeeks = a.NumberOfWeeks,
                            MinutesPerWeek = a.MinutesPerWeek,
                            RoomNumber = a.RoomNumber,
                            Cepdno = a.Cepdno,
                            FaNo = a.FaNo,
                            OaNo = a.OaNo,
                            ObNo = a.ObNo,
                            Psn = a.Psn,
                            Cipcode = a.Cipcode,
                            CourseNumber = a.CourseNumber,
                            CourseActive = a.CourseActive,
                            EligType = a.EligType,
                            EligTypeCert = a.EligTypeCert,
                            VirtualDelivery = a.VirtualDelivery
                        }).GroupBy(i => i.CourseId).Select(group => group.First()).ToList();

            return list.OrderBy(i => i.Psn);
        }

        //Get Course By Fiscal with Active or Deactive
        public IEnumerable<vmCourse> GetCoursesByFiscalByUserId(string Fano, string UserId, bool Active)
        {
            var list = (from a in _context.EnrCourse
                        join b in _context.UsrUserBldgList on a.FaNo equals b.Fano
                        where a.FaNo == Fano && b.UserId == UserId && a.CourseActive == Active
                        select new vmCourse {
                            CourseId = a.CourseId,
                            CourseSectionCode = a.CourseSectionCode,
                            CourseName = a.CourseName,
                            CrsBeginDate = a.CrsBeginDate,
                            CrsEndDate = a.CrsEndDate,
                            CourseHour = a.CourseHour,
                            Semester = a.Semester,
                            CourseType = a.CourseType,
                            NumberOfWeeks = a.NumberOfWeeks,
                            MinutesPerWeek = a.MinutesPerWeek,
                            RoomNumber = a.RoomNumber,
                            Cepdno = a.Cepdno,
                            FaNo = a.FaNo,
                            OaNo = a.OaNo,
                            ObNo = a.ObNo,
                            Psn = a.Psn,
                            Cipcode = a.Cipcode,
                            CourseNumber = a.CourseNumber,
                            CourseActive = a.CourseActive,
                            EligType = a.EligType,
                            EligTypeCert = a.EligTypeCert,
                            VirtualDelivery = a.VirtualDelivery
                        }).GroupBy(i => i.CourseId).Select(group => group.First()).ToList();

            return list.OrderBy(i=>i.Psn);
        }

        //Get ALl Courses by FANo
        public IEnumerable<vmCourse> GetAllCoursesByFiscal(string Fano)
        {
            var list = (from a in _context.EnrCourse
                        where a.FaNo == Fano
                        select new vmCourse
                        {
                            CourseId = a.CourseId,
                            CourseSectionCode = a.CourseSectionCode,
                            CourseName = a.CourseName,
                            CrsBeginDate = a.CrsBeginDate,
                            CrsEndDate = a.CrsEndDate,
                            CourseHour = a.CourseHour,
                            Semester = a.Semester,
                            CourseType = a.CourseType,
                            NumberOfWeeks = a.NumberOfWeeks,
                            MinutesPerWeek = a.MinutesPerWeek,
                            RoomNumber = a.RoomNumber,
                            Cepdno = a.Cepdno,
                            FaNo = a.FaNo,
                            OaNo = a.OaNo,
                            ObNo = a.ObNo,
                            Psn = a.Psn,
                            Cipcode = a.Cipcode,
                            CourseNumber = a.CourseNumber,
                            CourseActive = a.CourseActive,
                            EligType = a.EligType,
                            EligTypeCert = a.EligTypeCert,
                            VirtualDelivery = a.VirtualDelivery,
                            //Staff = _courseStaffService.GetPrimaryCourseStaff(a.CourseId)
                        }).ToList();

            return list.OrderBy(i => i.Psn);
        }

        //Get Courses by OBNO
        public IEnumerable<vmCourse> GetCoursesByObno(string Obno)
        {
            var list = (from a in _context.EnrCourse
                        where a.ObNo == Obno
                        select new vmCourse
                        {
                            CourseId = a.CourseId,
                            CourseSectionCode = a.CourseSectionCode,
                            CourseName = a.CourseName,
                            CrsBeginDate = a.CrsBeginDate,
                            CrsEndDate = a.CrsEndDate,
                            CourseHour = a.CourseHour,
                            Semester = a.Semester,
                            CourseType = a.CourseType,
                            NumberOfWeeks = a.NumberOfWeeks,
                            MinutesPerWeek = a.MinutesPerWeek,
                            RoomNumber = a.RoomNumber,
                            Cepdno = a.Cepdno,
                            FaNo = a.FaNo,
                            OaNo = a.OaNo,
                            ObNo = a.ObNo,
                            Psn = a.Psn,
                            Cipcode = a.Cipcode,
                            CourseNumber = a.CourseNumber,
                            CourseActive = a.CourseActive,
                            EligType = a.EligType,
                            EligTypeCert = a.EligTypeCert,
                            VirtualDelivery = a.VirtualDelivery,
                        })/*.GroupBy(i => i.ObNo).Select(group => group.First())*/.ToList();

            return list.OrderBy(i => i.CourseName);
        }

    }
}
