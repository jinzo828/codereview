﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class BuildingService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public BuildingService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create Building
        public void CreateBuilding(string BuildNo, string BuildName, string Eeno, bool? SendBldg, bool? OperBldg, DateTime? ClosedBldgDate, string BuildingType,
                                string ConsortiumType, string CountyCode, string ProsperityCode, string Longitude, string Latitude)
        {
            EntBuilding EntBuilding = new EntBuilding();
            EntBuilding.BuildNo = BuildNo;
            EntBuilding.BuildNo = BuildNo;
            EntBuilding.Eeno = Eeno;
            EntBuilding.SendBldg = SendBldg;
            EntBuilding.OperBldg = OperBldg;
            EntBuilding.ClosedBldgDate = ClosedBldgDate;
            EntBuilding.BuildingType = BuildingType;
            EntBuilding.ConsortiumType = ConsortiumType;
            EntBuilding.CountyCode = CountyCode;
            EntBuilding.ProsperityCode = ProsperityCode;
            EntBuilding.Longitude = Longitude;
            EntBuilding.Latitude = Latitude;

            _context.EntBuilding.Add(EntBuilding);
            _context.SaveChanges();
        }

        //Update Building
        public void UpdateBuilding(string BuildNo, string BuildName, string Eeno, bool? SendBldg, bool? OperBldg, DateTime? ClosedBldgDate, string BuildingType,
                                string ConsortiumType, string CountyCode, string ProsperityCode, string Longitude, string Latitude)
        {
            var entBuilding = (from a in _context.EntBuilding
                               where a.BuildNo == BuildNo
                               select a).FirstOrDefault();


            if(entBuilding != null)
            {
                entBuilding.BuildNo = BuildNo;
                entBuilding.BuildNo = BuildNo;
                entBuilding.Eeno = Eeno;
                entBuilding.SendBldg = SendBldg;
                entBuilding.OperBldg = OperBldg;
                entBuilding.ClosedBldgDate = ClosedBldgDate;
                entBuilding.BuildingType = BuildingType;
                entBuilding.ConsortiumType = ConsortiumType;
                entBuilding.CountyCode = CountyCode;
                entBuilding.ProsperityCode = ProsperityCode;
                entBuilding.Longitude = Longitude;
                entBuilding.Latitude = Latitude;

                _context.Entry(entBuilding).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove Building
        public void RemoveBuilding(string BuildNo)
        {
            var building = (from a in _context.EntBuilding
                            where a.BuildNo == BuildNo
                            select a).FirstOrDefault();

            if(building != null)
            {
                _context.Entry(building).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get Buildings By Fiscal
        public List<vmBuilding> GetBuildingListByFiscal(string Fiscal)
        {
            var buildings = (from a in _context.EntBuilding
                             join b in _context.EntEntity on a.BuildNo equals b.Obno
                             join c in _context.EntDistrict on b.Fano equals c.DistNo
                             where c.DistNo == Fiscal && a.BuildNo != null
                             orderby c.DistNo
                             select new vmBuilding
                             {
                                 BuildingName = a.BuildName + "-" + a.BuildNo,
                                 Fano = b.Fano,
                                 Obno = b.Obno,
                                 Oano = b.Oano,
                                 BuildingNumber = a.BuildNo
                             }).OrderBy(i=>i.BuildingName).ToList();

            return buildings;
        }

        //Get Buildings by OANO
        public List<vmBuilding> GetBuildingListByEntId(int EntId)
        {
            var buildings = (from a in _context.EntBuilding
                             join b in _context.EntEntity on a.BuildNo equals b.Obno
                             join c in _context.EntDistrict on b.Fano equals c.DistNo
                             where b.EntId == EntId
                             orderby c.DistNo
                             select new vmBuilding
                             {
                                 BuildingName = a.BuildName + " " + a.BuildNo,
                                 Fano = b.Fano,
                                 Obno = b.Obno,
                                 Oano = b.Oano,
                                 BuildingNumber = a.BuildNo,
                                 EntId = b.EntId
                             }).ToList();

            return buildings;
        }

        //Get builgind by building number
        public EntBuilding GetBuildingByBuildingNumber(string BuildingNumber)
        {
            var building = (from a in _context.EntBuilding
                            where a.BuildNo == BuildingNumber
                            select a).FirstOrDefault();

            return building;
        }

        //Returns a list of entBuildings
        public List<EntBuilding> GetAllBuildings()
        {
            var list = (from a in _context.EntBuilding
                        select a).ToList();

            return list;
        }

        //Get Buildings By OaNo
        public List<vmBuilding> GetBuildingsByOano(string Oano)
        {
            var list = (from a in _context.EntBuilding
                        join b in _context.EntEntity on a.BuildNo equals b.Obno
                        where b.Oano == Oano /*&& b.Obno != "00000"*/
                        select new vmBuilding {
                            BuildingName = a.BuildName + "-" + b.Obno,
                            BuildingNumber = b.Obno
                        }).Distinct().GroupBy(i => i.BuildingNumber).Select(group => group.First()).ToList();

            return list;
        }

    }
}
