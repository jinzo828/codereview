﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CteisCore.Bl.Services.DomainServices
{
    public class StudentService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ExitStatusService _exitStatusService;
        private readonly RaceEthnicService _ethnicService;
        private readonly HandicaptTypeService _handicapeTypeService;

        public StudentService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, ExitStatusService exitStatusService,
            RaceEthnicService ethnicService, HandicaptTypeService handicapTypeService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _exitStatusService = exitStatusService;
            _ethnicService = ethnicService;
            _handicapeTypeService = handicapTypeService;
        }

        //Create a student record
        public string CreateStudent(string UIC, string LastName, string FirstName, string MiddleInital, DateTime DateOfBirth, string Gender, string Address1, string Address2,
            string City, string ZipCode, string Phone1, string Phone2, string Email, string SendingFacility, string SendingDistrict, bool? SingleParent, bool? DisplacedHomemaker, string UpdatedBy, DateTime UpdatedDate)
        {

            try
            {
                _context.Database.ExecuteSqlCommand("dbo.CreateStudent @p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18", UIC, LastName, FirstName, MiddleInital, DateOfBirth, Gender, Address1,
               Address2, City, ZipCode, Phone1, Phone2, Email, SendingDistrict, SendingFacility, SingleParent, DisplacedHomemaker, UpdatedDate, "19");
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "Success";

        }

        //Update a student Record
        public string UpdateStudent(string UIC, string LastName, string FirstName, string MiddleInital, DateTime DateOfBirth, string Gender, string Address1, string Address2,
            string City, string ZipCode, string Phone1, string Phone2, string Email, string SendingFacility, string SendingDistrict, bool? SingleParent, bool? DisplacedHomemaker, string UpdatedBy, DateTime UpdatedDate)
        {

            try
            {
                _context.Database.ExecuteSqlCommand("dbo.UpdateStudent @p0, @p1, @p2, @p3, @p4, @p5, @p6, @p7, @p8, @p9, @p10, @p11, @p12, @p13, @p14, @p15, @p16, @p17, @p18", UIC, LastName, FirstName, MiddleInital, DateOfBirth, Gender, Address1,
               Address2, City, ZipCode, Phone1, Phone2, Email, SendingDistrict, SendingFacility, SingleParent, DisplacedHomemaker, UpdatedDate, "19");
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "Success";

        }

        //Remove a Student record
        public void RemoveStudent(string UIC)
        {
            var student = (from a in _context.EnrStudent
                           where a.Uic == UIC
                           select a).FirstOrDefault();

            if(student != null)
            {
                _context.Entry(student).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Returns a view model of Student
        public vmStudent GetVmStudent(string StudentUIC)
        {
            var dbStudent = _context.EnrStudent.FromSql("GetStudentByUIC @p0", StudentUIC).FirstOrDefault();

            if(dbStudent != null)
            {
                vmStudent student = new vmStudent();
                student.StudentUIC = dbStudent.Uic;
                student.FirstName = dbStudent.FirstName;
                student.LastName = dbStudent.LastName;
                student.MiddleInital = dbStudent.MiddleInit;
                student.DateOfBirth = dbStudent.Dob;
                student.Gender = dbStudent.Gender;
                student.Grade = dbStudent.Grade;
                student.RaceEthnic = _ethnicService.GetRaceEthnicDescription(dbStudent.RaceEthnic);
                student.Migrant = dbStudent.Migrant;
                student.ExitStatus = _exitStatusService.GetExitStatusDesc(dbStudent.ExitStatus);
                student.Disabled = _handicapeTypeService.GetHandicapDescription(dbStudent.HandicapType);
                student.EconDisadvanged = dbStudent.Disadvantaged;
                student.LEP = dbStudent.Lep;
                student.SingleParent = dbStudent.SingleParent;
                student.DisplacedHomemaker = dbStudent.DisplacedHomemaker;
                student.SendingDistrict = dbStudent.SendingDist;
                student.SendingFacility = dbStudent.SendingBldg;
                student.Address1 = dbStudent.Address1;
                student.Address2 = dbStudent.Address2;
                student.State = dbStudent.AddrState;
                student.City = dbStudent.City;
                student.ZipCode = dbStudent.ZipCode;
                student.Phone1 = dbStudent.Phone1;
                student.Phone2 = dbStudent.Phone2;
                student.Email = dbStudent.Email;
                student.UICStatus = dbStudent.ErrorReason;
                student.UICStatusDate = dbStudent.UiccheckDate;
                student.MSDSLastUpdated = dbStudent.SrsdUpdated;
                return student;
            }
            else
            {
                vmStudent student = new vmStudent();
                return student;
            }
            
        }

        //Returns a list of students by building
        public List<vmStudent> GetStudentsBySendingFacility(string sendingBuilding)
        {
            var Students = _context.EnrStudent.FromSql("GetStudentsByBuilding @p0", sendingBuilding).ToList();

            var students = (from a in Students
                            join b in _context.EntBuilding on a.SendingBldg equals b.BuildNo
                            //where a.SendingBldg == sendingBuilding
                            select new vmStudent
                            {
                                StudentUIC = a.Uic,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                MiddleInital = a.MiddleInit,
                                DateOfBirth =  a.Dob,
                                Gender = a.Gender,
                                Grade = a.Grade,
                                SendingFacility = b.BuildName + "-" + a.SendingBldg,
                                CanView = true,
                                LeftSchool = a.ExitStatus.Equals("19") ? false : true
                            }).ToList();

            return students;
        }

        //Returns a list of students by building with left school
        public List<vmStudent> GetStudentsBySendingFacilityLeftSchool(string sendingBuilding)
        {
            var Students = _context.EnrStudent.FromSql("GetAllStudentsByBuilding @p0", sendingBuilding).ToList();

            var students = (from a in Students
                            join b in _context.EntBuilding on a.SendingBldg equals b.BuildNo
                            //where a.SendingBldg == sendingBuilding
                            select new vmStudent
                            {
                                StudentUIC = a.Uic,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                MiddleInital = a.MiddleInit,
                                DateOfBirth = a.Dob,
                                Gender = a.Gender,
                                Grade = a.Grade,
                                SendingFacility = b.BuildName + "-" + a.SendingBldg,
                                CanView = true,
                                LeftSchool = a.ExitStatus.Equals("19") ? false : true
                            }).ToList();

            return students;
        }

        //Chcek for duplicate UIC
        public bool UICAlreadyExsists(string UIC)
        {
            var student = (from a in _context.EnrStudent
                           where a.Uic == UIC
                           select a).FirstOrDefault();

            if(student != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Returns a list of Studenst by UIC
        public List<vmStudent> GetStudentByUIC(string UIC, string UserId)
        {
            var dbStudent = _context.EnrStudent.FromSql("GetStudentByUIC @p0", UIC).ToList();
            //var dbStudents = _context.EnrStudent.FromSql("GetUsersStudentList @p0", UserId).ToList();

            var students = (from a in dbStudent
                            join b in _context.EntBuilding on a.SendingBldg equals b.BuildNo
                            select new vmStudent
                            {
                                StudentUIC = a.Uic,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                MiddleInital = a.MiddleInit,
                                DateOfBirth = a.Dob,
                                Gender = a.Gender,
                                Grade = a.Grade,
                                SendingFacility = b.BuildName + "-" + a.SendingBldg,
                                CanView = CanView(a.Uic, UserId),
                                LeftSchool = a.ExitStatus.Equals("19") ? false : true
                            }).ToList();

            return students;
        }

        //Check if user can view Student
        public bool CanView(string UIC, string UserId)
        {
            var canView = (from a in _context.UsrUserBldgList
                           join b in _context.EnrStudent on a.Obno equals b.SendingBldg
                           where b.Uic == UIC && a.UserId == UserId
                           select a).FirstOrDefault();

            if(canView == null)
            {
                bool View = false;
                return View;
            }
            else
            {
                bool View = true;
                return View;
            }
        }

        //Get students By lastName (For Fiscal Agent)
        public IEnumerable<vmStudent> GetStudentByLastName(string LastName, string UserId)
        {
            //var Students = _context.EnrStudent.FromSql("GetStudentsByLastName @p0, @p1", LastName, UserId).ToList();
            var dbStudents = _context.EnrStudent.FromSql("GetUsersStudentList @p0", UserId).ToList();

            var students = (from a in dbStudents
                            join b in _context.EntBuilding on a.SendingBldg equals b.BuildNo
                            select new vmStudent
                            {
                                StudentUIC = a.Uic,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                MiddleInital = a.MiddleInit,
                                DateOfBirth = a.Dob,
                                Gender = a.Gender,
                                Grade = a.Grade,
                                SendingFacility = b.BuildName + "-" + a.SendingBldg,
                                CanView = true,
                                LeftSchool = a.ExitStatus.Equals("19") ? false : true
                            }).Where(c => c.LastName.ToLower().Contains(LastName.ToLower())).ToList();

            return students.OrderBy(i=>i.SendingFacility);

        }

        //Get All students
        public List<EnrStudent> GetAllStudents()
        {
            var dbStudents = _context.EnrStudent.FromSql("GetStudents").ToList();

            return dbStudents;
        }

        //Get all students by UIC
        public List<vmStudent> GetAllStudentsByUIC(string UIC)
        {
            var dbStudent = _context.EnrStudent.FromSql("GetStudentByUIC @p0", UIC).ToList();

            var students = (from a in dbStudent
                            join b in _context.EntBuilding on a.SendingBldg equals b.BuildNo
                            select new vmStudent
                            {
                                StudentUIC = a.Uic,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                MiddleInital = a.MiddleInit,
                                DateOfBirth = a.Dob,
                                Gender = a.Gender,
                                Grade = a.Grade,
                                SendingFacility = b.BuildName + "-" + a.SendingBldg,
                                CanView = true,
                                LeftSchool = a.ExitStatus.Equals("19") ? false : true
                            }).ToList();



            return students;
        }
    }
    }
