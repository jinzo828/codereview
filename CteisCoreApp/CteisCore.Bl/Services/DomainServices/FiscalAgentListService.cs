﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class FiscalAgentListService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public FiscalAgentListService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create a record of UsrUserFaList
        public void CreateUserFa(string UserId, string Fano)
        {
            var dbFa = (from a in _context.UsrUserFalist
                        where a.Fano == Fano && a.UserId == UserId
                        select a).FirstOrDefault();

            if(dbFa == null)
            {
                UsrUserFalist usrFa = new UsrUserFalist();
                usrFa.UserId = UserId;
                usrFa.Fano = Fano;

                _context.UsrUserFalist.Add(usrFa);
                _context.SaveChanges();
            }
        }

        //Remove User Fa
        public void RemoveUserFa(int FalistId)
        {
            var fa = (from a in _context.UsrUserFalist
                      where a.FalistId == FalistId
                      select a).FirstOrDefault();

            if(fa != null)
            {
                _context.Entry(fa).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Remove all User Fa list By User Id
        public void RemoveAllUserFaListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserFalist
                        where a.UserId == UserId
                        select a).ToList();

            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    _context.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    _context.SaveChanges();
                }
            }
        }

        //Gets User Fa List By UserId
        public List<vmDistrict> GetFaUserListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserFalist
                        join b in _context.EntDistrict on a.Fano equals b.DistNo
                        where a.UserId == UserId
                        select new vmDistrict
                        {
                            DistrictName = a.Fano + " " + b.DistrictName,
                            Fano = a.Fano
                        }).ToList();

            return list;
        }

        //Get usrUser Fiscals by UserId
        public List<vmDistrict> GetFiscalsByCEPDByUserId(string UserId)
        {
            var list = (from a in _context.EntEntity
                        join b in _context.EntDistrict on a.Fano equals b.DistNo
                        join c in _context.UsrUserFalist on a.Fano equals c.Fano
                        where c.UserId == UserId
                        select new vmDistrict
                        {
                            DistrictName = b.DistrictName,
                            Fano = a.Fano
                        }).Distinct().GroupBy(i => i.Fano).Select(group => group.First()).ToList();

            return list;
        }

    }
}
