﻿using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using CteisCore.Bl.ViewModels.CteisViewModels;

namespace CteisCore.Bl.Services.DomainServices
{
    public class CEPDListService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public CEPDListService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }


        //Create record of usr Cepd
        public void CreateUserCEPD(string UserId, string Cepdno)
        {
            var dbCepd = (from a in _context.UsrCepdlist
                          where a.Cepdno == Cepdno && a.UserId == UserId
                          select a).FirstOrDefault();

            if(dbCepd == null)
            {
                UsrCepdlist userCepd = new UsrCepdlist();
                userCepd.UserId = UserId;
                userCepd.Cepdno = Cepdno;

                _context.UsrCepdlist.Add(userCepd);
                _context.SaveChanges();
            }
        }

        //Remove User CEPD
        public void RemoveUserCEPD(int CepdlistId)
        {
            var record = (from a in _context.UsrCepdlist
                          where a.CepdlistId == CepdlistId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Update User CEPD
        public void UpdateUserCEPD(int CepdlistId, string UserId, string Cepdno)
        {
            var record = (from a in _context.UsrCepdlist
                          where a.CepdlistId == CepdlistId
                          select a).FirstOrDefault();

            if(record != null)
            {
                record.Cepdno = Cepdno;
                record.UserId = UserId;

                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove all User Cepd list by User Id
        public void RemoveAllUserCepdAdminByUserId(string UserId)
        {
            var list = (from a in _context.UsrCepdlist
                        where a.UserId == UserId
                        select a).ToList();

            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    _context.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    _context.SaveChanges();
                }
            }
        }

        //Reutns a list of User CEPD List
        public List<EntCepd> GetCEPDListByUserId(string UserId)
        {
            var list = (from a in _context.EntCepd
                        join b in _context.UsrCepdlist on a.Cepdno equals b.Cepdno
                        where b.UserId == UserId
                        orderby a.Cepdno
                        select a).ToList();

            return list;
        }



    }
    }
