﻿using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCoreApp.Services;

namespace CteisCore.Bl.Services.DomainServices
{
    public class UserService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly UserManagementService _userManagementService;

        public UserService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, UserManagementService userManagementService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _userManagementService = userManagementService;
        }

        //Gets all users with roles, will have duplicates
        public List<vmUser> GetAllUsers()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId into bc
                         from b in bc.DefaultIfEmpty()
                         join c in _context.Roles on b.RoleId equals c.Id into cb
                         from c in cb.DefaultIfEmpty()
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             Role = c != null ? c.Name : "None",
                             SchoolName = a.SchoolName
                         }).ToList();

            return users;
        }

        //Get User by MEISS
        public ApplicationUser GetUserByMEISS(string MEISSNumber)
        {
            var user = (from a in _context.Users
                        where a.UserName == MEISSNumber
                        select a).FirstOrDefault();

            return user;
        }

        //Get User By Last Name
        public ApplicationUser GetUserByLastName(string LastName)
        {
            var user = (from a in _context.Users
                        where a.LastName == LastName
                        select a).FirstOrDefault();

            return user;
        }

        //Gets User by Application User Id
        public vmUser GetUserById(string UserId)
        {
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select a).FirstOrDefault();

            vmUser vmuser = new vmUser();

            if (user != null)
            {
                vmuser.Email = user.Email;
                vmuser.FirstName = user.FirstName;
                vmuser.LastName = user.LastName;
                vmuser.MeisNumber = user.UserName;
                vmuser.PhoneNumber = user.PhoneNumber;
                vmuser.SchoolName = user.SchoolName;
                vmuser.Title = user.Title;
                vmuser.UserId = user.Id;

                var roles = _userManagementService.GetAllRolesForUserById(user.Id);
                List<string> userRoles = new List<string>();
                if (roles != null)
                {
                    foreach (var item in roles)
                    {
                        userRoles.Add(item.RoleName);
                    }
                }

                vmuser.Roles = userRoles;

            }

            return vmuser;
        }

        //Get Applcation user
        public ApplicationUser GetApplicationUser(string Id)
        {
            var user = (from a in _context.Users
                        where a.Id == Id
                        select a).FirstOrDefault();

            return user;
        }

        //UsersList no Duplicates
        public List<vmUser> GetUsersList()
        {
            var users = (from a in _context.Users
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             Level = a.Level,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //GEts all users with the role PTDUser and PTDSurv
        public List<vmUser> GetAllSystemUsers()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "PTDUser" || c.Name == "PTDSurv"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Get all Users with ROle OCTPAdmin
        public List<vmUser> GetAllOCTEAdmins()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "OCTPAdmin"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Get all users with Ad Hoc Role
        public List<vmUser> GetAllAdHocUsers()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "OCTEAdHoc"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Get all users with role OCTE TRACS
        public List<vmUser> GetAllOCTETracs()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "OCTETRAC"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }


        //Get all users with role OCTESRCR
        public List<vmUser> GetAllOCTECRCR()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "OCTECRCR"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Get All users with OCTPUser Role
        public List<vmUser> GetAllOCTE()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "OCTPUser"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Get all College Admins and COllege Users
        public List<vmUser> GetAllCollegeUsers()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         where c.Name == "CollegeAdmin" || c.Name == "CollegeUser"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Returns a list of User College Programs
        public List<vmUserCollegeProgram> GetAllCollegeUsersPrograms()
        {
            var users = (from a in _context.Users
                         join d in _context.UsrUserCollegePrograms on a.Id equals d.UserId
                         orderby a.LastName
                         select new vmUserCollegeProgram
                         {
                             UserId = a.Id,
                             MeissNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             Psn = d.Psn,
                             Fano = d.Fano,
                             Oano = d.Oano,
                             Obno = d.Obno,
                             EntId = d.EntId
                         }).ToList();

            return users;
        }

        //Get All Fiscal Agents
        public List<vmUser> GetAllFiscalAgents()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISFiscAgent"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Get User List By Fiscal
        public List<vmUser> GetUsersByFiscal(string Fiscal)
        {
            var users = (from a in _context.Users
                         join b in _context.UsrUserFalist on a.Id equals b.UserId
                         where b.Fano == Fiscal
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = b.Fano
                         }).ToList();

            return users;
        }

        //Get all users by Fiscal
        public List<vmUser> GetAllUsersByFiscal(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISFiscAgent" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Serach Users By Last Name
        //Get all users by Fiscal
        public List<vmUser> GetAllUsersByLastName(string LastName)
        {
            var users = (from a in _context.Users
                         where a.LastName == LastName
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active
                         }).ToList();

            return users;
        }

        //Returns a list of users for role CTIES Funding by FANO
        public List<vmUser> GetAllUsersByFanoForCteisFunding(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISFunding" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Returns a list of users by FANO that are in role CTEIS Programs
        public List<vmUser> GetAllUsersByFanoForCTEISPrograms(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISPrograms" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Returns all users with role of Follow Up
        public List<vmUser> GetAllUsersByFanoForCTEISFollowup(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISFollowup" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Returns a list of users that have role CTEISReadOnly
        public List<vmUser> GetAllUsersByFanoForCTEISReadOnly(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISReadOnly" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Returns a list of users with role EconDis
        public List<vmUser> GetAllUsersByFanoForCTEISEconDist(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISEconDist" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //Retuns all users for CTEIS Expenditures
        public List<vmUser> GetAllUsersByFANOForCTEISExpenditures(string FANO)
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrUserFalist on a.Id equals d.UserId
                         where c.Name == "CTEISExpenditures" && d.Fano == FANO
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Fano
                         }).ToList();

            return users;
        }

        //GEt ALL CEPD Admins
        public List<vmUser> GetAllCepdAdmins()
        {
            var users = (from a in _context.Users
                         join b in _context.UserRoles on a.Id equals b.UserId
                         join c in _context.Roles on b.RoleId equals c.Id
                         join d in _context.UsrCepdlist on a.Id equals d.UserId
                         where c.Name == "CEPDAdmin"
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = d.Cepdno
                         }).ToList();

            return users;
        }

        //Get user list by CEPD
        public List<vmUser> GetUsersByCEPD(string CEPD)
        {
            var users = (from a in _context.Users
                         join b in _context.UsrCepdlist on a.Id equals b.UserId
                         where b.Cepdno == CEPD
                         orderby a.LastName
                         select new vmUser
                         {
                             UserId = a.Id,
                             MeisNumber = a.UserName,
                             LastName = a.LastName,
                             FirstName = a.FirstName,
                             Title = a.Title,
                             PhoneNumber = a.PhoneNumber,
                             Email = a.Email,
                             SchoolName = a.SchoolName,
                             Active = a.Active,
                             FAorCEPD = b.Cepdno
                         }).ToList();

            return users;
        }

        //Get vmUserView
        public vmUserView GetVmUserView(string UserId)
        {
            vmUserView model = new vmUserView();
            var user = (from a in _context.Users
                        where a.Id == UserId
                        select new vmUser
                        {
                            UserId = a.Id,
                            MeisNumber = a.UserName,
                            LastName = a.LastName,
                            FirstName = a.FirstName,
                            Title = a.Title,
                            PhoneNumber = a.PhoneNumber,
                            Email = a.Email,
                            SchoolName = a.SchoolName,
                            Level = a.Level,
                            Active = a.Active
                        }).FirstOrDefault();

            var roles = (from a in _context.UserRoles
                         join b in _context.Roles on a.RoleId equals b.Id
                         where a.UserId == UserId
                         select new vmRole
                         {
                             RoleName = b.Name,
                             RoleId = b.Id,
                             UserId = a.UserId
                         }).ToList();

            var cepds = (from a in _context.UsrCepdlist
                         where a.UserId == UserId
                         select a).ToList();

            var Fas = (from a in _context.UsrUserFalist
                       where a.UserId == UserId
                       select a).ToList();

            var buildings = (from a in _context.UsrUserBldgList
                             where a.UserId == UserId
                             select a).ToList();

            model.user = user;
            model.level = user.Level;

            if (roles.Count != 0)
            {
                foreach (var item in roles)
                {
                    if (item.RoleName == "CTEISUser")
                    {

                    }
                    if (item.RoleName == "CEPDAdmin")
                    {
                        model.CEPD = true;
                    }
                    if (item.RoleName == "CTEISFiscAgent")
                    {
                        model.FiscalAgent = true;
                    }
                    if (item.RoleName == "CTEISFunding")
                    {
                        model.ModuleEnrollment = true;
                    }
                    if (item.RoleName == "CTEISExpenditures")
                    {
                        model.ModuleExpenditures = true;
                    }
                    if (item.RoleName == "CTEISFollowup")
                    {
                        model.ModuleFollowUp = true;
                    }
                    if (item.RoleName == "CTEISPrograms")
                    {
                        model.ModuleProgram = true;
                    }
                    if (item.RoleName == "OCTPReadOnly")
                    {
                        model.manage = "ManagementReadOnly";
                    }
                    if (item.RoleName == "OCTPReportsOnly")
                    {
                        model.manage = "ManagementReportsOnly";
                    }
                    if (item.RoleName == "OCTPEdit")
                    {
                        model.manage = "ManagementEdit";
                    }
                    if (item.RoleName == "OCTPFunding")
                    {
                        model.ManagementFunding = true;
                    }
                    if (item.RoleName == "OCTPExpenditures")
                    {
                        model.ManagementExpenditures = true;
                    }
                    if (item.RoleName == "OCTPFollowUp")
                    {
                        model.ManagementFollowUp = true;
                    }
                    if (item.RoleName == "ProgramReview")
                    {
                        model.ManagementNPReview = true;
                    }
                    if (item.RoleName == "ProgramAdmin")
                    {
                        model.ManagementNPAdmin = true;
                    }
                    if (item.RoleName == "OCTEAdHoc")
                    {
                        model.ReportingADHoc = true;
                    }
                    if (item.RoleName == "OCTETRAC")
                    {
                        model.ReportingTRAC = true;
                    }
                    if (item.RoleName == "OCTECRCR")
                    {
                        model.ReportingCRCR = true;
                    }
                    if (item.RoleName == "OCTPAdmin")
                    {
                        model.SystemCTEISAdmin = true;
                    }
                    if (item.RoleName == "OCTPMastDir")
                    {
                        model.SystemMDAdmin = true;
                    }
                    if (item.RoleName == "CollegeAdmin")
                    {
                        model.college = "CollegeAdmin";
                    }
                    if (item.RoleName == "CollegeUser")
                    {
                        model.college = "CollegeUser";
                    }
                    if (item.RoleName == "PTDUser")
                    {
                        model.PTDUser = true;
                    }
                    if (item.RoleName == "PTDSurv")
                    {
                        model.PTDSurv = true;
                    }
                    if (item.RoleName == "User")
                    {
                        model.User = true;
                    }
                    if (item.RoleName == "CTEISReadOnly")
                    {
                        model.CTEISReadOnly = true;
                    }
                    if (item.RoleName == "CTEISUser")
                    {
                        model.CTEISUser = true;
                    }
                }
            }

            return model;

        }

        //Get UsruserCepdListBy User Id
        public List<vmUserCepdAdmin> GetUserCEPDListByUserId(string UserId)
        {
            var list = (from a in _context.UsrCepdlist
                        join b in _context.Users on a.UserId equals b.Id
                        where b.Id == UserId
                        select new vmUserCepdAdmin
                        {
                            CepdlistId = a.CepdlistId,
                            UserId = a.UserId,
                            Cepdno = a.Cepdno,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName
                        }).ToList();

            return list;
        }

        //Get All User CEPD List
        public List<vmUserCepdAdmin> GetUserCEPDList()
        {
            var list = (from a in _context.UsrCepdlist
                        join b in _context.Users on a.UserId equals b.Id
                        select new vmUserCepdAdmin
                        {
                            CepdlistId = a.CepdlistId,
                            UserId = a.UserId,
                            Cepdno = a.Cepdno,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName
                        }).ToList();

            return list;
        }

        //GetUsrUserFalistBy User Id
        public List<vmUsrUserFalist> GetUserFalistByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserFalist
                        join b in _context.Users on a.UserId equals b.Id
                        where b.Id == UserId
                        select new vmUsrUserFalist
                        {
                            FalistId = a.FalistId,
                            UserId = a.UserId,
                            Fano = a.Fano,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName
                        }).ToList();

            return list;
        }

        //GetUsrUserFalist
        public List<vmUsrUserFalist> GetUserFalist()
        {
            var list = (from a in _context.UsrUserFalist
                        join b in _context.Users on a.UserId equals b.Id
                        select new vmUsrUserFalist
                        {
                            FalistId = a.FalistId,
                            UserId = a.UserId,
                            Fano = a.Fano,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName
                        }).ToList();

            return list;
        }

        //Get users Building list by User Id
        public List<vmUserBuilding> GetUsersBuildingListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserBldgList
                        join b in _context.Users on a.UserId equals b.Id
                        join c in _context.EntBuilding on a.Obno equals c.BuildNo
                        where b.Id == UserId
                        select new vmUserBuilding
                        {
                            BldglistId = a.BldglistId,
                            UserId = b.Id,
                            EntId = a.EntId,
                            Obno = a.Obno,
                            Fano = a.Fano,
                            Oano = a.Oano,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName,
                            BuildingName = c.BuildName
                        }).ToList();

            return list;
        }

        //Get User Building List
        public List<vmUserBuilding> GetUserBuildingList()
        {
            var list = (from a in _context.UsrUserBldgList
                        join b in _context.Users on a.UserId equals b.Id
                        join c in _context.EntBuilding on a.Obno equals c.BuildNo
                        select new vmUserBuilding
                        {
                            BldglistId = a.BldglistId,
                            UserId = b.Id,
                            EntId = a.EntId,
                            Obno = a.Obno,
                            Fano = a.Fano,
                            Oano = a.Oano,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName,
                            BuildingName = c.BuildName
                        }).ToList();

            return list;
        }

        //Get vmUser college programs by User Id
        public List<vmUserCollegeProgram> GetCollegeProgramsyUserId(string UserId)
        {
            var list = (from a in _context.UsrUserCollegePrograms
                        join b in _context.Users on a.UserId equals b.Id
                        join c in _context.EntBuilding on a.Obno equals c.BuildNo
                        join d in _context.EnrProgram on a.Psn equals d.Psn
                        where b.Id == UserId
                        select new vmUserCollegeProgram
                        {
                            CollegeUsrProgramId = a.CollegeUsrProgramId,
                            UserId = b.Id,
                            EntId = a.EntId,
                            Obno = a.Obno,
                            Fano = a.Fano,
                            Oano = a.Oano,
                            FirstName = b.FirstName,
                            LastName = b.LastName,
                            MeissNumber = b.UserName,
                            BuildingName = c.BuildName,
                            ProgramName = d.ProgramName
                        }).ToList();

            return list;
        }

    }
}
