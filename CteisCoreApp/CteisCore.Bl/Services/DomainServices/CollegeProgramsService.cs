﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class CollegeProgramsService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public CollegeProgramsService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create college user program record
        public void CreateUserCollegeProgram(string FaNo, string OaNo, string ObNo, string UserId, int Psn)
        {
            var dbCollegeProgram = (from a in _context.UsrUserCollegePrograms
                                    where a.UserId == UserId && a.Psn == Psn
                                    select a).FirstOrDefault();

            if(dbCollegeProgram == null)
            {
                UsrUserCollegePrograms userCollegeProgram = new UsrUserCollegePrograms();
                userCollegeProgram.Fano = FaNo;
                userCollegeProgram.Oano = OaNo;
                userCollegeProgram.Obno = ObNo;
                userCollegeProgram.UserId = UserId;
                userCollegeProgram.Psn = Psn;

                if (userCollegeProgram != null)
                {
                    _context.UsrUserCollegePrograms.Add(userCollegeProgram);
                    _context.SaveChanges();
                }
            }
        }

        //Remove the College User Program
        public void RemoveUserCollegeProgram(int CollegeUsrProgramId)
        {
            var collegeProgram = (from a in _context.UsrUserCollegePrograms
                                  where a.CollegeUsrProgramId == CollegeUsrProgramId
                                  select a).FirstOrDefault();

            if(collegeProgram != null)
            {
                _context.Entry(collegeProgram).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get a list of college User Programs
        public List<EnrProgram> GetUsrCollegeListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserCollegePrograms
                        join b in _context.Users on a.UserId equals b.Id
                        join c in _context.EnrProgram on a.Psn equals c.Psn
                        where b.Id == UserId
                        select c).ToList();

            return list;
        }

        //Remove all college user programs by UserId
        public void RemoveAllCollegeProgramsListByUserId(string UserId)
        {
            var list = (from a in _context.UsrUserCollegePrograms
                        where a.UserId == UserId
                        select a).ToList();

            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    _context.Entry(item).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                    _context.SaveChanges();
                }
            }
        }

    }
}
