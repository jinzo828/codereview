﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace CteisCore.Bl.Services.DomainServices
{
    public class EnrollmentService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ExitStatusService _exitStatusService;
        private readonly RaceEthnicService _ethnicService;
        private readonly HandicaptTypeService _handicapeTypeService;
        private readonly ProgramService _programService;
        private readonly CourseService _courseService;

        public EnrollmentService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager, ExitStatusService exitStatusService,
            RaceEthnicService ethnicService, HandicaptTypeService handicapTypeService, ProgramService programService, CourseService courseService)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
            _exitStatusService = exitStatusService;
            _ethnicService = ethnicService;
            _handicapeTypeService = handicapTypeService;
            _programService = programService;
            _courseService = courseService;
        }

        //Create Enrollment Record
        public void CreateEnrollment()
        {

        }

        //Update Enrollment Record
        public void UpdateEnrollment()
        {

        }

        //Remove Enrollment Record
        public void RemoveEnrollmentRecord()
        {

        }

        //Returns a list of courses by PSN
        public List<vmCourse> GetCourseListByPSn(string UIC, int? Psn)
        {
            var list = ((from a in _context.EnrEnrollCount
                         join b in _context.EnrEnrollment on new { CourseId = a.CourseId, Reportcheck = a.ReportCheck } equals new { b.CourseId, b.Reportcheck }
                         join c in _context.EnrProgramEnrollment on b.Uic equals c.StudentUic
                         where b.Uic == UIC && a.Psn == Psn && c.Psn == Psn && c.StudentUic == UIC
                         select new vmCourse
                         {
                             CourseId = a.CourseId,
                             CourseSectionCode = a.Csc,
                             CourseName = a.CourseName,
                             CrsBeginDate = a.BegDate,
                             CrsEndDate = a.EndDate,
                             CourseHour = a.CourseHour,
                             Semester = a.Sem,
                             //CourseType = a.cours,
                             //NumberOfWeeks = a,
                             //MinutesPerWeek = a.MinutesPerWeek,
                             //RoomNumber = a.RoomNumber,
                             Cepdno = a.Cepd,
                             FaNo = a.Fano,
                             OaNo = a.Oano,
                             ObNo = a.Obno,
                             Psn = a.Psn,
                             Cipcode = a.Cipcode,
                             //CourseNumber = a.course,
                             //CourseActive = a.active,
                             //EligType = a.EligType,
                             //EligTypeCert = a.EligTypeCert,
                             VirtualDelivery = a.VirtualDelivery,
                             FinalSegments = b.SubSection.Equals("A") ? a.Asegments : b.SubSection.Equals("B") ? a.Bsegments : b.SubSection.Equals("C") ? a.Csegments : b.SubSection.Equals("D") ? a.Dsegments : null,
                             Grade = b.SemesterGrade1,
                             Asegment = a.Asegments,
                             Bsegment = a.Bsegments,
                             Csegment = a.Csegments,
                             Dsegment = a.Dsegments,
                             SubSet = b.SubSection
                         }).Distinct().ToList());

            return list;
        }

    }
}
