﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class CourseStaffService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public CourseStaffService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //create a record of course staff
        public void CreateCourseStaff()
        {

        }

        //UPdate a record of course staff
        public void UpdateCourseStaff()
        {

        }

        //Remove a record of course staff
        public void RemoveCourseStaff()
        {

        }

        //public vmCourseStaff GetPrimaryCourseStaff(int CourseId)
        //{
        //    var staffMember = (from a in _context.EnrCourseStaff
        //                       where a.CourseId == CourseId && a.StaffType == "P"
        //                       select new vmCourseStaff {
        //                           CourseStaffId = a.CourseStaffId,
        //                           CourseId = a.CourseId,
        //                           CourseSectionCode = a.CourseSectionCode,
        //                           StaffType = a.StaffType,
        //                           StaffId = a.StaffId,
        //                           Mentor = a.Mentor,
        //                           Course = _courseStaffService.GetCourseByCourseId(a.CourseId)
        //                       }).FirstOrDefault();

        //    return staffMember;
        //}

    }
}
