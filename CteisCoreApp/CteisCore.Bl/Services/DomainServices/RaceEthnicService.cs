﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class RaceEthnicService
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public RaceEthnicService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Creates a record of RaceEthnic
        public void CreateRaceEthnic(string RaceEthnic, string RaceEthDesc, string EdenRaceEthnic, int SortOrder)
        {
            LkpRaceEthnic newRace = new LkpRaceEthnic();
            newRace.RaceEthnic = RaceEthnic;
            newRace.RaceEthDesc = RaceEthDesc;
            newRace.EdenRaceEthnic = EdenRaceEthnic;
            newRace.SortOrder = SortOrder;

            _context.LkpRaceEthnic.Add(newRace);
            _context.SaveChanges();
        }

        //Updates Race Ethnic Record
        public void UpdateRaceEthnic(int RacehEthnicId, string RaceEthnic, string RaceEthDesc, string EdenRaceEthnic, int SortOrder)
        {
            var record = (from a in _context.LkpRaceEthnic
                          where a.RaceEthnicId == RacehEthnicId
                          select a).FirstOrDefault();

            if(record != null)
            {
                record.RaceEthnic = RaceEthnic;
                record.RaceEthDesc = RaceEthDesc;
                record.EdenRaceEthnic = EdenRaceEthnic;
                record.SortOrder = SortOrder;

                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remoe Race Ethnic Record
        public void RemoveRaceEthnic(int RaceEthnicId)
        {
            var record = (from a in _context.LkpRaceEthnic
                          where a.RaceEthnicId == RaceEthnicId
                          select a).FirstOrDefault();

            if(record != null)
            {
                _context.Entry(record).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Returns a list of all RaceEthnics
        public List<LkpRaceEthnic> GetAllRaceEthnics()
        {
            var list = (from a in _context.LkpRaceEthnic
                        select a).ToList();

            return list;
        }

        //Returns Race by RaceEthnic
        public LkpRaceEthnic GetRaceEthnicByRace(string RaceEthnic)
        {
            var record = (from a in _context.LkpRaceEthnic
                          where a.RaceEthnic == RaceEthnic
                          select a).FirstOrDefault();

            return record;
        }

        //Get Reace Ethnic Desc
        public string GetRaceEthnicDescription(string RaceEthnic)
        {
            var record = (from a in _context.LkpRaceEthnic
                          where a.RaceEthnic == RaceEthnic
                          select a.RaceEthDesc).FirstOrDefault();

            return record;
        }

    }
}
