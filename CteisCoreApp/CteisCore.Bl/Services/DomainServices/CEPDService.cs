﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class CEPDService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public CEPDService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //Create new CEPD
        public void CreateCEPD(string Cepdno, string Cepdcounties, string CepdName)
        {
            EntCepd CEPD = new EntCepd();
            CEPD.Cepdno = Cepdno;
            CEPD.Cepdcounties = Cepdcounties;
            CEPD.Cepdname = CepdName;

            _context.EntCepd.Add(CEPD);
            _context.SaveChanges();
        }

        //Update CEPD
        public void UpdateCEPD(string Cepdno, string Cepdcounties, string CepdName)
        {
            var CEPD = (from a in _context.EntCepd
                        where a.Cepdno == Cepdno
                        select a).FirstOrDefault();

            if(CEPD != null)
            {
                CEPD.Cepdno = Cepdno;
                CEPD.Cepdcounties = Cepdcounties;
                CEPD.Cepdname = CepdName;

                _context.Entry(CEPD).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove CEPD
        public void RemoveCEPD(string Cepdno)
        {
            var CEPD = (from a in _context.EntCepd
                        where a.Cepdno == Cepdno
                        select a).FirstOrDefault();

            if(CEPD != null)
            {
                _context.Entry(CEPD).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get a list of CEPDS
        public List<vmCEPD> GetCEPDList()
        {

            var list = (from a in _context.EntCepd
                        orderby a.Cepdno
                        select new vmCEPD {
                            Cepdno = a.Cepdno,
                            Cepdcounties = a.Cepdcounties,
                            Cepdname = a.Cepdname + "-" + a.Cepdno
                        }).ToList();

            return list;
        }

    }
}
