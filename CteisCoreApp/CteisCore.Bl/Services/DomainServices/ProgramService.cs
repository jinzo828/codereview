﻿using CteisCore.Bl.ViewModels.CteisViewModels;
using CteisCore.Bl.ViewModels.UserViewModels;
using CteisCoreApp.Data;
using CteisCoreApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CteisCore.Data.Models;

namespace CteisCore.Bl.Services.DomainServices
{
    public class ProgramService
    {

        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProgramService(ApplicationDbContext context, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        //create program
        public void CreateProgram(int Psn, string Cepdno, string Fano, string Oano, string Obno, string ProgramName, string ProgramComments, string CipCode, string ProgramType,
                                    DateTime? ProgramStartDate, bool ProgramActive, DateTime? DeativateDate, string Faname, string Oaname, string Obname, string Region, int? EntId)
        {
            EnrProgram program = new EnrProgram();
            program.Psn = Psn;
            program.Cepdno = Cepdno;
            program.FaNo = Fano;
            program.OaNo = Oano;
            program.ProgramName = ProgramName;
            program.ProgramComments = ProgramComments;
            program.Cipcode = CipCode;
            program.ProgramType = ProgramType;
            program.ProgramStartDate = ProgramStartDate;
            program.ProgramActive = ProgramActive;
            program.DeactivateDate = DeativateDate;
            program.Faname = Faname;
            program.Oaname = Oaname;
            program.Obname = Obname;
            program.Region = Region;
            program.EntId = EntId;

            _context.EnrProgram.Add(program);
            _context.SaveChanges();
        }

        //Update Program
        public void UpdateProgram(int Psn, string Cepdno, string Fano, string Oano, string Obno, string ProgramName, string ProgramComments, string CipCode, string ProgramType,
                                    DateTime? ProgramStartDate, bool ProgramActive, DateTime? DeativateDate, string Faname, string Oaname, string Obname, string Region, int? EntId)
        {
            var program = (from a in _context.EnrProgram
                           where a.Psn == Psn
                           select a).FirstOrDefault();

            if(program != null)
            {
                program.Psn = Psn;
                program.Cepdno = Cepdno;
                program.FaNo = Fano;
                program.OaNo = Oano;
                program.ProgramName = ProgramName;
                program.ProgramComments = ProgramComments;
                program.Cipcode = CipCode;
                program.ProgramType = ProgramType;
                program.ProgramStartDate = ProgramStartDate;
                program.ProgramActive = ProgramActive;
                program.DeactivateDate = DeativateDate;
                program.Faname = Faname;
                program.Oaname = Oaname;
                program.Obname = Obname;
                program.Region = Region;
                program.EntId = EntId;

                _context.Entry(program).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _context.SaveChanges();
            }
        }

        //Remove Program
        public void RemoveProgram(int Psn)
        {
            var program = (from a in _context.EnrProgram
                           where a.Psn == Psn
                           select a).FirstOrDefault();

            if(program != null)
            {
                _context.Entry(program).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
                _context.SaveChanges();
            }
        }

        //Get all Cipcodes
        public IQueryable<EnrProgram> GetAllCipCodes()
        {
            var list = (from a in _context.EnrProgram
                        where a.ProgramActive == true
                        select a).GroupBy(i => i.Cipcode).Select(group => group.First());

            return list;
        }

        //Get program by PSN
        public vmProgram GetProgramByPsn(int Psn)
        {
            var program = (from a in _context.EnrProgram
                           where a.Psn == Psn
                           select new vmProgram {
                               Psn = a.Psn,
                               Cepdno = a.Cepdno,
                               FaNo = a.FaNo,
                               OaNo = a.OaNo,
                               ObNo = a.ObNo,
                               ProgramName = a.ProgramName,
                               ProgramComments = a.ProgramComments,
                               Cipcode = a.Cipcode,
                               ProgramType = a.ProgramType,
                               ProgramStartDate = a.ProgramStartDate,
                               ProgramActive = a.ProgramActive,
                               DeactivateDate = a.DeactivateDate,
                               Faname = a.Faname,
                               Oaname = a.Oaname,
                               Obname = a.Obname,
                               Region = a.Region,
                               EntId = a.EntId
                           }).FirstOrDefault();

            return program;
        }

        //Get All Programs (ProgramMaintenance Controller)
        public List<vmProgram> GetAllPrograms()
            {
            var program = (from a in _context.EnrProgram
                           select new vmProgram
                               {
                               Psn = a.Psn,
                               Cepdno = a.Cepdno,
                               FaNo = a.FaNo,
                               OaNo = a.OaNo,
                               ObNo = a.ObNo,
                               ProgramName = a.ProgramName,
                               ProgramComments = a.ProgramComments,
                               Cipcode = a.Cipcode,
                               ProgramType = a.ProgramType,
                               ProgramStartDate = a.ProgramStartDate,
                               ProgramActive = a.ProgramActive,
                               DeactivateDate = a.DeactivateDate,
                               Faname = a.Faname,
                               Oaname = a.Oaname,
                               Obname = a.Obname,
                               Region = a.Region,
                               EntId = a.EntId
                               }).ToList();

            return program;
            }

        //Get All Programs (ProgramMaintenance Controller)
        public List<vmProgram> GetAllPrograms( bool filter)
        {
            var program = (from a in _context.EnrProgram
                           where a.ProgramActive == filter
                           select new vmProgram
                           {
                               Psn = a.Psn,
                               Cepdno = a.Cepdno,
                               FaNo = a.FaNo,
                               OaNo = a.OaNo,
                               ObNo = a.ObNo,
                               ProgramName = a.ProgramName,
                               ProgramComments = a.ProgramComments,
                               Cipcode = a.Cipcode,
                               ProgramType = a.ProgramType,
                               ProgramStartDate = a.ProgramStartDate,
                               ProgramActive = a.ProgramActive,
                               DeactivateDate = a.DeactivateDate,
                               Faname = a.Faname,
                               Oaname = a.Oaname,
                               Obname = a.Obname,
                               Region = a.Region,
                               EntId = a.EntId
                           }).ToList();
                
            return program;
        }

        //Returns programs by building number or cipcode
        public List<EnrProgram> GetProgramsByBuildNumOrCipCode(string data)
        {
            var buildingPrograms = (from a in _context.EnrProgram
                                    where a.ObNo == data && a.ProgramActive == true
                                    select a).ToList();

            var cipcodePrograms = (from a in _context.EnrProgram
                                   where a.Cipcode == data && a.ProgramActive == true
                                   select a).ToList();

            List<EnrProgram> programs = new List<EnrProgram>();

            if (buildingPrograms.Count != 0)
            {
                foreach (var item in buildingPrograms)
                {
                    programs.Add(item);
                }
            }

            if (cipcodePrograms.Count != 0)
            {
                foreach (var item in cipcodePrograms)
                {
                    programs.Add(item);
                }
            }

            return programs;
        }

        //Get Programs by Building Number
        public List<vmProgram> GetProgramsByBuilding(string BuildingNum)
        {
            var programs = (from a in _context.EnrProgram
                            join b in _context.EntBuilding on a.ObNo equals b.BuildNo
                            where b.BuildNo == BuildingNum
                            select new vmProgram {
                                Psn = a.Psn,
                                Cipcode = a.Cipcode,
                                Cepdno = a.Cepdno,
                                FaNo = a.FaNo,
                                OaNo = a.OaNo,
                                ObNo = a.ObNo,
                                ProgramType = a.ProgramType,
                                ProgramName = a.ProgramName
                            }).ToList();

            return programs;
        }
    }
}
