﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
   public class vmBuilding
    {
        public string BuildingName { get; set; }
        public string Fano { get; set; }
        public string Obno { get; set; }
        public string Oano { get; set; }
        public string BuildingNumber { get; set; }
        public int? EntId { get; set; }
        public string Cepd { get; set; }
        public string FANO { get; set; }
        public string FiscalAgency { get; set; }
        public string District { get; set; }
        public string Status { get; set; }
    }
}
