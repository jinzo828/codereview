﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
    public class vmCourse
    {

        public int CourseId { get; set; }
        public string CourseSectionCode { get; set; }
        public string CourseName { get; set; }
        public DateTime? CrsBeginDate { get; set; }
        public DateTime? CrsEndDate { get; set; }
        public string CourseHour { get; set; }
        public byte? Semester { get; set; }
        public byte? CourseType { get; set; }
        public byte? NumberOfWeeks { get; set; }
        public short? MinutesPerWeek { get; set; }
        public string RoomNumber { get; set; }
        public string Cepdno { get; set; }
        public string FaNo { get; set; }
        public string OaNo { get; set; }
        public string ObNo { get; set; }
        public int? Psn { get; set; }
        public string Cipcode { get; set; }
        public byte? CourseNumber { get; set; }
        public bool? CourseActive { get; set; }
        public int? EligType { get; set; }
        public string EligTypeCert { get; set; }
        public string VirtualDelivery { get; set; }
        public string FinalSegments { get; set; }
        public string Grade { get; set; }
        public string Asegment { get; set; }
        public string Bsegment { get; set; }
        public string Csegment { get; set; }
        public string Dsegment { get; set; }
        public string SubSet { get; set; }

        public vmCourseStaff Staff { get; set; }
    }
}
