﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
    public class vmStudent
    {
        public string StudentUIC { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleInital { get; set; }
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string Grade { get; set; }
        public string RaceEthnic { get; set; }
        public bool? Migrant { get; set; }
        public string ExitStatus { get; set; }
        public string Disabled { get; set; }
        public string EconDisadvanged { get; set; }
        public bool? LEP { get; set; }
        public bool? SingleParent { get; set; }
        public bool? DisplacedHomemaker { get; set; }
        public string SendingDistrict { get; set; }
        public string SendingFacility { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        public string UICStatus { get; set; }
        public string UICStatusDate { get; set; }
        public string MSDSLastUpdated { get; set; }
        public bool CanView { get; set; }
        public bool LeftSchool { get; set; }
    }
}
