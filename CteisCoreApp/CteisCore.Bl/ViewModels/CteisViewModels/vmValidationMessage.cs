﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
   public class vmValidationMessage
    {
        public string Message { get; set; }
        public List<string> Items { get; set; }
        public string Type { get; set; }
    }
}
