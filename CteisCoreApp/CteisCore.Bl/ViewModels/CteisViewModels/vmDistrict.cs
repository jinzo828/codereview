﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
   public class vmDistrict
    {
        public string DistrictName { get; set; }
        public string DistNo { get; set; }
        public string Fano { get; set; }
        public string Oano { get; set; }
        public int EntId { get; set; }
    }
}
