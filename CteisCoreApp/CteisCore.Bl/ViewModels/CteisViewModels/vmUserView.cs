﻿using CteisCore.Bl.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
   public class vmUserView
    {
        public vmUser user { get; set; }
        public bool CEPD { get; set; }
        public List<string> CEPDSelect { get; set; }
        public bool FiscalAgent { get; set; }
        public List<string> StoredFiscals { get; set; }
        public string level { get; set; }
        public bool ModuleEnrollment { get; set; }
        public bool ModuleExpenditures { get; set; }
        public bool ModuleFollowUp { get; set; }
        public bool ModuleProgram { get; set; }
        public List<string> SelectedBuildings { get; set; }
        public string manage { get; set; }
        public bool ManagementFunding { get; set; }
        public bool ManagementExpenditures { get; set; }
        public bool ManagementFollowUp { get; set; }
        public bool ManagementNPReview { get; set; }
        public bool ManagementNPAdmin { get; set; }
        public bool ReportingADHoc { get; set; }
        public bool ReportingTRAC { get; set; }
        public bool ReportingCRCR { get; set; }
        public bool SystemCTEISAdmin { get; set; }
        public bool SystemMDAdmin { get; set; }
        public string college { get; set; }
        public List<string> SelectedPrograms { get; set; }
        public bool PTDUser { get; set; }
        public bool PTDSurv { get; set; }
        public bool Activate { get; set; }
        public bool Deactivate { get; set; }
        public bool User { get; set; }
        public bool CTEISReadOnly { get; set; }
        public bool CTEISUser { get; set; }
        public bool CTEISEconDist { get; set; }
        public string FANO { get; set; }
    }
}
