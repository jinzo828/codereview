﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
    public class vmCourseStaff
    {
        public int CourseStaffId { get; set; }
        public int CourseId { get; set; }
        public string CourseSectionCode { get; set; }
        public string StaffType { get; set; }
        public int StaffId { get; set; }
        public bool? Mentor { get; set; }

        public vmCourse Course { get; set; }
    }
}
