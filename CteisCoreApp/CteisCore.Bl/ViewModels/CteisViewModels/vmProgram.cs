﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
    public class vmProgram
    {
        public int Psn { get; set; }
        public string Cepdno { get; set; }
        public string FaNo { get; set; }
        public string OaNo { get; set; }
        public string ObNo { get; set; }
        public string ProgramName { get; set; }
        public string ProgramComments { get; set; }
        public string Cipcode { get; set; }
        public string ProgramType { get; set; }
        public DateTime? ProgramStartDate { get; set; }
        public bool ProgramActive { get; set; }
        public DateTime? DeactivateDate { get; set; }
        public string Faname { get; set; }
        public string Oaname { get; set; }
        public string Obname { get; set; }
        public string Region { get; set; }
        public int? EntId { get; set; }
    }
}
