﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.CteisViewModels
{
    public class vmCEPD
    {
        public string Cepdno { get; set; }
        public string Cepdcounties { get; set; }
        public string Cepdname { get; set; }
    }
}
