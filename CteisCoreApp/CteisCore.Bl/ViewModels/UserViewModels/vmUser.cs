﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
    public class vmUser
    {

        public string UserId { get; set; }
        [Display(Name = "Meis Number")]
        public string MeisNumber { get; set; }//Application UserName = MeisNumber
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public string Title { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Role { get; set; }
        public List<string> Roles { get; set; }
        [Display(Name = "College Name")]
        public string SchoolName { get; set; }
        public string Level { get; set; }
        public bool Active { get; set; }
        public string FAorCEPD { get; set; }
    }
}
