﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
   public class vmUserCepdAdmin
    {
        public int CepdlistId { get; set; }
        public string UserId { get; set; }
        public string Cepdno { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MeissNumber { get; set; }
    }
}
