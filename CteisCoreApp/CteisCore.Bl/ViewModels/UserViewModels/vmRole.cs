﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
    public class vmRole
    {
        public string RoleName { get; set; }
        public string RoleId { get; set; }
        public string UserId { get; set; }
    }
}
