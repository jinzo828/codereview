﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
   public class vmUsrUserFalist
    {
        public int FalistId { get; set; }
        public string UserId { get; set; }
        public string Fano { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MeissNumber { get; set; }
    }
}
