﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
   public class vmUserCollegeProgram
    {
        public int CollegeUsrProgramId { get; set; }
        public string Fano { get; set; }
        public string Oano { get; set; }
        public string Obno { get; set; }
        public int Psn { get; set; }
        public int? EntId { get; set; }
        public string UserId { get; set; }
        public string ProgramName { get; set; }
        public string BuildingName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string SchoolName { get; set; }
        public bool Active { get; set; }
        public string MeissNumber { get; set; }
    }
}
