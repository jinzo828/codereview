﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CteisCore.Bl.ViewModels.UserViewModels
{
   public class vmUserBuilding
    {
        public int BldglistId { get; set; }
        public string UserId { get; set; }
        public int? EntId { get; set; }
        public string Obno { get; set; }
        public string Fano { get; set; }
        public string Oano { get; set; }
        public string BuildingName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MeissNumber { get; set; }
    }
}
